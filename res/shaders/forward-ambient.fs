/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#version 110

uniform vec3 u_ambient;
uniform sampler2D u_textureSampler;
uniform vec3 eye;
uniform vec3 fogColor;
uniform float fogDensity;

varying vec3 v_worldPos;
varying vec3 v_color;
varying vec2 v_texCoord;
varying vec3 v_normal;


vec3 calcFog(vec3 color, float fogValue)
{
    return mix(color, fogColor, fogValue);
}

vec3 expFog(vec3 color)
{
    float dist = distance(-eye, v_worldPos);
    float e = exp(-fogDensity * dist);
    float fogValue = clamp(e, 0.0, 1.0);
    return calcFog(color, 1.0 - fogValue);
}

void main()
{
	vec4 color = vec4(v_color, 1.0) * vec4(u_ambient, 1.0);
	vec4 textureColor = texture2D(u_textureSampler, v_texCoord);

	if(textureColor.xyz != vec3(0.0, 0.0, 0.0))
	{
		color *= textureColor;
	}

	gl_FragColor = vec4(expFog(color.rgb), 1.0);
}

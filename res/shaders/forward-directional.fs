/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#version 110

uniform vec3 u_direction;
uniform vec3 u_color;
uniform vec3 u_eyePosition;
uniform float u_intensity;
uniform float u_specularIntensity;
uniform float u_specularExponent;

varying vec3 v_worldPos;
varying vec3 v_normal;

void main()
{

	// calculate diffuse
	float NDotL = dot(v_normal, -u_direction);
	vec3 diffuse = u_color * u_intensity * max(0.0, NDotL);
	vec3 specularComponent = vec3(0.0, 0.0, 0.0);

	if(NDotL > 0.0)
	{
		// calculate specular
		vec3 eyeDir = normalize(v_worldPos - u_eyePosition);
		vec3 reflectVec = normalize(reflect(u_direction, v_normal));

		float specular = pow(max(0.0, dot(eyeDir, reflectVec)), u_specularExponent) * u_specularIntensity;
		specularComponent = vec3(specular, specular, specular);
	}

	gl_FragColor = vec4(diffuse + specularComponent, 1.0);

}

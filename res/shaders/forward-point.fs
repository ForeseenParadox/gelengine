/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#version 110

uniform vec3 u_position;
uniform vec3 u_color;
uniform vec3 u_eyePosition;
uniform float u_intensity;
uniform float u_specularIntensity;
uniform float u_specularExponent;
uniform float u_constantAtten;
uniform float u_linearAtten;
uniform float u_quadraticAtten;

varying vec3 v_worldPos;
varying vec3 v_normal;

void main()
{

	// calculate diffuse
	vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
	vec3 dir = normalize(v_worldPos - u_position);
	float NDotL = dot(v_normal, -dir);

	// calculate diffuse
	color += vec4((u_color * u_intensity * max(0.0, NDotL)), 0.0);

	if(NDotL > 0.0)
	{
		vec3 eyeDir = normalize(v_worldPos - u_eyePosition);
		vec3 reflectVec = normalize(reflect(dir, v_normal));

		// calculate specular
		float specular = pow(max(0.0, dot(eyeDir, reflectVec)), u_specularExponent) * u_specularIntensity;
		color += vec4(specular, specular, specular, 0.0);
	}

	// calculate attenuation
	float distance = distance(v_worldPos, u_position);
	float attenuation = u_quadraticAtten * distance * distance + u_linearAtten * distance + u_constantAtten;

	color /= attenuation;

	gl_FragColor = color;
	
}

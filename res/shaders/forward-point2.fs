/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#version 110

uniform vec3 u_position;
uniform vec3 u_color;
uniform vec2 u_translate;
uniform float u_intensity;
uniform float u_constantAtten;
uniform float u_linearAtten;
uniform float u_quadraticAtten;

void main()
{
	vec2 pos = gl_FragCoord.xy - u_translate;
	float dist = distance(u_position.xy, pos);
	float atten = u_quadraticAtten * dist * dist + u_linearAtten * dist + u_constantAtten;
	gl_FragColor = vec4((u_color * u_intensity) / atten, 1.0);
}

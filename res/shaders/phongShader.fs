/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#version 110

uniform sampler2D u_sampler;
uniform sampler2D u_normalSampler;
uniform vec3 u_ambient;
uniform vec3 directionalColor;
uniform float directionalIntensity;
uniform vec3 directionalDir;
uniform vec3 eye;
uniform float specularIntensity;
uniform float specularExponent;

varying vec3 worldPos;
varying vec3 v_color;
varying vec2 v_texCoord;
varying vec3 v_normal;

vec3 dirLight()
{
	// diffuse
	vec3 dir = normalize(directionalDir);
	vec3 normal = normalize(v_normal);
	vec3 diffuse = directionalColor * directionalIntensity * max(0.0, dot(normal, -dir));
	
	float NDotL = dot(normal, -dir);
	vec3 specularFinal = vec3(0.0, 0.0, 0.0);
	if(NDotL > 0.0)
	{
		// specular
		vec3 eyeDir = normalize(worldPos - eye);
		vec3 reflectVec = normalize(reflect(dir, normal));
		
		float baseSpecular = max(0.0, dot(eyeDir, reflectVec));
		float specular = pow(baseSpecular, specularExponent) * specularIntensity;
		specularFinal = vec3(specular, specular, specular);
	}

	// combined
	return diffuse + specularFinal;
	
}

void main()
{
	gl_FragColor = vec4(u_ambient, 1.0) * texture2D(u_sampler, v_texCoord) + vec4(dirLight(), 1.0);;	
}

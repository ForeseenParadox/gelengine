/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#version 110

uniform mat4 u_viewProjectionMatrix;

attribute vec3 a_position;
attribute vec3 a_color;
attribute vec2 a_texCoord;
attribute vec3 a_normal;

varying vec3 worldPos;
varying vec3 v_color;
varying vec2 v_texCoord;
varying vec3 v_normal;

void main()
{
	gl_Position = u_viewProjectionMatrix * vec4(a_position, 1.0);
	worldPos = (u_viewProjectionMatrix * vec4(a_position, 1.0)).xyz;
	v_color = a_color;
	v_texCoord = a_texCoord;
	v_normal = 0a_normal;
}

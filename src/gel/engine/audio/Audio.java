/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.audio;

import static org.lwjgl.openal.AL10.AL_NO_ERROR;
import static org.lwjgl.openal.AL10.AL_ORIENTATION;
import static org.lwjgl.openal.AL10.AL_POSITION;
import static org.lwjgl.openal.AL10.AL_VELOCITY;
import static org.lwjgl.openal.AL10.alGetError;
import static org.lwjgl.openal.AL10.alListener3f;
import gel.engine.core.Gel;

import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;

public class Audio
{

	public Audio()
	{
		if (Gel.audio == null)
		{
			Gel.audio = this;
		} else
		{
			throw new IllegalStateException("Can not make more than one instance of Audio");
		}
		
	}

	public int getALError()
	{
		return alGetError();
	}

	public void setListenerPosition(float x, float y, float z)
	{
		alListener3f(AL_POSITION, x, y, z);
	}

	public void setListenerVelocity(float x, float y, float z)
	{
		alListener3f(AL_VELOCITY, x, y, z);
	}

	public void setListenerOrientation(float rotX, float rotY, float rotZ)
	{
		alListener3f(AL_ORIENTATION, rotX, rotY, rotZ);
	}

	public void checkALErrors()
	{
		int err = 0;
		boolean berr = false;

		while ((err = getALError()) != AL_NO_ERROR)
		{
			berr = true;
			Gel.log.fatal("OpenAL error: " + err);
		}

		if (berr)
		{
			// crash because of error
			System.exit(1);
		}

	}

	public void create()
	{
		try
		{
			AL.create();
		} catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	public void dispose()
	{
		AL.destroy();
	}

}

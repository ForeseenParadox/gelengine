/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.audio;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static org.lwjgl.openal.AL10.*;

public class AudioBuffer
{

	private int handle;

	public AudioBuffer()
	{
		handle = alGenBuffers();
	}

	public int getHandle()
	{
		return handle;
	}

	public void sendAudioData(int format, ByteBuffer data, int freq)
	{
		alBufferData(handle, format, data, freq);
	}

	public void sendAudioData(int format, ShortBuffer data, int freq)
	{
		alBufferData(handle, format, data, freq);
	}

	public void sendAudioData(int format, IntBuffer data, int freq)
	{
		alBufferData(handle, format, data, freq);
	}

	public void dispose()
	{
		alDeleteBuffers(handle);
	}

}

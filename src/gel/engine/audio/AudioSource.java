/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.audio;

import static org.lwjgl.openal.AL10.AL_BUFFER;
import static org.lwjgl.openal.AL10.AL_FALSE;
import static org.lwjgl.openal.AL10.AL_GAIN;
import static org.lwjgl.openal.AL10.AL_LOOPING;
import static org.lwjgl.openal.AL10.AL_PITCH;
import static org.lwjgl.openal.AL10.AL_POSITION;
import static org.lwjgl.openal.AL10.AL_TRUE;
import static org.lwjgl.openal.AL10.AL_VELOCITY;
import static org.lwjgl.openal.AL10.alDeleteSources;
import static org.lwjgl.openal.AL10.alGenSources;
import static org.lwjgl.openal.AL10.alSource3f;
import static org.lwjgl.openal.AL10.alSourcePause;
import static org.lwjgl.openal.AL10.alSourcePlay;
import static org.lwjgl.openal.AL10.alSourceStop;
import static org.lwjgl.openal.AL10.alSourcef;
import static org.lwjgl.openal.AL10.alSourcei;

public class AudioSource
{

	private int handle;

	public AudioSource()
	{
		this(null);
	}

	public AudioSource(AudioBuffer data)
	{
		handle = alGenSources();

		if (data != null)
		{
			loadBufferToSource(data);
		}
	}

	public int getHandle()
	{
		return handle;
	}

	public void loadBufferToSource(AudioBuffer data)
	{
		alSourcei(handle, AL_BUFFER, data.getHandle());
	}

	public void setPosition(float x, float y, float z)
	{
		alSource3f(handle, AL_POSITION, x, y, z);
	}

	public void setVelocity(float x, float y, float z)
	{
		alSource3f(handle, AL_VELOCITY, x, y, z);
	}

	public void setGain(float gain)
	{
		alSourcef(handle, AL_GAIN, gain);
	}

	public void setPitch(float pitch)
	{
		alSourcef(handle, AL_PITCH, pitch);
	}

	public void stop()
	{
		alSourceStop(handle);
	}

	public void pause()
	{
		alSourcePause(handle);
	}

	public void play()
	{
		alSourcePlay(handle);
	}

	public void loop(boolean loop)
	{
		if (loop)
		{
			alSourcei(handle, AL_LOOPING, AL_TRUE);
		} else
		{
			alSourcei(handle, AL_LOOPING, AL_FALSE);
		}
	}

	public void dispose()
	{
		alDeleteSources(handle);
	}

}

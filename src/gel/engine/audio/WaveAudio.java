/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.audio;

import static org.lwjgl.openal.AL10.AL_FORMAT_MONO16;
import static org.lwjgl.openal.AL10.AL_FORMAT_MONO8;
import static org.lwjgl.openal.AL10.AL_FORMAT_STEREO16;
import static org.lwjgl.openal.AL10.AL_FORMAT_STEREO8;
import gel.engine.utils.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

public class WaveAudio
{

	private ByteBuffer data;
	private int format;
	private int sampleRate;

	public WaveAudio(String path) throws FileNotFoundException, UnsupportedAudioFileException, IOException
	{
		this(IOUtils.getInternalResourceAsFile(path));
	}

	public WaveAudio(File file) throws FileNotFoundException, UnsupportedAudioFileException, IOException
	{
		this(new BufferedInputStream(new FileInputStream(file)));
	}

	public WaveAudio(InputStream stream) throws UnsupportedAudioFileException, IOException
	{
		this(AudioSystem.getAudioInputStream(stream));
	}

	public WaveAudio(AudioInputStream stream)
	{

		AudioFormat audioFormat = stream.getFormat();

		// load the openal format
		if (audioFormat.getChannels() == 1)
		{
			if (audioFormat.getSampleSizeInBits() == 8)
			{
				format = AL_FORMAT_MONO8;
			} else if (audioFormat.getSampleSizeInBits() == 16)
			{
				format = AL_FORMAT_MONO16;
			}
		} else if (audioFormat.getChannels() == 2)
		{
			if (audioFormat.getSampleSizeInBits() == 8)
			{
				format = AL_FORMAT_STEREO8;
			} else if (audioFormat.getSampleSizeInBits() == 16)
			{
				format = AL_FORMAT_STEREO16;
			}
		}

		sampleRate = (int) audioFormat.getSampleRate();

		// load wave data
		try
		{
			int size = stream.available();
			byte[] rawData = new byte[size];

			stream.read(rawData);

			data = IOUtils.byteArrayToBuffer(rawData);
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public ByteBuffer getData()
	{
		return data;
	}

	public int getFormat()
	{
		return format;
	}

	public int getSampleRate()
	{
		return sampleRate;
	}

	public static WaveAudio loadWaveAudio(String path)
	{
		try
		{
			return new WaveAudio(path);
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static WaveAudio loadWaveAudio(File file)
	{
		try
		{
			return new WaveAudio(file);
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static WaveAudio loadWaveAudio(InputStream stream)
	{
		try
		{
			return new WaveAudio(stream);
		} catch (UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static WaveAudio loadWaveAudio(AudioInputStream stream)
	{
		return new WaveAudio(stream);
	}

}

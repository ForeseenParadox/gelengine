/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.core;

/**
 * An abstraction over an application.
 * 
 * @author ForeseenParadox
 * 
 */
public abstract class Application
{

	/**
	 * Initializes the application. Load all game content here.
	 */
	public abstract void init();

	/**
	 * Called when the window has been resized.
	 */
	public abstract void resized();

	/**
	 * Updates the application.
	 */
	public abstract void update();

	/**
	 * Renders the application.
	 */
	public abstract void render();

	/**
	 * Disposes the application.
	 */
	public abstract void dispose();

}

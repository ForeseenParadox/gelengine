package gel.engine.core;

import gel.engine.scene.object.GameObject;

public class Basic3DGame extends Application
{

    protected GameObject root;

    public GameObject getRootObject()
    {
        return root;
    }

    @Override
    public void init()
    {
        Gel.graphics.setDepthTestEnabled(true);
        root = new GameObject();
    }

    @Override
    public void resized()
    {

    }

    @Override
    public void update()
    {
        root.update();
    }

    @Override
    public void render()
    {
        Gel.graphics.clearDepthBuffer();
        root.render();
    }

    @Override
    public void dispose()
    {

    }
}

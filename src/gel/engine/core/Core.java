/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.core;

import gel.engine.audio.Audio;
import gel.engine.core.debug.ProfileTimer;
import gel.engine.graphics.Color;
import gel.engine.graphics.RenderCore;
import gel.engine.input.Input;
import gel.engine.utils.Logger;
import gel.engine.window.Window;

import java.awt.Canvas;

/**
 * The Core engine.
 * 
 * @author ForeseenParadox
 */
public class Core
{

	private boolean running;
	private Application app;
	private int fps;
	private int ups;
	private long delta;
	private int foregroundFps;
	private int backgroundFps;
	private int foregroundUps;
	private int backgroundUps;
	private Color clearColor;
	private ProfileTimer updateProfiler;
	private ProfileTimer renderProfiler;

	/**
	 * Constructs a new Core instance. Only one Core instance may be made
	 * throughout the application's lifetime.
	 */
	public Core(Application app, boolean start, String title, int width, int height, boolean resizable, int fgFps, int fgUps, int bgFps, int bgUps)
	{
		this(app, null, start, title, width, height, resizable, fgFps, fgUps, bgFps, bgUps);
	}

	/**
	 * Constructs a new Core instance. Only one Core instance may be made
	 * throughout the application's lifetime.
	 */
	public Core(Application app, Canvas parent, boolean start, String title, int width, int height, boolean resizable, int fgFps, int fgUps, int bgFps, int bgUps)
	{
		if (Gel.core == null)
		{
			Gel.core = this;

			this.app = app;

			// initialize window
			Window wnd = new Window();
			wnd.setTitle(title);
			wnd.setSize(width, height);

			if (parent != null)
			{
				wnd.setParent(parent);
			}

			wnd.setResizable(resizable);
			wnd.create();

			// initialize rendering engine
			new RenderCore();

			foregroundFps = fgFps;
			foregroundUps = fgUps;
			backgroundFps = bgFps;
			backgroundUps = bgUps;

			// initialize audio
			Audio aud = new Audio();
			aud.create();

			clearColor = Color.BLACK;

			if (start)
			{
				start();
			}
		} else
		{
			throw new IllegalStateException("Can not make more than one instance of Core");
		}
	}

	/**
	 * @return The creation status of this singleton.
	 */
	public static boolean created()
	{
		if (Gel.core == null)
		{
			return false;
		} else
		{
			return true;
		}
	}

	/**
	 * @return The running status of the Core engine.
	 */
	public boolean isRunning()
	{
		return running;
	}

	/**
	 * @return The application instance attached to the engine.
	 */
	public Application getApplication()
	{
		return app;
	}

	/**
	 * @return The amount of frames per second the application is running at.
	 */
	public int getFps()
	{
		return fps;
	}

	/**
	 * @return The amount of updates per second the application is running at.
	 */
	public int getUps()
	{
		return ups;
	}

	/**
	 * @return The color that the screen is being cleared to every frame.
	 */
	public Color getClearColor()
	{
		return clearColor;
	}

	/**
	 * @return The amount of time passed between this frame and last frame.
	 */
	public long getDelta()
	{
		return delta;
	}

	/**
	 * @return The amount of frames per second the engine will target when the
	 *         game window is in focus.
	 */
	public int getForegroundFps()
	{
		return foregroundFps;
	}

	/**
	 * @return The amount of frames per second the engine will target when the
	 *         game window is not in focus.
	 */
	public int getBackgroundFps()
	{
		return backgroundFps;
	}

	/**
	 * @return The amount of updates per second the engine will target when the
	 *         game window is in focus.
	 */
	public int getForegroundUps()
	{
		return foregroundUps;
	}

	/**
	 * @return The amount of updates per second the engine will target when the
	 *         game window is not in focus.
	 */
	public int getBackgroundUps()
	{
		return backgroundUps;
	}

	/**
	 * @return The profiler for updating the application.
	 */
	public ProfileTimer getUpdateTimer()
	{
		return updateProfiler;
	}

	/**
	 * @return The profiler for rendering the application.
	 */
	public ProfileTimer getRenderTimer()
	{
		return renderProfiler;
	}

	/**
	 * Sets the amount of frames per second that the engine will target when the
	 * window is focused.
	 */
	public void setForegroundFps(int fps)
	{
		this.foregroundFps = fps;
	}

	/**
	 * Sets the amount of frames per second that the engine will target when the
	 * window is not focused.
	 */
	public void setBackgroundFps(int fps)
	{
		this.backgroundFps = fps;
	}

	/**
	 * Sets the amount of updates per second that the engine will target when
	 * the window is focused.
	 */
	public void setForegroundUps(int ups)
	{
		this.foregroundUps = ups;
	}

	/**
	 * Sets the amount of frames per second that the engine will target when the
	 * window is not focused.
	 */
	public void setBackgroundUps(int ups)
	{
		this.backgroundUps = ups;
	}

	/**
	 * Sets the color that the screen will clear to every frame.
	 */
	public void setClearColor(Color clearColor)
	{
		this.clearColor = clearColor;
	}

	/**
	 * Starts the engine.
	 */
	public void start()
	{
		running = true;
		run();
	}

	/**
	 * Stops the engine.
	 */
	public void stop()
	{
		running = false;
	}

	/**
	 * Start the main game loop.
	 */
	private void run()
	{

		// initialize the engine
		init();

		long lastSecond = System.nanoTime();
		long thisFrame = System.nanoTime();
		long lastFrame = System.nanoTime();
		double updateTime = 1e9f / foregroundFps;
		double renderTime = 1e9f / foregroundUps;
		double accumulativeUpdateTime = 0;
		double accumulativeFrameTime = 0;
		long sleepTime = 0;
		int updateCount = 0;
		int frameCount = 0;

		while (running)
		{

			// Get the delta between this frame and last frame
			thisFrame = System.nanoTime();
			delta = thisFrame - lastFrame;
			lastFrame = thisFrame;

			// Accumulate the delta in the update and render accumulate
			// variables.
			accumulativeUpdateTime += delta;
			accumulativeFrameTime += delta;

			int targetUps = 0;
			int targetFps = 0;

			if (Gel.window.isVisible())
			{
				targetFps = foregroundFps;
				targetUps = foregroundUps;
			} else
			{
				targetFps = backgroundFps;
				targetUps = backgroundUps;
			}

			updateTime = 1e9f / targetUps;
			renderTime = 1e9f / targetFps;

			// Check if window was resized
			if (Gel.window.wasResized())
			{
				app.resized();
			}

			// Update and take into account any missed frames.
			if (targetUps != -1)
			{
				while (accumulativeUpdateTime >= updateTime)
				{
					updateProfiler.start();
					update();
					updateProfiler.end();
					updateCount++;
					accumulativeUpdateTime -= updateTime;
				}
			} else
			{
				updateProfiler.start();
				update();
				updateProfiler.end();
				updateCount++;
				accumulativeUpdateTime = 0;
			}

			// Render if possible.
			if (targetFps != -1)
			{
				if (accumulativeFrameTime >= renderTime)
				{
					renderProfiler.start();
					render();
					renderProfiler.end();
					frameCount++;
					accumulativeFrameTime = 0;
				} else
				{

					// Calculate sleep time and sleep for that amount.

					sleepTime = (int) ((renderTime - accumulativeFrameTime) / (1e6f));

					try
					{
						Thread.sleep(sleepTime);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}

				}
			} else
			{
				renderProfiler.start();
				render();
				renderProfiler.end();
				frameCount++;
				accumulativeFrameTime = 0;
			}

			// Check if a second has passed, and if so, update fps and ups.
			if (System.nanoTime() - lastSecond >= 1e9)
			{
				fps = frameCount;
				ups = updateCount;
				frameCount = 0;
				updateCount = 0;
				lastSecond = System.nanoTime();
			}

			// Check if the game needs to stop.
			if (Gel.window.isCloseRequested())
			{
				stop();
			}

		}

		dispose();

	}

	/**
	 * Initialize the engine and application.
	 */
	private void init()
	{

		/*
		 * load all default engine data
		 */

		ProfileTimer initProfiler = new ProfileTimer();

		initProfiler.start();

		// initialize logger
		Gel.log = new Logger(System.out);

		// initialize engine components with default values

		new Input().create();

		updateProfiler = new ProfileTimer();
		renderProfiler = new ProfileTimer();

		// initialize application and all plugins
		app.init();

		initProfiler.end();

		Gel.log.info("Engine initialization took " + (initProfiler.getDelta() / 1e6f) + " ms");
	}

	/**
	 * Update the engine and application.
	 */
	private void update()
	{
		Gel.input.update();
		app.update();
	}

	/**
	 * Render the engine and application.
	 */
	private void render()
	{

		Gel.graphics.clearColorBuffer();

		app.render();

		// check for an opengl and openal errors after application renders
		Gel.graphics.checkGLErrors();
		Gel.audio.checkALErrors();

		Gel.window.update();

	}

	/**
	 * Dispose the engine and application.
	 */
	private void dispose()
	{

		app.dispose();

		Gel.window.dispose();
		Gel.audio.dispose();

	}

}

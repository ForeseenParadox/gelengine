/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.core;

import gel.engine.audio.Audio;
import gel.engine.graphics.RenderCore;
import gel.engine.input.Input;
import gel.engine.utils.Logger;
import gel.engine.window.Window;

/**
 * A class holding public references to subsystems of the engine.
 * 
 * @author ForeseenParadox
 */
public class Gel
{

	public static Window window = null;
	public static RenderCore graphics = null;
	public static Audio audio = null;
	public static Core core = null;
	public static Input input = null;
	public static Logger log = null;

}

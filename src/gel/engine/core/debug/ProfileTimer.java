/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.core.debug;

public class ProfileTimer
{

	private boolean started;
	private long startInvocation;
	private long endInvocation;
	private long delta;
	private long accumDelta;
	private int invocationCount;

	public boolean isStarted()
	{
		return started;
	}

	public long getStartInvocation()
	{
		return startInvocation;
	}

	public long getEndInvocation()
	{
		return endInvocation;
	}

	public long getDelta()
	{
		return delta;
	}

	public long getAccumDelta()
	{
		return accumDelta;
	}

	public int getInvocationCount()
	{
		return invocationCount;
	}

	public void start()
	{
		if (!started)
		{
			started = true;
			startInvocation = System.nanoTime();
		} else
		{
			throw new IllegalStateException("Profile timer already started");
		}

	}

	public void end()
	{
		if (started)
		{
			endInvocation = System.nanoTime();
			delta = endInvocation - startInvocation;
			accumDelta += delta;
			invocationCount++;
			started = false;
		} else
		{
			throw new IllegalStateException("Must start profile timer before invocating end");
		}
	}

	public void reset()
	{
		started = false;
		startInvocation = 0;
		endInvocation = 0;
		delta = 0;
		accumDelta = 0;
		invocationCount = 0;
	}

}

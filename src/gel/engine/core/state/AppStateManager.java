/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.core.state;

import java.util.ArrayList;
import java.util.List;

public class AppStateManager
{

	private List<AppState> appStates;
	private AppState currentState;

	public AppStateManager()
	{
		this.appStates = new ArrayList<AppState>();
	}

	public List<AppState> getAppStates()
	{
		return appStates;
	}

	public AppState getAppState(int id)
	{
		for (AppState state : appStates)
		{
			if (state.getId() == id)
			{
				return state;
			}
		}
		return null;
	}

	public AppState getCurrentState()
	{
		return currentState;
	}

	public void addAppState(AppState state)
	{
		this.appStates.add(state);
	}

	public void setCurrentState(int id)
	{
		for (AppState state : appStates)
		{
			if (state.getId() == id)
			{
				currentState = state;
			}
		}
	}

}

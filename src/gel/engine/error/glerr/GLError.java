/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.error.glerr;

import gel.engine.error.GelError;

public class GLError extends GelError
{

	private static final long serialVersionUID = -2667552649870063720L;

	public GLError(int id)
	{
	//	System.err.println("An OpenGL error has occured with decimal ID: " + id + " and hex ID: " + Integer.toString(id, 16));
	}

}

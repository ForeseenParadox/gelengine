/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import gel.engine.core.Gel;
import gel.engine.utils.IOUtils;

import java.awt.image.BufferedImage;

public class Bitmap
{

	private int width;
	private int height;
	private int[] data;

	public Bitmap(int width, int height)
	{
		this.width = width;
		this.height = height;
		allocate();
	}

	public Bitmap(String path)
	{
		BufferedImage image = IOUtils.loadInteralImage(path);
		width = image.getWidth();
		height = image.getHeight();
		allocate();
		image.getRGB(0, 0, width, height, data, 0, width); // x,y,width,height,array,offset,scan size

		flipY();
	}

	public Bitmap(Bitmap other)
	{
		width = other.getWidth();
		height = other.getHeight();
		allocate();
		other.getData(data);
	}

	public Bitmap(BufferedImage awtImage)
	{
		width = awtImage.getWidth();
		height = awtImage.getHeight();
		allocate();
		awtImage.getRGB(0, 0, width, height, data, 0, width); // x,y,width,height,array,offset,scan size
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public int size()
	{
		return width * height;
	}

	public int[] getData()
	{
		int[] res = new int[data.length];
		getData(res);
		return res;
	}

	public void getData(int[] dst)
	{
		if (dst.length != data.length)
		{
			// throw error
			Gel.log.fatal("Destination array not the same length as data array");
		}

		for (int i = 0; i < data.length; i++)
		{
			dst[i] = data[i];
		}
	}

	public int getPixel(int x, int y)
	{
		return data[x + (y * width)];
	}

	public int getRed(int x, int y)
	{
		return (getPixel(x, y) >> 16) & 0xFF;
	}

	public int getGreen(int x, int y)
	{
		return (getPixel(x, y) >> 8) & 0xFF;
	}

	public int getBlue(int x, int y)
	{
		return (getPixel(x, y) >> 0) & 0xFF;
	}

	public void allocate()
	{
		data = new int[width * height];
	}

	public Bitmap resize(int width, int height)
	{
		int[] newAlloc = new int[width * height];

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				int dataValue = 0;
				if (x < this.width && y < this.height)
				{
					dataValue = getPixel(x, y);

				}
				newAlloc[x + (y * width)] = dataValue;
			}
		}

		this.width = width;
		this.height = height;
		allocate();
		setData(newAlloc);

		return this;
	}

	public Bitmap multiplicativeBlend(int rgb)
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				multiplicativeBlendPixel(x, y, rgb);
			}
		}
		return this;
	}

	public Bitmap additiveBlend(int rgb)
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				additiveBlendPixel(x, y, rgb);
			}
		}
		return this;
	}

	public Bitmap setData(int[] data)
	{
		if (data.length != this.data.length)
		{
			// throw error
		}
		for (int i = 0; i < data.length; i++)
		{
			this.data[i] = data[i];
		}
		return this;
	}

	public Bitmap setData(String imagePath)
	{
		BufferedImage image = IOUtils.loadInteralImage(imagePath);
		width = image.getWidth();
		height = image.getHeight();

		int[] res = new int[width * height];
		image.getRGB(0, 0, width, height, res, 0, width); // x,y,width,height,array,offset,scan size

		for (int i = 0; i < data.length; i++)
		{
			if (i > data.length)
			{
				Gel.log.warning("The loaded image was larger than the current allocated data");
				break;
			}
			data[i] = res[i];
		}

		flipY();
		return this;
	}

	public Bitmap setPixel(int x, int y, int value)
	{
		data[x + (y * width)] = value;
		return this;
	}

	public void multiplicativeBlendPixel(int x, int y, int rgb)
	{
		int blendRed = (rgb >> 16) & 0xFF;
		int blendGreen = (rgb >> 8) & 0xFF;
		int blendBlue = (rgb >> 0) & 0xFF;

		int red = (((getRed(x, y) * blendRed) / 0xFF) << 16);
		int green = (((getGreen(x, y) * blendGreen) / 0xFF) << 8);
		int blue = (((getBlue(x, y) * blendBlue) / 0xFF) << 0);

		setPixel(x, y, red | green | blue);
	}

	public void additiveBlendPixel(int x, int y, int rgb)
	{
		int blendRed = (rgb >> 16) & 0xFF;
		int blendGreen = (rgb >> 8) & 0xFF;
		int blendBlue = (rgb >> 0) & 0xFF;

		int red = (((getRed(x, y) + blendRed) << 16));
		int green = (((getGreen(x, y) + blendGreen) << 8));
		int blue = (((getBlue(x, y) + blendBlue) << 0));

		setPixel(x, y, red | green | blue);
	}

	public Bitmap fill(int rgb)
	{
		for (int i = 0; i < data.length; i++)
		{
			data[i] = rgb;
		}
		return this;
	}

	public Bitmap flipX()
	{
		int[] res = new int[width * height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				res[x + (y * width)] = data[(width - x - 1) + (y * width)];
			}
		}
		setData(res);
		return this;
	}

	public Bitmap flipY()
	{
		int[] res = new int[width * height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				res[x + (y * width)] = data[x + ((height - y - 1) * width)];
			}
		}
		setData(res);
		return this;
	}

}

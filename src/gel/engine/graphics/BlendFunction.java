/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import static org.lwjgl.opengl.GL11.GL_DST_COLOR;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.*;

public enum BlendFunction
{
	ADDITIVE(GL_ONE, GL_ONE),
	MULTIPLICATIVE(GL_DST_COLOR, GL_ZERO),
	ALPHA_BLEND(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	private int param1;
	private int param2;

	private BlendFunction(int param1, int param2)
	{
		this.param1 = param1;
		this.param2 = param2;
	}

	public int getParam1()
	{
		return param1;
	}

	public int getParam2()
	{
		return param2;
	}
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import gel.engine.math.Vector3f;

public class Color
{

	public static final Color BLACK = new Color(0xFF000000);
	public static final Color WHITE = new Color(0xFFFFFFFF);
	public static final Color RED = new Color(0xFFFF0000);
	public static final Color GREEN = new Color(0xFF00FF00);
	public static final Color BLUE = new Color(0xFF0000FF);
	public static final Color CYAN = new Color(0xFF00FFFF);
	public static final Color FUCHSIA = new Color(0xFFFF77FF);
	public static final Color LAVENDER = new Color(0xFFB57EDC);
	public static final Color INDIGO = new Color(0xFF4B0082);
	public static final Color GOLD = new Color(0xFFFFD700);
	public static final Color LIME_GREEN = new Color(0xFFBFFF00);
	public static final Color OLIVE = new Color(0xFF808000);
	public static final Color CORNFLOWER_BLUE = new Color(0xFF6495ED);

	private float red;
	private float green;
	private float blue;
	private float alpha;

	public Color(int hex)
	{
		this((hex >> 16) & 0xFF, (hex >> 8) & 0xFF, (hex >> 0) & 0xFF, (hex >> 24) & 0xFF);
	}

	public Color(float red, float green, float blue)
	{
		this(red, green, blue, 1.0f);
	}

	public Color(float red, float green, float blue, float alpha)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;

		if (red > 1)
			this.red /= 255f;
		if (green > 1)
			this.green /= 255f;
		if (blue > 1)
			this.blue /= 255f;
		if (alpha > 1)
			this.alpha /= 255f;

	}

	public int getRGB()
	{
		return ((int) (alpha * 255) << 24) | ((int) (red * 255) << 16) | ((int) (green * 255) << 8) | ((int) (blue * 255) << 0);
	}

	public float getRed()
	{
		return red;
	}

	public float getGreen()
	{
		return green;
	}

	public float getBlue()
	{
		return blue;
	}

	public float getAlpha()
	{
		return alpha;
	}

	public void setRGB(int hex)
	{
		this.alpha = ((hex >> 24) & 0xFF) / 255;
		this.red = ((hex >> 16) & 0xFF) / 255;
		this.green = ((hex >> 8) & 0xFF) / 255;
		this.blue = ((hex >> 0) & 0xFF) / 255;
	}

	public void setRed(float red)
	{
		this.red = red;
	}

	public void setGreen(float green)
	{
		this.green = green;
	}

	public void setBlue(float blue)
	{
		this.blue = blue;
	}

	public void setAlpha(float alpha)
	{
		this.alpha = alpha;
	}

	public Vector3f toVector3f()
	{
		return new Vector3f(red, green, blue);
	}

	public static int getRedFromRGB(int rgb)
	{
		return (rgb >> 16) & 0xFF;
	}

	public static int getGreenFromRGB(int rgb)
	{
		return (rgb >> 8) & 0xFF;
	}

	public static int getBlueFromRGB(int rgb)
	{
		return (rgb >> 0) & 0xFF;
	}
}

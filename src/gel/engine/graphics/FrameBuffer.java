/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import gel.engine.graphics.texture.Texture;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL20.glDrawBuffers;
import static org.lwjgl.opengl.GL30.*;

public class FrameBuffer
{

	private int handle;

	public FrameBuffer()
	{
		handle = glGenFramebuffers();
	}

	public int getHandle()
	{
		return handle;
	}

	public void bind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, handle);
	}

	public void attachTexture(Texture texture, int attachment)
	{
		bind();
		glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture.getID(), 0);
	}

	public void enableAttachment(int attach)
	{
		glDrawBuffers(attach);
	}

	public int checkComplete()
	{
		bind();
		int result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		return result;
	}
}

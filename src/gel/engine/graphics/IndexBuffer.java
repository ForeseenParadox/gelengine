/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import gel.engine.utils.IOUtils;

import java.nio.IntBuffer;

public class IndexBuffer
{

	private int size;
	private int handle;
	private IntBuffer indices;
	private BufferUsage usage;

	public IndexBuffer(int size, BufferUsage usage)
	{
		this.size = size;
		this.handle = glGenBuffers();
		this.usage = usage;
		allocate();
	}

	public int getHandle()
	{
		return handle;
	}

	public int getSize()
	{
		return size;
	}

	public BufferUsage getUsage()
	{
		return usage;
	}

	public void setUsage(BufferUsage usage)
	{
		this.usage = usage;
	}

	public void bind()
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
	}

	public void unbind()
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	public void allocate()
	{
		indices = IOUtils.createIntBuffer(size);
	}

	public void setIndices(int[] indices)
	{
		this.indices.clear();
		this.indices.put(indices);
	}

	public void sendData()
	{
		bind();
		indices.flip();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, usage.getGlConst());
	}

	public void addIndex(int index)
	{
		indices.put(index);
	}

}

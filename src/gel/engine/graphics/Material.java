/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import gel.engine.graphics.texture.Texture;

public class Material
{

	private Texture texture;
	private Texture normalMap;
	private float specularIntensity;
	private float specularExponent;

	public Material()
	{
		
	}

	public Material(Texture texture, Texture normalMap, float specularIntensity, float specularExponent)
	{
		this.texture = texture;
		this.normalMap = normalMap;
		this.specularIntensity = specularIntensity;
		this.specularExponent = specularExponent;
	}

	public Texture getTexture()
	{
		return texture;
	}

	public Texture getNormalMap()
	{
		return normalMap;
	}

	public float getSpecularIntensity()
	{
		return specularIntensity;
	}

	public float getSpecularExponent()
	{
		return specularExponent;
	}

	public void setTexture(Texture texture)
	{
		this.texture = texture;
	}

	public void setNormalMap(Texture normalMap)
	{
		this.normalMap = normalMap;
	}

	public void setSpecularIntensity(float specularIntensity)
	{
		this.specularIntensity = specularIntensity;
	}

	public void setSpecularExponent(float specularExponent)
	{
		this.specularExponent = specularExponent;
	}

}

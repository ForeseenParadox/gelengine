/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import static org.lwjgl.opengl.ARBImaging.GL_TABLE_TOO_LARGE;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_EQUAL;
import static org.lwjgl.opengl.GL11.GL_FILL;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.GL_GEQUAL;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_INVALID_ENUM;
import static org.lwjgl.opengl.GL11.GL_INVALID_OPERATION;
import static org.lwjgl.opengl.GL11.GL_INVALID_VALUE;
import static org.lwjgl.opengl.GL11.GL_LEQUAL;
import static org.lwjgl.opengl.GL11.GL_LESS;
import static org.lwjgl.opengl.GL11.GL_LINE;
import static org.lwjgl.opengl.GL11.GL_NO_ERROR;
import static org.lwjgl.opengl.GL11.GL_OUT_OF_MEMORY;
import static org.lwjgl.opengl.GL11.GL_POINT;
import static org.lwjgl.opengl.GL11.GL_STACK_OVERFLOW;
import static org.lwjgl.opengl.GL11.GL_STACK_UNDERFLOW;
import static org.lwjgl.opengl.GL11.GL_VENDOR;
import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glDepthFunc;
import static org.lwjgl.opengl.GL11.glDepthMask;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGetError;
import static org.lwjgl.opengl.GL11.glGetString;
import static org.lwjgl.opengl.GL11.glPolygonMode;
import static org.lwjgl.opengl.GL30.GL_INVALID_FRAMEBUFFER_OPERATION;
import gel.engine.core.Gel;
import gel.engine.error.glerr.GLError;
import gel.engine.error.glerr.GLInvalidEnumError;
import gel.engine.error.glerr.GLInvalidFrameBufferOPError;
import gel.engine.error.glerr.GLInvalidOperationError;
import gel.engine.error.glerr.GLInvalidValueError;
import gel.engine.error.glerr.GLOutOfMemoryError;
import gel.engine.error.glerr.GLStackOverflowError;
import gel.engine.error.glerr.GLStackUnderflowError;
import gel.engine.error.glerr.GLTableTooLargeError;

public class RenderCore {

	public RenderCore() {
		if (Gel.graphics == null) {
			Gel.graphics = this;
		} else {
			throw new IllegalStateException(
					"Can not make more than one instance of RenderCore");
		}
	}

	public enum DepthFunction {
		LESS_THAN(GL_LESS), LESS_THAN_OR_EQUAL(GL_LEQUAL), EQUAL_TO(GL_EQUAL), GREATER_THAN_OR_EQUAL(
				GL_GEQUAL), GREATER_THAN(GL_GREATER);

		private int param1;

		private DepthFunction(int param1) {
			this.param1 = param1;
		}

		public int getParam1() {
			return param1;
		}
	}

	public enum PolygonMode {
		POINT(GL_POINT), LINE(GL_LINE), FILL(GL_FILL);

		private int param1;

		private PolygonMode(int param1) {
			this.param1 = param1;
		}

		public int getParam1() {
			return param1;
		}
	}

	public String getGLVersion() {
		return glGetString(GL_VERSION);
	}

	public String getVendor() {
		return glGetString(GL_VENDOR);
	}

	public int getGLError() {
		return glGetError();
	}

	public void clearColorBuffer() {
		glClear(GL_COLOR_BUFFER_BIT);
	}

	public void clearDepthBuffer() {
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	public void checkGLErrors() {

		int err = getGLError();
		boolean berr = false;

		while (err != GL_NO_ERROR) {
			berr = true;

			GLError glerr = null;

			if (err == GL_INVALID_ENUM) {
				glerr = new GLInvalidEnumError();
			} else if (err == GL_INVALID_FRAMEBUFFER_OPERATION) {
				glerr = new GLInvalidFrameBufferOPError();
			} else if (err == GL_INVALID_OPERATION) {
				glerr = new GLInvalidOperationError();
			} else if (err == GL_INVALID_VALUE) {
				glerr = new GLInvalidValueError();
			} else if (err == GL_OUT_OF_MEMORY) {
				glerr = new GLOutOfMemoryError();
			} else if (err == GL_STACK_OVERFLOW) {
				glerr = new GLStackOverflowError();
			} else if (err == GL_STACK_UNDERFLOW) {
				glerr = new GLStackUnderflowError();
			} else if (err == GL_TABLE_TOO_LARGE) {
				glerr = new GLTableTooLargeError();
			} else {
				// error not supported
			}

			StringBuilder str = new StringBuilder();

			for (StackTraceElement a : glerr.getStackTrace()) {
				str.append("\t" + a.toString() + "\n");
			}

			Gel.log.fatal(glerr.getClass().getSimpleName() + ": "
					+ glerr.toString());
			System.out.println(str.toString());

			err = glGetError();

		}

		if (berr) {
			System.exit(1);
		}
	}

	public void setClearColor(Color clearColor) {
		glClearColor(clearColor.getRed(), clearColor.getGreen(),
				clearColor.getBlue(), clearColor.getAlpha());
	}

	public void setBlendingEnabled(boolean blend) {
		if (blend) {
			glEnable(GL_BLEND);
		} else {
			glDisable(GL_BLEND);
		}
	}

	public void setBlendFunction(BlendFunction function) {
		glBlendFunc(function.getParam1(), function.getParam2());
	}

	public void setDepthFunction(DepthFunction function) {
		glDepthFunc(function.getParam1());
	}

	public void setDepthTestEnabled(boolean enabled) {
		if (enabled) {
			glEnable(GL_DEPTH_TEST);
		} else {
			glDisable(GL_DEPTH_TEST);
		}
	}

	public void setDepthMask(boolean mask) {
		glDepthMask(mask);
	}

	public void setPolygonMode(PolygonMode mode) {
		glPolygonMode(GL_FRONT_AND_BACK, mode.getParam1());
	}

}

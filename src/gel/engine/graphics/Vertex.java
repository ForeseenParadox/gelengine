/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import gel.engine.math.Vector2f;
import gel.engine.math.Vector3f;

public class Vertex
{

	private Vector3f position;
	private Color color;
	private Vector2f texCoord;
	private Vector3f normal;

	public Vertex()
	{
		this(new Vector3f(), new Color(0, 0, 0), new Vector2f());
	}

	public Vertex(Vector3f position)
	{
		this(position, new Color(0, 0, 0), new Vector2f());
	}

	public Vertex(Vector3f position, Color color)
	{
		this(position, color, new Vector2f());
	}

	public Vertex(Vector3f position, Color color, Vector2f texCoord)
	{
		this(position, color, texCoord, new Vector3f());
	}

	public Vertex(Vector3f position, Color color, Vector2f texCoord, Vector3f normal)
	{
		this.position = position;
		this.color = color;
		this.texCoord = texCoord;
		this.normal = normal;
	}

	public Vector3f getPosition()
	{
		return position;
	}

	public Color getColor()
	{
		return color;
	}

	public Vector2f getTexCoord()
	{
		return texCoord;
	}

	public Vector3f getNormal()
	{
		return normal;
	}

	public void setPosition(Vector3f position)
	{
		this.position = position;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public void setTexCoord(Vector2f texCoord)
	{
		this.texCoord = texCoord;
	}

	public void setNormal(Vector3f normal)
	{
		this.normal = normal;
	}

}

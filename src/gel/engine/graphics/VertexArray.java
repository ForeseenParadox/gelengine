/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.util.ArrayList;
import java.util.List;

public class VertexArray
{

	private int handle;
	private List<VertexAttrib> vertexAttributes;

	public VertexArray()
	{
		handle = glGenVertexArrays();
		vertexAttributes = new ArrayList<VertexAttrib>();
	}

	public int getHandle()
	{
		return handle;
	}

	public List<VertexAttrib> getVertexAttributes()
	{
		return vertexAttributes;
	}

	public int getStride()
	{
		int stride = 0;

		for (VertexAttrib attrib : vertexAttributes)
		{
			stride += attrib.getDataType().totalSize();
		}

		return stride;
	}

	public int getOffset(int attribIndex)
	{
		int offset = 0;
		for (int i = 0; i < attribIndex; i++)
		{
			offset += vertexAttributes.get(i).getDataType().totalSize();
		}
		return offset;
	}

	public void bind()
	{
		glBindVertexArray(handle);
	}

	public void update(VertexBuffer vbo)
	{

		bind();
		{
			int stride = getStride();

			// TODO: optimize
			vbo.bind();
			vbo.sendData();

			for (int i = 0; i < vertexAttributes.size(); i++)
			{
				VertexAttrib a = vertexAttributes.get(i);
				glVertexAttribPointer(a.getLocation(), a.getDataType().getElementCount(), a.getDataType().getGlType(), false, stride, getOffset(i));
			}

			vbo.unbind();
			
		}
		unbind();
	}

	public void unbind()
	{
		glBindVertexArray(0);
	}

	public void dispose()
	{
		glDeleteVertexArrays(handle);
	}

}

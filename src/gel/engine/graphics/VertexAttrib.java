/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import gel.engine.graphics.shader.DataType;

public class VertexAttrib
{

	private String name;
	private int location;
	private DataType type;

	public VertexAttrib(String name, int location, DataType type)
	{
		this.name = name;
		this.location = location;
		this.type = type;
	}

	public String getName()
	{
		return name;
	}

	public int getLocation()
	{
		return location;
	}

	public DataType getDataType()
	{
		return type;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setLocation(int location)
	{
		this.location = location;
	}

	public void setDataType(DataType type)
	{
		this.type = type;
	}
}

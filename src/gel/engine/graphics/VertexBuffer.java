/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics;

import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import gel.engine.utils.IOUtils;

import java.nio.FloatBuffer;

public class VertexBuffer
{

	private int handle;
	private int bufferSize;
	private int vertexCount;
	private FloatBuffer data;
	private IndexBuffer indexBuffer;

	private VertexArray vao;
	private Primitive prim;
	private BufferUsage usage;

	public VertexBuffer(VertexArray vao, int bufferSize, Primitive prim, BufferUsage usage)
	{
		this.vao = vao;
		this.handle = glGenBuffers();
		this.bufferSize = bufferSize;
		this.usage = usage;
		this.prim = prim;
		allocate();
	}

	public int getHandle()
	{
		return handle;
	}

	public int getBufferSize()
	{
		return bufferSize;
	}

	public int getVertexCount()
	{
		return vertexCount;
	}

	public FloatBuffer getBufferData()
	{
		return data;
	}

	public IndexBuffer getIndexBuffer()
	{
		return indexBuffer;
	}

	public VertexArray getVao()
	{
		return vao;
	}

	public Primitive getPrimitive()
	{
		return prim;
	}

	public BufferUsage getUsage()
	{
		return usage;
	}

	public void setVao(VertexArray vao)
	{
		this.vao = vao;
	}

	public void setPrimitive(Primitive prim)
	{
		this.prim = prim;
	}

	public void setBufferSize(int bufferSize)
	{
		this.bufferSize = bufferSize;
		allocate();
	}

	public void setIndexBuffer(IndexBuffer indexBuffer)
	{
		this.indexBuffer = indexBuffer;
	}

	public void removeIndexBuffer()
	{
		indexBuffer = null;
	}

	public void clearBufferData()
	{
		data.clear();
		vertexCount = 0;
	}

	public void bind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, handle);
	}

	public void unbind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	public void vertex(float[] data)
	{
		if (vertexCount < bufferSize)
		{
			for (float f : data)
			{
				this.data.put(f);
			}
			vertexCount++;
		}
	}

	public void vertex(float x, float y, float z, float r, float g, float b, float a, float u, float v, float nx, float ny, float nz)
	{
		if (vertexCount < bufferSize)
		{
			data.put(x);
			data.put(y);
			data.put(z);
			data.put(r);
			data.put(g);
			data.put(b);
			data.put(a);
			data.put(u);
			data.put(v);
			data.put(nx);
			data.put(ny);
			data.put(nz);
			vertexCount++;
		}
	}

	public void vertex(Vertex vert)
	{
		if (vert != null)
		{
			vertex(vert.getPosition().getX(), vert.getPosition().getY(), vert.getPosition().getZ(), vert.getColor().getRed(), vert.getColor().getGreen(), vert.getColor().getBlue(), vert.getColor()
					.getAlpha(), vert.getTexCoord().getX(), vert.getTexCoord().getY(), vert.getNormal().getX(), vert.getNormal().getY(), vert.getNormal().getZ());
		}
	}

	public void sendData()
	{
		glBufferData(GL_ARRAY_BUFFER, data, usage.getGlConst());
	}

	public void allocate()
	{
		int elementCount = 0;
		for (VertexAttrib attrib : vao.getVertexAttributes())
		{
			elementCount += attrib.getDataType().getElementCount();
		}
		data = IOUtils.createFloatBuffer(bufferSize * elementCount);
	}

	public void dispose()
	{
		glDeleteBuffers(handle);
	}

	public void updateBufferData()
	{
		data.flip();

		vao.update(this);
	}

	public void render()
	{
		if (indexBuffer == null)
		{
			render(vertexCount);
		} else
		{
			render(indexBuffer.getSize());
		}
	}

	public void render(int indexCount)
	{

		// TODO: optimize
		bind();
		{
			vao.bind();

			for (VertexAttrib a : vao.getVertexAttributes())
			{
				glEnableVertexAttribArray(a.getLocation());
			}

			if (indexBuffer == null)
			{
				glDrawArrays(prim.getGlConst(), 0, indexCount);
			} else
			{
				indexBuffer.bind();
				glDrawElements(prim.getGlConst(), indexCount, GL_UNSIGNED_INT, 0);
			}

			for (VertexAttrib a : vao.getVertexAttributes())
			{
				glDisableVertexAttribArray(a.getLocation());
			}

			vao.unbind();
		}
		unbind();
	}
}

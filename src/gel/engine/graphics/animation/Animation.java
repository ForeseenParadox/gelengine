package gel.engine.graphics.animation;

import gel.engine.graphics.texture.Texture;
import gel.engine.graphics.texture.TextureRegion;
import gel.engine.utils.Delay;

import java.util.ArrayList;
import java.util.List;

public class Animation
{

	private List<Frame> frames;
	private int currentFrame;
	private Delay frameDelay;
	private boolean started;
	private boolean paused;

	public Animation()
	{
		frames = new ArrayList<Frame>();
		currentFrame = 0;
		frameDelay = new Delay(0);
		started = false;
		paused = false;
	}

	public Animation(Texture texture, int frameWidth, int frameHeight, long frameLength)
	{
		this(texture, frameWidth, frameHeight, 0, 0, texture.getWidth(), texture.getHeight(), frameLength, null);
	}

	public Animation(Texture texture, int frameWidth, int frameHeight, int startX, int startY, int width, int height, long frameLength, int[] indices)
	{
		frames = new ArrayList<Frame>();
		List<Frame> unindexedFrames = new ArrayList<Frame>();

		for (int y = startY; y < height; y += frameHeight)
		{
			for (int x = startX; x < width; x += frameWidth)
			{
				unindexedFrames.add(new Frame(new TextureRegion(texture, x, y, frameWidth, frameHeight), frameLength));
			}
		}

		if (indices != null)
		{
			for (int i : indices)
			{
				frames.add(unindexedFrames.get(i));
			}
		} else
		{
			frames = unindexedFrames;
		}

		frameDelay = new Delay(frameLength);
	}

	public List<Frame> getFrames()
	{
		return frames;
	}

	public Frame getCurrentFrame()
	{
		return frames.get(currentFrame);
	}

	public int getCurrentFrameIndex()
	{
		return currentFrame;
	}

	public Animation start()
	{
		started = true;
		return this;
	}

	public Animation stop()
	{
		started = false;
		currentFrame = 0;
		frameDelay.setDelay(getCurrentFrame().getLength());
		return this;
	}

	public Animation pause()
	{
		paused = true;
		return this;
	}

	public Animation resume()
	{
		paused = false;
		return this;
	}

	public void update()
	{
		if (frames.size() > 0 && started && !paused)
		{
			if (frameDelay == null)
			{
				frameDelay = new Delay(frames.get(currentFrame).getLength());
			}
			if (frameDelay.isReady())
			{
				currentFrame++;

				if (currentFrame >= frames.size())
				{
					currentFrame = 0;
				}
			}
		} else
		{
			// exception
		}
	}

}

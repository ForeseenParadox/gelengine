package gel.engine.graphics.animation;

import gel.engine.graphics.texture.TextureRegion;

public class Frame
{

	private TextureRegion texture;
	private long length;

	public Frame(TextureRegion texture, long length)
	{
		this.texture = texture;
		this.length = length;
	}

	public TextureRegion getTexture()
	{
		return texture;
	}

	public long getLength()
	{
		return length;
	}

	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

	public void setLength(long length)
	{
		this.length = length;
	}

}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.batch;

import gel.engine.core.Gel;
import gel.engine.graphics.BufferUsage;
import gel.engine.graphics.Color;
import gel.engine.graphics.Primitive;
import gel.engine.graphics.VertexBuffer;
import gel.engine.graphics.shader.forward.ForwardShape;
import gel.engine.math.Matrix4f;
import gel.engine.math.Vector2f;
import gel.engine.math.Vector4f;
import gel.engine.utils.MathUtils;

public class ShapeBatch
{

	public static final int CIRCLE_VERTICES = 30;

	private boolean began;
	private int maxDraws;
	private int draws;
	private int flushCount;
	private ForwardShape shader;
	private VertexBuffer vbo;
	private Matrix4f viewProjectionMatrix;
	private Matrix4f modelMatrix;
	private Color color;

	// the current primitive being rendered
	private Primitive primitive;

	public ShapeBatch()
	{
		this(50000);
	}

	public ShapeBatch(int maxDraws)
	{

		shader = new ForwardShape("shaders/shapeShader.vs", "shaders/shapeShader.fs", true);

		primitive = Primitive.QUAD;
		vbo = new VertexBuffer(shader.getVertexArray(), maxDraws * 4, primitive, BufferUsage.STREAM);

		viewProjectionMatrix = MathUtils.orthoMatrix(0, Gel.window.getWidth(), 0, Gel.window.getHeight(), -1, 1);
		modelMatrix = new Matrix4f();
		modelMatrix.setIdentity();
		color = Color.WHITE;

	}

	public boolean hasBegan()
	{
		return began;
	}

	public int getMaxDraws()
	{
		return maxDraws;
	}

	public int getDraws()
	{
		return draws;
	}

	public int getFlushCount()
	{
		return flushCount;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	// rendering methods

	public void renderLine(float x1, float y1, float x2, float y2)
	{

		if (!began)
			return;

		if (primitive == Primitive.LINE)
		{
			vbo.vertex(x1, y1, 0, color.getRed(), color.getBlue(), color.getGreen(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(x2, y2, 0, color.getRed(), color.getBlue(), color.getGreen(), 1, 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = Primitive.LINE;
			renderLine(x1, y1, x2, y2);
		}

	}

	public void renderLineStrip(float x, float y)
	{
		if (!began)
			return;

		if (primitive == Primitive.LINE_STRIP)
		{
			vbo.vertex(x, y, 0, color.getRed(), color.getBlue(), color.getGreen(), 1, 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = Primitive.LINE_STRIP;
			renderLineStrip(x, y);
		}
	}

	public void renderLineLoop(float x, float y)
	{
		if (!began)
			return;

		if (primitive == Primitive.LINE_LOOP)
		{
			vbo.vertex(x, y, 0, color.getRed(), color.getBlue(), color.getGreen(), 1, 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = Primitive.LINE_LOOP;
			renderLineLoop(x, y);
		}
	}

	public void renderQuad(Vector2f point1, Vector2f point2, Vector2f point3, Vector2f point4)
	{
		if (!began)
			return;

		if (primitive == Primitive.QUAD)
		{
			vbo.vertex(point1.getX(), point1.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(point2.getX(), point2.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(point3.getX(), point3.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(point4.getX(), point4.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = Primitive.QUAD;
			renderQuad(point1, point2, point3, point4);
		}
	}

	public void renderRect(float x, float y, float width, float height, float originX, float originY, float rot)
	{
		if (!began)
			return;

		if (primitive == Primitive.LINE)
		{
			Vector4f v1 = new Vector4f(0, 0, 0, 1);
			Vector4f v2 = new Vector4f(0, height, 0, 1);
			Vector4f v3 = new Vector4f(width, height, 0, 1);
			Vector4f v4 = new Vector4f(width, 0, 0, 1);

			if (rot != 0)
			{

				v1.translate(-originX, -originY, 0, 0);
				v2.translate(-originX, -originY, 0, 0);
				v3.translate(-originX, -originY, 0, 0);
				v4.translate(-originX, -originY, 0, 0);

				Matrix4f mat = MathUtils.rotationZ(rot);
				v1.set(v1.multiply(mat));
				v2.set(v2.multiply(mat));
				v3.set(v3.multiply(mat));
				v4.set(v4.multiply(mat));

				v1.translate(originX, originY, 0, 0);
				v2.translate(originX, originY, 0, 0);
				v3.translate(originX, originY, 0, 0);
				v4.translate(originX, originY, 0, 0);
			}

			v1.translate(x, y, 0, 0);
			v2.translate(x, y, 0, 0);
			v3.translate(x, y, 0, 0);
			v4.translate(x, y, 0, 0);

			vbo.vertex(v1.getX(), v1.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(v2.getX(), v2.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);

			vbo.vertex(v2.getX(), v2.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(v3.getX(), v3.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);

			vbo.vertex(v3.getX(), v3.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(v4.getX(), v4.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);

			vbo.vertex(v4.getX(), v4.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(v1.getX(), v1.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);

		} else
		{
			flush();
			primitive = Primitive.LINE;
			renderRect(x, y, width, height, originX, originY, rot);
		}
	}

	public void renderFilledRect(float x, float y, float width, float height)
	{

		if (!began)
			return;

		if (primitive == Primitive.QUAD)
		{
			vbo.vertex(x, y, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(x, y + height, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(x + width, y + height, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			vbo.vertex(x + width, y, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = Primitive.QUAD;
			renderFilledRect(x, y, width, height);
		}
	}

	public void renderCircle(float x, float y, float rad)
	{

		if (!began)
			return;

		if (primitive == Primitive.LINE)
		{
			float inc = 360f / CIRCLE_VERTICES;
			for (float i = 0; i <= 360; i += inc)
			{
				float x_1 = x + (float) Math.cos(Math.toRadians(i)) * rad;
				float y_1 = y + (float) Math.sin(Math.toRadians(i)) * rad;
				float x_2 = x + (float) Math.cos(Math.toRadians(i - inc)) * rad;
				float y_2 = y + (float) Math.sin(Math.toRadians(i - inc)) * rad;

				vbo.vertex(x_1, y_1, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
				vbo.vertex(x_2, y_2, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			}
		} else
		{
			flush();
			primitive = Primitive.LINE;
			renderCircle(x, y, rad);
		}

	}

	public void renderPolygon(Vector2f[] vertices)
	{
		if (!began)
			return;

		for (int i = 0; i < vertices.length; i++)
		{
			Vector2f v1 = vertices[i];
			Vector2f v2 = null;
			vbo.vertex(v1.getX(), v1.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

			if (i == vertices.length - 1)
			{
				v2 = vertices[0];
			} else
			{
				v2 = vertices[i + 1];
			}

			vbo.vertex(v2.getX(), v2.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

		}
	}

	public void renderFilledCircle(float x, float y, float rad)
	{
		if (!began)
			return;

		if (primitive == Primitive.TRIANGLE)
		{
			float inc = 360f / CIRCLE_VERTICES;

			for (float i = 0; i <= 360; i += inc)
			{
				vbo.vertex(x, y, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

				float x_1 = x + (float) Math.cos(Math.toRadians(i)) * rad;
				float y_1 = y + (float) Math.sin(Math.toRadians(i)) * rad;
				float x_2 = x + (float) Math.cos(Math.toRadians(i - inc)) * rad;
				float y_2 = y + (float) Math.sin(Math.toRadians(i - inc)) * rad;

				vbo.vertex(x_1, y_1, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
				vbo.vertex(x_2, y_2, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, 0, 0, 0, 0, 1);
			}
		} else
		{
			flush();
			primitive = Primitive.TRIANGLE;
			renderFilledCircle(x, y, rad);
		}
	}

	public void begin()
	{
		if (!began)
		{
			began = true;
			draws = 0;
			shader.updateUniforms(viewProjectionMatrix, modelMatrix);
		} else
		{
			// throw exception
		}
	}

	public void end()
	{
		if (began)
		{
			began = false;
			flush();
			flushCount++;
		} else
		{
			// throw exception
		}
	}

	public void flush()
	{
		shader.bind();

		vbo.setPrimitive(primitive);
		vbo.updateBufferData();
		vbo.render();
		vbo.clearBufferData();

		draws = 0;

		flushCount++;
	}
}

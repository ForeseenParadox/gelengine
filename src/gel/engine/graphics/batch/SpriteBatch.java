/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.batch;

import gel.engine.core.Gel;
import gel.engine.graphics.BufferUsage;
import gel.engine.graphics.Color;
import gel.engine.graphics.IndexBuffer;
import gel.engine.graphics.Primitive;
import gel.engine.graphics.VertexBuffer;
import gel.engine.graphics.lighting.AmbientLight;
import gel.engine.graphics.shader.ShaderProgram;
import gel.engine.graphics.shader.forward.ForwardAmbient2;
import gel.engine.graphics.texture.Texture;
import gel.engine.graphics.texture.TextureRegion;
import gel.engine.math.Matrix4f;
import gel.engine.math.Vector2f;
import gel.engine.math.Vector4f;
import gel.engine.utils.MathUtils;

public class SpriteBatch
{

	private boolean began;
	private int maxDraws;
	private int drawCount;
	private int flushCount;
	private Texture currentTexture;

	private ForwardAmbient2 shader;
	private VertexBuffer vbo;
	private Matrix4f viewProjectionMatrix;
	private Matrix4f modelMatrix;
	private AmbientLight ambient;
	private Color tint = Color.WHITE;

	public SpriteBatch()
	{
		this(50000);
	}

	public SpriteBatch(int maxDraws)
	{
		this.maxDraws = maxDraws;

		shader = new ForwardAmbient2("shaders/forward-vertex2.vs", "shaders/forward-ambient2.fs", true);

		vbo = new VertexBuffer(shader.getVertexArray(), maxDraws * 4, Primitive.TRIANGLE, BufferUsage.STREAM);

		IndexBuffer indexBuffer = new IndexBuffer(maxDraws * 6, BufferUsage.STATIC);

		int[] indices = new int[maxDraws * 6];

		for (int i = 0, j = 0; i < maxDraws * 6; i += 6, j += 4)
		{
			indices[i + 0] = j + 0;
			indices[i + 1] = j + 1;
			indices[i + 2] = j + 2;
			indices[i + 3] = j + 2;
			indices[i + 4] = j + 3;
			indices[i + 5] = j + 0;
		}

		indexBuffer.setIndices(indices);
		indexBuffer.sendData();

		vbo.setIndexBuffer(indexBuffer);

		viewProjectionMatrix = MathUtils.orthoMatrix(0, Gel.window.getWidth(), 0, Gel.window.getHeight(), -1, 1);

		modelMatrix = new Matrix4f();
		modelMatrix.setIdentity();

		ambient = new AmbientLight(Color.WHITE);
	}

	public boolean hasBegan()
	{
		return began;
	}

	public int getMaxDraws()
	{
		return maxDraws;
	}

	public int getDrawCount()
	{
		return drawCount;
	}

	public int getFlushCount()
	{
		return flushCount;
	}

	public Texture getCurrentTexture()
	{
		return currentTexture;
	}

	public ShaderProgram getShader()
	{
		return shader;
	}

	public VertexBuffer getVBO()
	{
		return vbo;
	}

	public Matrix4f getViewProjectionMatrix()
	{
		return viewProjectionMatrix;
	}

	public Matrix4f getModelMatrix()
	{
		return modelMatrix;
	}

	public AmbientLight getAmbient()
	{
		return ambient;
	}

	public Color getTint()
	{
		return tint;
	}

	public void setViewProjectionMatrix(Matrix4f viewProjectionMatrix)
	{
		this.viewProjectionMatrix = viewProjectionMatrix;
	}

	public void setTint(Color tint)
	{
		this.tint = tint;
	}

	public void begin()
	{
		if (!began)
		{
			began = true;

			drawCount = 0;

			shader.updateUniforms(viewProjectionMatrix, modelMatrix, ambient.getColor().toVector3f());
		} else
		{

		}
	}

	public void end()
	{
		if (began)
		{
			began = false;
			flush();
			flushCount = 0;
		} else
		{

		}
	}

	public void render(Texture tex, float x, float y)
	{
		render(tex, x, y, 0);
	}

	public void render(TextureRegion tex, float x, float y)
	{
		render(tex, 0, 0, x, y, 0, 1, 1);
	}

	public void render(Texture tex, float x, float y, float rotation)
	{
		render(tex, x, y, rotation, 1, 1);
	}

	public void render(Texture tex, float x, float y, float rotation, float scaleX, float scaleY)
	{
		render(tex, 0, 0, x, y, rotation, scaleX, scaleY);
	}

	public void render(Texture tex, float originX, float originY, float x, float y, float rotation, float scaleX, float scaleY)
	{
		render(new TextureRegion(tex, 0, 0, tex.getWidth(), tex.getHeight()), originX, originY, x, y, rotation, scaleX, scaleY);
	}

	public void render(TextureRegion region, float originX, float originY, float x, float y, float rotation, float scaleX, float scaleY)
	{
		render(region.getTexture(), new Vector2f[] { region.getUv1(), region.getUv2(), region.getUv3(), region.getUv4() }, originX, originY, x, y, rotation, scaleX, scaleY);
	}

	public void render(Texture tex, Vector2f[] texCoords, float originX, float originY, float x, float y, float rotation, float scaleX, float scaleY)
	{

		if (!began)
			return;

		if (tex == null)
			return;

		float width = (texCoords[2].getX() - texCoords[0].getX()) * tex.getWidth();
		float height = (texCoords[2].getY() - texCoords[0].getY()) * tex.getHeight();

		if (x + width < 0 || x > Gel.window.getWidth() || y + height < 0 || y > Gel.window.getHeight())
			return;

		if (currentTexture == null)
			currentTexture = tex;

		if (currentTexture == tex && drawCount < maxDraws)
		{

			Vector4f V1 = new Vector4f(-originX, -originY, 0, 1);
			Vector4f V2 = new Vector4f(-originX, height - originY, 0, 1);
			Vector4f V3 = new Vector4f(width - originX, height - originY, 0, 1);
			Vector4f V4 = new Vector4f(width - originX, -originY, 0, 1);

			V1.mult(scaleX, scaleY, 1, 1);
			V2.mult(scaleX, scaleY, 1, 1);
			V3.mult(scaleX, scaleY, 1, 1);
			V4.mult(scaleX, scaleY, 1, 1);

			if (rotation != 0)
			{
				Matrix4f mat = MathUtils.rotationZ(rotation);
				V1.set(V1.multiply(mat));
				V2.set(V2.multiply(mat));
				V3.set(V3.multiply(mat));
				V4.set(V4.multiply(mat));
			}

			V1.translate(originX + x, originY + y, 0, 0);
			V2.translate(originX + x, originY + y, 0, 0);
			V3.translate(originX + x, originY + y, 0, 0);
			V4.translate(originX + x, originY + y, 0, 0);

			vbo.vertex(V1.getX(), V1.getY(), 0, tint.getRed(), tint.getGreen(), tint.getBlue(), 1, texCoords[0].getX(), texCoords[0].getY(), 0, 0, -1);
			vbo.vertex(V2.getX(), V2.getY(), 0, tint.getRed(), tint.getGreen(), tint.getBlue(), 1, texCoords[1].getX(), texCoords[1].getY(), 0, 0, -1);
			vbo.vertex(V3.getX(), V3.getY(), 0, tint.getRed(), tint.getGreen(), tint.getBlue(), 1, texCoords[2].getX(), texCoords[2].getY(), 0, 0, -1);
			vbo.vertex(V4.getX(), V4.getY(), 0, tint.getRed(), tint.getGreen(), tint.getBlue(), 1, texCoords[3].getX(), texCoords[3].getY(), 0, 0, -1);

			drawCount++;

		} else
		{
			// flush the current texture, then start the rendering of the recent
			// texture
			flush();
			drawCount = 0;
			currentTexture = tex;
			render(tex, texCoords, originX, originY, x, y, rotation, scaleX, scaleY);
		}

	}

	public void flush()
	{
		shader.bind();

		if (currentTexture != null)
		{
			currentTexture.bind();
		}

		vbo.updateBufferData();
		vbo.render(drawCount * 6);
		vbo.clearBufferData();

		flushCount++;
	}

}

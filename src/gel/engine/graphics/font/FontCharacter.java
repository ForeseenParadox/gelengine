/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.font;

public class FontCharacter
{

	private char character;
	private int width;
	private int x;
	private int y;

	public FontCharacter(char character, int width, int x, int y)
	{
		this.character = character;
		this.width = width;
		this.x = x;
		this.y = y;
	}

	public char getCharacter()
	{
		return character;
	}

	public int getWidth()
	{
		return width;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public void setCharacter(char character)
	{
		this.character = character;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

}

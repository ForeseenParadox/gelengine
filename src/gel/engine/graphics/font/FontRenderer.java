/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.font;

import gel.engine.core.Gel;
import gel.engine.graphics.BufferUsage;
import gel.engine.graphics.Color;
import gel.engine.graphics.Primitive;
import gel.engine.graphics.VertexBuffer;
import gel.engine.graphics.shader.forward.ForwardText;
import gel.engine.graphics.texture.TextureRegion;
import gel.engine.math.Matrix4f;
import gel.engine.math.Vector2f;
import gel.engine.utils.MathUtils;

public class FontRenderer
{

	private ForwardText shader;
	private VertexBuffer vbo;
	private Color color;
	private Matrix4f viewProjectionMatrix;
	private Matrix4f modelMatrix;

	public FontRenderer()
	{

		shader = new ForwardText("shaders/forward-vertex2.vs", "shaders/forward-text.fs", true);

		vbo = new VertexBuffer(shader.getVertexArray(), 50000, Primitive.QUAD, BufferUsage.STREAM);

		viewProjectionMatrix = MathUtils.orthoMatrix(0, Gel.window.getWidth(), 0, Gel.window.getHeight(), -1, 1);
		modelMatrix = new Matrix4f();
		modelMatrix.setIdentity();

		color = Color.WHITE;
	}

	public Color getColor()
	{
		return color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public void render(TrueTypeFont font, String text, float x, float y)
	{

		shader.updateUniforms(viewProjectionMatrix, modelMatrix);
		shader.bind();

		font.getFontTexture().bind();

		float currentX = x;
		int cHeight = font.getCharacterHeight();

		for (char c : text.toCharArray())
		{

			int cWidth = font.getCharacterWidth(c);
			int charX = font.getCharacterX(c);
			int charY = font.getFontTexture().getHeight() - font.getCharacterY(c);

			TextureRegion region = new TextureRegion(font.getFontTexture(), charX, charY, cWidth, cHeight);

			Vector2f uv1 = region.getUv1();
			Vector2f uv2 = region.getUv2();
			Vector2f uv3 = region.getUv3();
			Vector2f uv4 = region.getUv4();

			vbo.vertex(currentX, y, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, uv1.getX(), uv1.getY(), 0, 0, 1);
			vbo.vertex(currentX, y + cHeight, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, uv2.getX(), uv2.getY(), 0, 0, 1);
			vbo.vertex(currentX + cWidth, y + cHeight, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, uv3.getX(), uv3.getY(), 0, 0, 1);
			vbo.vertex(currentX + cWidth, y, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, uv4.getX(), uv4.getY(), 0, 0, 1);

			currentX += cWidth;
		}

		vbo.updateBufferData();
		vbo.render(text.length() * 4);
		vbo.clearBufferData();
	}
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.font;

import java.awt.GraphicsEnvironment;

public class FontType
{

	public static final FontType ARIAL = new FontType("Arial");
	public static final FontType CALIBRI = new FontType("Calibri");
	public static final FontType COMIC_SAND = new FontType("Comic Sans");

	private String fontName;

	public FontType(String fontName)
	{
		this.fontName = fontName;
	}

	public String getFontName()
	{
		return fontName;
	}

	public boolean exists()
	{
		for (String x : GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames())
		{
			if (x.equals(fontName))
			{
				return true;
			}
		}
		return false;
	}
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.font;

import gel.engine.core.Gel;
import gel.engine.graphics.Bitmap;
import gel.engine.graphics.texture.Texture;
import gel.engine.utils.MathUtils;

import java.awt.*;
import java.awt.image.BufferedImage;

public class TrueTypeFont
{

	public static final int FONT_TEXTURE_WIDTH = 512;

	private FontType fontType;
	private int fontSize;
	private boolean antiAlias;
	private FontCharacter[] fontCharacters;
	private int characterHeight;
	private Texture fontTexture;

	public TrueTypeFont(FontType fontType, int fontSize, boolean antiAlias)
	{

		boolean found = true;
		if (!fontType.exists())
		{
			found = false;
			Gel.log.warning(fontType.getFontName() + " was not found, loading Arial by default");
			fontType = FontType.ARIAL;
		}

		this.fontType = fontType;
		this.fontSize = fontSize;
		this.antiAlias = antiAlias;
		this.fontCharacters = new FontCharacter[256];

		BufferedImage renderedFontImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) renderedFontImage.createGraphics();
		Font awtFont = new Font(found ? fontType.getFontName() : "Arial", Font.PLAIN, fontSize);
		FontMetrics awtFontMetrics = g.getFontMetrics(awtFont);

		// get largest character height
		characterHeight = awtFontMetrics.getMaxAscent() + awtFontMetrics.getMaxDescent();

		// get row count and position characters
		int rowCount = 0;

		for (int i = 0, x = 0, y = 0; i < 256; i++)
		{

			int width = (int) awtFontMetrics.charWidth((char) i);
			int widthNext = (int) awtFontMetrics.charWidth((char) (i + 1));

			fontCharacters[i] = new FontCharacter((char) i, width, x, y);

			x += width;

			if (x + widthNext >= FONT_TEXTURE_WIDTH)
			{
				x = 0;
				y += characterHeight;
				rowCount++;
			}

		}

		// find correct image height, needs to be a power of 2
		int height = rowCount * characterHeight;

		if (!MathUtils.isPowerOfTwo(rowCount * characterHeight))
		{
			int pow = 0;

			// find the next power of 2
			for (int power = 0; Math.pow(2, power - 1) < height; power++)
			{
				pow = power;
			}

			height = (int) Math.pow(2, pow);
		}
		
		renderedFontImage = new BufferedImage(FONT_TEXTURE_WIDTH, height, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) renderedFontImage.getGraphics();

		if (antiAlias)
		{
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		}

		// render characters to buffered image
		for (int i = 0; i < 256; i++)
		{
			g.setColor(Color.WHITE);
			g.setFont(awtFont);
			g.drawString(String.valueOf((char) i), fontCharacters[i].getX(), fontCharacters[i].getY() - awtFontMetrics.getMaxDescent());
		}

		fontTexture = new Texture(new Bitmap(renderedFontImage).flipY());
		fontTexture.load();

	}

	public FontType getFontType()
	{
		return fontType;
	}

	public int getFontSize()
	{
		return fontSize;
	}

	public int getCharacterHeight()
	{
		return characterHeight;
	}

	public FontCharacter[] getFontCharacters()
	{
		return fontCharacters;
	}

	public Texture getFontTexture()
	{
		return fontTexture;
	}

	public int getCharacterX(char c)
	{
		return fontCharacters[(int) c].getX();
	}

	public int getCharacterY(char c)
	{
		return fontCharacters[(int) c].getY();
	}

	public int getCharacterWidth(char c)
	{
		return fontCharacters[(int) c].getWidth();
	}

	public boolean isAntiAliased()
	{
		return antiAlias;
	}

	public int getStringWidth(String string)
	{
		int width = 0;
		for (char c : string.toCharArray())
		{
			width += getCharacterWidth(c);
		}
		return width;
	}

}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.lighting;

import gel.engine.graphics.Color;
import gel.engine.math.Vector2f;
import gel.engine.math.Vector3f;

public class DirectionalLight extends BaseLight
{

	private Vector3f direction;
	private float intensity;

	public DirectionalLight(Color color, Vector2f direction, float intensity)
	{
		this(color, new Vector3f(direction, 0), intensity);
	}

	public DirectionalLight(Color color, Vector3f direction, float intensity)
	{
		super(color);
		this.direction = direction;
		this.intensity = intensity;
	}

	public Vector3f getDirection()
	{
		return direction;
	}

	public float getIntensity()
	{
		return intensity;
	}

	public void setDirection(Vector3f direction)
	{
		this.direction = direction;
	}

	public void setIntensity(float intensity)
	{
		this.intensity = intensity;
	}

}

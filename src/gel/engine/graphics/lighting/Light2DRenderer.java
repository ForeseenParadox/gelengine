/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.lighting;

import gel.engine.core.Gel;
import gel.engine.graphics.BlendFunction;
import gel.engine.graphics.BufferUsage;
import gel.engine.graphics.Primitive;
import gel.engine.graphics.VertexBuffer;
import gel.engine.graphics.batch.ShapeBatch;
import gel.engine.graphics.shader.forward.ForwardPoint2;
import gel.engine.math.Matrix4f;
import gel.engine.math.Vector2f;
import gel.engine.physics.BoundingBox2D;
import gel.engine.utils.MathUtils;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

public class Light2DRenderer
{

	public static final float SHADOW_SCALE_FACTOR = 800f;

	private ForwardPoint2 pointShader;
	private VertexBuffer pointVBO;
	private ShapeBatch occluderRenderer;
	private Matrix4f viewProjectionMatrix;
	private Matrix4f modelMatrix;

	public final List<BoundingBox2D> occluders = new ArrayList<BoundingBox2D>();

	public Light2DRenderer()
	{
		pointShader = new ForwardPoint2("shaders/forward-vertex2.vs", "shaders/forward-point2.fs", true);

		// change later to batch lights, this as it is is really inefficient
		pointVBO = new VertexBuffer(pointShader.getVertexArray(), 4, Primitive.QUAD, BufferUsage.STREAM);

		occluderRenderer = new ShapeBatch();

		viewProjectionMatrix = MathUtils.orthoMatrix(0, Gel.window.getWidth(), 0, Gel.window.getHeight(), -1, 1);
		modelMatrix = new Matrix4f();
		modelMatrix.setIdentity();
	}

    public void renderPointLight(PointLight light)
    {
        renderPointLight(light, false);
    }

	public void renderPointLight(PointLight light, boolean shadows)
	{
		renderPointLight(light, null, shadows);
	}

	public void renderPointLight(PointLight light, Vector2f translate, boolean shadows)
	{

		if (shadows)
		{
			// set up stenciling
			GL11.glEnable(GL11.GL_STENCIL_TEST);
			GL11.glClearStencil(0);
			GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);
			GL11.glStencilFunc(GL11.GL_ALWAYS, 1, 1);
			GL11.glStencilOp(GL11.GL_REPLACE, GL11.GL_REPLACE, GL11.GL_REPLACE);
			GL11.glColorMask(false, false, false, false);
			GL11.glDepthMask(true);

			occluderRenderer.begin();

			for (BoundingBox2D box : occluders)
			{
				Vector2f[] vertices = new Vector2f[4];
				vertices[0] = new Vector2f(box.getPosition().getX(), box.getPosition().getY());
				vertices[1] = new Vector2f(box.getPosition().getX(), box.getPosition().getY() + box.getHeight());
				vertices[2] = new Vector2f(box.getPosition().getX() + box.getWidth(), box.getPosition().getY() + box.getHeight());
				vertices[3] = new Vector2f(box.getPosition().getX() + box.getWidth(), box.getPosition().getY());

				Vector2f lightPos = new Vector2f(light.getPosition().getX(), light.getPosition().getY());

				for (int i = 0; i < vertices.length; i++)
				{
					Vector2f vert = vertices[i];
					Vector2f next = vertices[(i + 1) % vertices.length];
					Vector2f edge = next.subtract(vert);
					Vector2f normal = new Vector2f(-edge.getY(), edge.getX());
					Vector2f lightToVertex = vert.subtract(lightPos).normalized();
					if (normal.dotProduct(lightToVertex) < 0)
					{
						Vector2f point1 = vert.add(vert.subtract(lightPos).multiply(SHADOW_SCALE_FACTOR));
						Vector2f point2 = next.add(next.subtract(lightPos).multiply(SHADOW_SCALE_FACTOR));

						occluderRenderer.renderQuad(next, point2, point1, vert);
					}
				}

			}

			occluderRenderer.end();

			GL11.glColorMask(true, true, true, true);
			GL11.glDepthMask(false);
			GL11.glStencilFunc(GL11.GL_NOTEQUAL, 1, 1);
			GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_KEEP);
		}

		Gel.graphics.setBlendingEnabled(true);
		Gel.graphics.setBlendFunction(BlendFunction.ADDITIVE);
		
		if (translate != null)
		{
			pointShader.updateUniforms(viewProjectionMatrix, modelMatrix, light, translate);
		} else
		{
			pointShader.updateUniforms(viewProjectionMatrix, modelMatrix, light);
		}
		pointShader.bind();

		pointVBO.vertex(0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, -1);
		pointVBO.vertex(0, Gel.window.getHeight(), 0, 1, 1, 1, 1, 0, 0, 0, 0, -1);
		pointVBO.vertex(Gel.window.getWidth(), Gel.window.getHeight(), 0, 1, 1, 1, 1, 0, 0, 0, 0, -1);
		pointVBO.vertex(Gel.window.getWidth(), 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, -1);

		pointVBO.updateBufferData();
		pointVBO.render();
		pointVBO.clearBufferData();
		Gel.graphics.setBlendingEnabled(false);

		GL11.glDisable(GL11.GL_STENCIL_TEST);

	}
}

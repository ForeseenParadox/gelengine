/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.lighting;

import gel.engine.graphics.Color;
import gel.engine.math.Vector3f;

public class PointLight extends BaseLight
{

	private Vector3f position;
	private float radius;
	private float intensity;
	private float constAtten;
	private float linearAtten;
	private float quadAtten;

	public PointLight(Vector3f position, Color color, float radius, float intensity, float constAtten, float linearAtten, float quadAtten)
	{
		super(color);
		this.position = position;
		this.radius = radius;
		this.intensity = intensity;
		this.constAtten = constAtten;
		this.linearAtten = linearAtten;
		this.quadAtten = quadAtten;

	}

	public Vector3f getPosition()
	{
		return position;
	}

	public float getRadius()
	{
		return radius;
	}

	public float getIntensity()
	{
		return intensity;
	}

	public float getConstAtten()
	{
		return constAtten;
	}

	public float getLinearAtten()
	{
		return linearAtten;
	}

	public float getQuadAtten()
	{
		return quadAtten;
	}

	public void setPosition(Vector3f position)
	{
		this.position = position;
	}

	public void setRadius(float radius)
	{
		this.radius = radius;
	}

	public void setIntensity(float intensity)
	{
		this.intensity = intensity;
	}

	public void setConstAtten(float constAtten)
	{
		this.constAtten = constAtten;
	}

	public void setLinearAtten(float linearAtten)
	{
		this.linearAtten = linearAtten;
	}

	public void setQuadAtten(float quadAtten)
	{
		this.quadAtten = quadAtten;
	}

}

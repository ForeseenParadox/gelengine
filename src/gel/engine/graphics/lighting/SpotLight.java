/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.lighting;

import gel.engine.graphics.Color;
import gel.engine.math.Vector3f;

public class SpotLight extends PointLight
{

	public SpotLight(Vector3f position, Color color, float radius, float intensity, float constAtten, float linearAtten, float quadAtten)
	{
		super(position, color, radius, intensity, constAtten, linearAtten, quadAtten);
	}

	private float theta;
	private float direction;
/*
	public SpotLight(Vector3f position, Color color, float radius, float constAtten, float linearAtten, float quadAtten, float theta, float direction)
	{
		super(position, color, radius, constAtten, linearAtten, quadAtten);
		this.theta = theta;
		this.direction = direction;
	}
*/
	public float getTheta()
	{
		return theta;
	}

	public float getDirection()
	{
		return direction;
	}

	public void setTheta(float theta)
	{
		this.theta = theta;
	}

	public void setDirection(float direction)
	{
		this.direction = direction;
	}

}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.mesh;

import gel.engine.graphics.Vertex;
import gel.engine.graphics.VertexBuffer;
import gel.engine.math.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class Mesh
{

	private VertexBuffer vbo;
	private List<Vertex> vertices;

	public Mesh()
	{
		// load default attribs
	}

	public Mesh(VertexBuffer vbo)
	{
		vertices = new ArrayList<Vertex>();
		this.vbo = vbo;
	}

	public VertexBuffer getVertexBuffer()
	{
		return vbo;
	}

	public List<Vertex> getVertices()
	{
		return vertices;
	}

	public void setVertexBuffer(VertexBuffer vbo)
	{
		this.vbo = vbo;
	}

	public void updateVBOWithData()
	{
		for (Vertex vertex : vertices)
		{
			vbo.vertex(vertex);
		}
		vbo.updateBufferData();
	}

	public void render()
	{
		vbo.render();
	}

	public void calcNormals(List<Integer> indices, boolean triangulated)
	{
		for (int i = 0; i < indices.size(); i += 3)
		{
			if (i <= indices.size() - 3)
			{
				int i0 = indices.get(i + 0);
				int i1 = indices.get(i + 1);
				int i2 = indices.get(i + 2);

				Vector3f edge1 = vertices.get(i1).getPosition().subtract(vertices.get(i0).getPosition());
				Vector3f edge2 = vertices.get(i2).getPosition().subtract(vertices.get(i0).getPosition());

				Vector3f normal = edge1.cross(edge2);

				vertices.get(i0).setNormal(vertices.get(i0).getNormal().add(normal));
				vertices.get(i1).setNormal(vertices.get(i1).getNormal().add(normal));
				vertices.get(i2).setNormal(vertices.get(i2).getNormal().add(normal));
			}
		}

		for (Vertex vert : vertices)
		{
			vert.getNormal().normalize();
		}
	}
}

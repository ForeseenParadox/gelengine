/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.model;

public class OBJIndex
{

	private int position;
	private int texCoord;
	private int normal;

	public OBJIndex()
	{
		
	}

	public OBJIndex(int position, int texCoord, int normal)
	{
		this.position = position;
		this.texCoord = texCoord;
		this.normal = normal;
	}

	public int getPosition()
	{
		return position;
	}

	public int getTexCoord()
	{
		return texCoord;
	}

	public int getNormal()
	{
		return normal;
	}

	public void setPosition(int position)
	{
		this.position = position;
	}

	public void setTexCoord(int texCoord)
	{
		this.texCoord = texCoord;
	}

	public void setNormal(int normal)
	{
		this.normal = normal;
	}

}

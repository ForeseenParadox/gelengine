/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.model;

import gel.engine.core.Gel;
import gel.engine.graphics.BufferUsage;
import gel.engine.graphics.Color;
import gel.engine.graphics.IndexBuffer;
import gel.engine.graphics.Vertex;
import gel.engine.graphics.VertexArray;
import gel.engine.graphics.VertexBuffer;
import gel.engine.graphics.mesh.Mesh;
import gel.engine.math.Vector2f;
import gel.engine.math.Vector3f;
import gel.engine.utils.IOUtils;
import gel.engine.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class OBJModel extends Model
{

	private List<Vector3f> positionData;
	private List<Vector2f> texCoordData;
	private List<Vector3f> normalData;
	private List<OBJIndex> indexData;
	private boolean hasPositionData;
	private boolean hasTexCoordData;
	private boolean hasNormalData;
	private boolean triangulated;

	public OBJModel(String path)
	{

		if (!Utils.correctExtension(path, "obj"))
		{
			Gel.log.fatal("Wrong extension whiles loading OBJ, should be .obj");
			System.exit(1);
		}

		positionData = new ArrayList<Vector3f>();
		texCoordData = new ArrayList<Vector2f>();
		normalData = new ArrayList<Vector3f>();
		indexData = new ArrayList<OBJIndex>();

		String[] lines = IOUtils.loadInternalFileAsString(path).split("\n");

		for (String line : lines)
		{
			String[] tokens = Utils.removeEmptyStrings(line.split(" "));

			if (tokens.length == 0)
			{
				continue;
			}

			String type = tokens[0];

			if (type.equals("v"))
			{
				float x = Float.valueOf(tokens[1]);
				float y = Float.valueOf(tokens[2]);
				float z = Float.valueOf(tokens[3]);
				positionData.add(new Vector3f(x, y, z));
			} else if (type.equals("vt"))
			{
				float u = Float.valueOf(tokens[1]);
				float v = Float.valueOf(tokens[2]);
				texCoordData.add(new Vector2f(u, v));
			} else if (type.equals("vn"))
			{
				float x = Float.valueOf(tokens[1]);
				float y = Float.valueOf(tokens[2]);
				float z = Float.valueOf(tokens[3]);
				normalData.add(new Vector3f(x, y, z));
			} else if (type.equals("f"))
			{

				if (tokens.length == 4)
				{
					triangulated = true;
				} else
				{
					triangulated = false;
				}

				for (int i = 1; i < tokens.length; i++)
				{

					OBJIndex index = new OBJIndex();

					String[] splitIndex = tokens[i].split("/");

					if (splitIndex.length > 0 && !splitIndex[0].equals(""))
					{
						index.setPosition(Integer.valueOf(splitIndex[0]) - 1);
						hasPositionData = true;
					}

					if (splitIndex.length > 1 && !splitIndex[1].equals(""))
					{
						index.setNormal(Integer.valueOf(splitIndex[1]) - 1);
						hasNormalData = true;
					}

					if (splitIndex.length > 2 && !splitIndex[2].equals(""))
					{
						index.setTexCoord(Integer.valueOf(splitIndex[2]) - 1);
						hasTexCoordData = true;
					}

					indexData.add(index);
				}
			}
		}

	}

	public List<Vector3f> getPositionData()
	{
		return positionData;
	}

	public List<Vector2f> getTexCoordData()
	{
		return texCoordData;
	}

	public List<Vector3f> getNormalData()
	{
		return normalData;
	}

	public List<OBJIndex> getIndexData()
	{
		return indexData;
	}

	public boolean hasPositionData()
	{
		return hasPositionData;
	}

	public boolean hasTexCoordData()
	{
		return hasTexCoordData;
	}

	public boolean hasNormalData()
	{
		return hasNormalData;
	}

	public boolean isTriangulated()
	{
		return triangulated;
	}

	@Override
	public Mesh loadModel()
	{
		VertexArray vao = new VertexArray();
		VertexBuffer buffer = new VertexBuffer(vao, 50000, null, BufferUsage.STATIC);

		return loadModel(buffer);
	}

	@Override
	public Mesh loadModel(VertexBuffer vbo)
	{

		Mesh res = new Mesh(vbo);

		List<Integer> indices = new ArrayList<Integer>();
		for (OBJIndex index : indexData)
		{
			indices.add(index.getPosition());
		}

		if (hasPositionData)
		{
			for (Vector3f pos : positionData)
			{
				Vertex vert = new Vertex();
				vert.setColor(Color.WHITE);
				vert.setPosition(pos);
				res.getVertices().add(vert);
			}
		}

		if (hasNormalData)
		{
			for (OBJIndex index : indexData)
			{
				if (index.getNormal() < normalData.size())
				{
					Vertex vert = res.getVertices().get(index.getNormal());
					vert.setNormal(normalData.get(index.getNormal()));
				}
			}
		}

		if (hasTexCoordData)
		{
			for (OBJIndex index : indexData)
			{
				if (index.getTexCoord() < texCoordData.size())
				{
					Vertex vert = res.getVertices().get(index.getTexCoord());
					vert.setTexCoord(texCoordData.get(index.getTexCoord()));
				}
			}
		}

		IndexBuffer ibo = new IndexBuffer(indices.size(), vbo.getUsage());
		for (Integer i : indices)
		{
			ibo.addIndex(i);
		}
		ibo.sendData();
		vbo.setIndexBuffer(ibo);

		if (!hasNormalData)
		{
			res.calcNormals(indices, triangulated);
		}

		return res;
	}
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.shader;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_INT;
import static org.lwjgl.opengl.GL20.GL_BOOL;

public enum DataType
{

	VOID(0, 0, 0),
	BOOL(1, 1, GL_BOOL),
	INT(1, 4, GL_INT),
	FLOAT(1, 4, GL_FLOAT),
	VEC2F(2, 4, GL_FLOAT),
	VEC2I(2, 4, GL_INT),
	VEC3F(3, 4, GL_FLOAT),
	VEC3I(3, 4, GL_INT),
	VEC4F(4, 4, GL_FLOAT),
	VEC4I(4, 4, GL_INT);

	private int elementCount;
	private int elementSize;
	private int glType;

	DataType(int elementCount, int elementSize, int glType)
	{
		this.elementCount = elementCount;
		this.elementSize = elementSize;
		this.glType = glType;
	}

	public int getElementCount()
	{
		return elementCount;
	}

	public int getElementSize()
	{
		return elementSize;
	}

	public int getGlType()
	{
		return glType;
	}

	public int totalSize()
	{
		return elementCount * elementSize;
	}

}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.shader;

import gel.engine.core.Gel;
import gel.engine.graphics.VertexArray;
import gel.engine.graphics.VertexAttrib;
import gel.engine.math.*;
import gel.engine.utils.IOUtils;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;

public class ShaderProgram
{

	public static final int MAX_ERR_LOG_SIZE = 10000;

	// Uniforms
	public static final String UNIFORM_MODEL_MATRIX = "u_modelMatrix";
	public static final String UNIFORM_VIEWPROJECTION_MATRIX = "u_viewProjectionMatrix";

	// Attributes
	public static final VertexAttrib ATTRIBUTE_POSITION = new VertexAttrib("a_position", 0, DataType.VEC3F);
	public static final VertexAttrib ATTRIBUTE_COLOR = new VertexAttrib("a_color", 1, DataType.VEC4F);
	public static final VertexAttrib ATTRIBUTE_TEXCOORD = new VertexAttrib("a_texCoord", 2, DataType.VEC2F);
	public static final VertexAttrib ATTRIBUTE_NORMAL = new VertexAttrib("a_normal", 3, DataType.VEC3F);

	private int programHandle;
	private int vertexShaderHandle;
	private int fragmentShaderHandle;
	private String log;
	private VertexArray vao;
	private String vertexShaderSource;
	private String fragmentShaderSource;

	public ShaderProgram()
	{
		this(null);
	}

	public ShaderProgram(VertexAttrib[] vertexAttribs)
	{
		this(vertexAttribs, null, null, false);
	}

	public ShaderProgram(VertexAttrib[] vertexAttribs, String vertexShaderPath, String fragmentShaderPath, boolean link)
	{
		this.log = "";
		this.vao = new VertexArray();

		createProgram();

		if (vertexAttribs != null)
		{
			for (VertexAttrib attrib : vertexAttribs)
			{
				vao.getVertexAttributes().add(attrib);
				bindVertexAttrib(attrib);
			}
		}

		if (vertexShaderPath != null)
		{
			createAndCompileVertexShader(vertexShaderPath);
		}

		if (fragmentShaderPath != null)
		{
			createAndCompileFragmentShader(fragmentShaderPath);
		}

		if (link)
		{
			linkProgram();
		}
	}

	public String getProgramLog()
	{
		return log;
	}

	public VertexArray getVertexArray()
	{
		return vao;
	}

	public void createProgram()
	{
		programHandle = glCreateProgram();
	}

	public void bindVertexAttrib(VertexAttrib vertexAttrib)
	{
		glBindAttribLocation(programHandle, vertexAttrib.getLocation(), vertexAttrib.getName());
	}

	public void createAndCompileVertexShader(String vertexShaderPath)
	{

		vertexShaderHandle = glCreateShader(GL_VERTEX_SHADER);
		vertexShaderSource = IOUtils.loadInternalFileAsString(vertexShaderPath);

		glShaderSource(vertexShaderHandle, vertexShaderSource);
		glCompileShader(vertexShaderHandle);

		glValidateProgram(programHandle);

		if (glGetShaderi(vertexShaderHandle, GL_COMPILE_STATUS) == GL_FALSE)
		{
			String result = glGetShaderInfoLog(vertexShaderHandle, MAX_ERR_LOG_SIZE);
			Gel.log.fatal("Error compiling vertex shader: " + result);
			log += result;
			Gel.core.stop();
		} else
		{
			Gel.log.info(IOUtils.getFileName(vertexShaderPath) + " (vertex) sucessfully compiled");
		}

		glAttachShader(programHandle, vertexShaderHandle);
	}

	public void createAndCompileFragmentShader(String fragmentShaderPath)
	{

		fragmentShaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
		fragmentShaderSource = IOUtils.loadInternalFileAsString(fragmentShaderPath);

		glShaderSource(fragmentShaderHandle, fragmentShaderSource);
		glCompileShader(fragmentShaderHandle);

		glValidateProgram(programHandle);

		if (glGetShaderi(fragmentShaderHandle, GL_COMPILE_STATUS) == GL_FALSE)
		{
			String result = glGetShaderInfoLog(fragmentShaderHandle, MAX_ERR_LOG_SIZE);
			Gel.log.fatal("Error compiling fragment shader: " + result);
			log += result;
			Gel.core.stop();
		} else
		{
			Gel.log.info(IOUtils.getFileName(fragmentShaderPath) + " (fragment) sucessfully compiled");
		}

		glAttachShader(programHandle, fragmentShaderHandle);
	}

	public void linkProgram()
	{
		glLinkProgram(programHandle);

		glValidateProgram(programHandle);

		if (glGetProgrami(programHandle, GL_LINK_STATUS) == GL_FALSE)
		{
			String result = glGetProgramInfoLog(programHandle, MAX_ERR_LOG_SIZE);
			Gel.log.fatal("Error whiles linking shader program:" + result);
			log += result;
			Gel.core.stop();
		} else
		{
			Gel.log.info("Shader with handle " + programHandle + " sucessfully linked");
		}

	}

	public void updateUniforms(Matrix4f viewProjectionMatrix, Matrix4f transform)
	{
		bind();
		setMatrix4f(UNIFORM_VIEWPROJECTION_MATRIX, viewProjectionMatrix);
		setMatrix4f(UNIFORM_MODEL_MATRIX, transform);
	}

	public void bind()
	{
		glUseProgram(programHandle);
	}

	public int getUniformLocation(String name)
	{
		return glGetUniformLocation(programHandle, name);
	}

	public void setInteger(String name, int value)
	{
		glUniform1i(getUniformLocation(name), value);
	}

	public void setFloat(String name, float value)
	{
		glUniform1f(getUniformLocation(name), value);
	}

	public void setVector2f(String name, Vector2f value)
	{
		glUniform2f(getUniformLocation(name), value.getX(), value.getY());
	}

	public void setVector3f(String name, Vector3f value)
	{
		glUniform3f(getUniformLocation(name), value.getX(), value.getY(), value.getZ());
	}

	public void setVector4f(String name, Vector4f value)
	{
		glUniform4f(getUniformLocation(name), value.getX(), value.getY(), value.getZ(), value.getW());
	}

	public void setMatrix2f(String name, Matrix2f value)
	{
		glUniformMatrix2(getUniformLocation(name), false, IOUtils.mat2ToFloatBuffer(value));
	}

	public void setMatrix3f(String name, Matrix3f value)
	{
		glUniformMatrix3(getUniformLocation(name), false, IOUtils.mat3ToFloatBuffer(value));
	}

	public void setMatrix4f(String name, Matrix4f value)
	{
		glUniformMatrix4(getUniformLocation(name), false, IOUtils.mat4ToFloatBuffer(value));
	}

}

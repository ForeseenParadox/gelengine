/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.shader.forward;

import gel.engine.graphics.VertexAttrib;
import gel.engine.graphics.shader.ShaderProgram;
import gel.engine.math.Matrix4f;
import gel.engine.math.Vector3f;

public class ForwardAmbient extends ShaderProgram
{

	public static final String UNIFORM_AMBIENT = "u_ambient";

	public ForwardAmbient()
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL });
	}

	public ForwardAmbient(String vertexShaderPath, String fragmentShaderPath, boolean link)
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL }, vertexShaderPath, fragmentShaderPath, link);
	}

	public void updateUniforms(Matrix4f viewProjectionMatrix, Matrix4f transform, Vector3f ambient)
	{
		super.updateUniforms(viewProjectionMatrix, transform);
		setVector3f(UNIFORM_AMBIENT, ambient);
	}

}

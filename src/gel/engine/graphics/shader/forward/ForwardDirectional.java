/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.shader.forward;

import gel.engine.graphics.Material;
import gel.engine.graphics.VertexAttrib;
import gel.engine.graphics.lighting.DirectionalLight;
import gel.engine.graphics.shader.ShaderProgram;
import gel.engine.math.Matrix4f;
import gel.engine.math.Vector3f;

public class ForwardDirectional extends ShaderProgram
{

	public static final String UNIFORM_DIRECTION = "u_direction";
	public static final String UNIFORM_COLOR = "u_color";
	public static final String UNIFORM_EYE_POSITION = "u_eyePosition";
	public static final String UNIFORM_INTENSITY = "u_intensity";
	public static final String UNIFORM_SPECULAR_INTENSITY = "u_specularIntensity";
	public static final String UNIFORM_SPECULAR_EXPONENT = "u_specularExponent";

	public ForwardDirectional()
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL });
	}

	public ForwardDirectional(String vertexShaderPath, String fragmentShaderPath, boolean link)
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL }, vertexShaderPath, fragmentShaderPath, link);
	}

	public void updateUniforms(Matrix4f viewProjectionMatrix, Matrix4f transform, Material material, DirectionalLight light, Vector3f eyePosition)
	{
		super.updateUniforms(viewProjectionMatrix, transform);

		setVector3f(UNIFORM_DIRECTION, light.getDirection());
		setVector3f(UNIFORM_COLOR, light.getColor().toVector3f());
		setVector3f(UNIFORM_EYE_POSITION, eyePosition);
		setFloat(UNIFORM_INTENSITY, light.getIntensity());
		setFloat(UNIFORM_SPECULAR_INTENSITY, material.getSpecularIntensity());
		setFloat(UNIFORM_SPECULAR_EXPONENT, material.getSpecularExponent());

	}

}

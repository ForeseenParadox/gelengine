package gel.engine.graphics.texture;

public enum Filter
{
    NONE, NEAREST, LINEAR
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.texture;

import gel.engine.graphics.Bitmap;
import gel.engine.utils.IOUtils;
import gel.engine.utils.MathUtils;
import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GL11;

import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public class Texture
{

	public static final float ANISOTROPIC_CLAMP_MAX = 16f;

	private Bitmap bitmap;
	private int textureID;

	public Texture(String path)
	{
		this(new Bitmap(path));
	}

	public Texture(Bitmap bitmap)
	{
		this.bitmap = bitmap;
	}

	public int getWidth()
	{
		return bitmap.getWidth();
	}

	public int getHeight()
	{
		return bitmap.getHeight();
	}

	public Bitmap getBitmap()
	{
		return bitmap;
	}

	public int getID()
	{
		return textureID;
	}

	public void bind()
	{
		glBindTexture(GL_TEXTURE_2D, textureID);
	}

	public void unbind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	public Texture load()
	{
		return load(Filter.LINEAR);
	}

	public Texture load(Filter textureFilter)
	{
		return load(textureFilter, Filter.LINEAR);
	}

	public Texture load(Filter textureFilter, Filter mipMapFilter)
	{
		return load(textureFilter, mipMapFilter, MathUtils.clamp(0.0f, ANISOTROPIC_CLAMP_MAX, GL11.glGetFloat(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT)));
	}

	public Texture load(Filter textureFilter, Filter mipMapFilter, float anisotropicLevel)
	{

		textureID = glGenTextures();

		bind();

		ByteBuffer pixelData = IOUtils.createByteBuffer(bitmap.getWidth() * bitmap.getHeight() * 4);

		int[] data = bitmap.getData();

		for (int i : data)
		{
			pixelData.put((byte) ((i >> 16) & 0xFF));
			pixelData.put((byte) ((i >> 8) & 0xFF));
			pixelData.put((byte) ((i >> 0) & 0xFF));
			pixelData.put((byte) ((i >> 24) & 0xFF));
		}

		pixelData.flip();

		if (textureFilter == Filter.NEAREST || textureFilter == Filter.NONE)
		{
			if (mipMapFilter == Filter.NONE)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			} else if (mipMapFilter == Filter.NEAREST)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
			} else if (mipMapFilter == Filter.LINEAR)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
			}
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		} else if (textureFilter == Filter.LINEAR)
		{
			if (mipMapFilter == Filter.NONE)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			} else if (mipMapFilter == Filter.NEAREST)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
			} else if (mipMapFilter == Filter.LINEAR)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			}
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		if (anisotropicLevel >= 1.0f)
		{
			glTexParameterf(GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropicLevel);
		}

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, bitmap.getWidth(), bitmap.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelData);

		if (mipMapFilter != Filter.NONE)
		{
			glGenerateMipmap(GL_TEXTURE_2D);
		}

		return this;

	}
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.graphics.texture;

import gel.engine.math.Vector2f;

public class TextureRegion
{

    private Texture texture;
    private float x;
    private float y;
    private float width;
    private float height;
    private Vector2f uv1;
    private Vector2f uv2;
    private Vector2f uv3;
    private Vector2f uv4;

    public TextureRegion(Texture texture)
    {
        this(texture, 0, 0, texture.getWidth(), texture.getHeight());
    }

    public TextureRegion()
    {
        this(null, 0, 0, 0, 0);
    }

    public TextureRegion(Texture texture, float x, float y, float width, float height)
    {

        this.texture = texture;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.uv1 = new Vector2f();
        this.uv2 = new Vector2f();
        this.uv3 = new Vector2f();
        this.uv4 = new Vector2f();
    }

    public Texture getTexture()
    {
        return texture;
    }

    public float getX()
    {
        return x;
    }

    public float getY()
    {
        return y;
    }

    public float getWidth()
    {
        return width;
    }

    public float getHeight()
    {
        return height;
    }

    public Vector2f getUv1()
    {
        uv1.set(x / texture.getWidth(), y / texture.getHeight());
        return uv1;
    }

    public Vector2f getUv2()
    {
        uv2.set(x / texture.getWidth(), (y + height) / texture.getHeight());
        return uv2;
    }

    public Vector2f getUv3()
    {
        uv3.set((x + width) / texture.getWidth(), (y + height) / texture.getHeight());
        return uv3;
    }

    public Vector2f getUv4()
    {
        uv4.set((x + width) / texture.getWidth(), y / texture.getHeight());
        return uv4;
    }

    public void setTexture(Texture texture)
    {
        this.texture = texture;
    }

    public void setX(float x)
    {
        this.x = x;
    }

    public void setY(float y)
    {
        this.y = y;
    }

    public void setWidth(float width)
    {
        this.width = width;
    }

    public void setHeight(float height)
    {
        this.height = height;
    }

}

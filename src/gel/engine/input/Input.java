/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.input;

import gel.engine.core.Gel;

public class Input
{

	private Keyboard keyboard;
	private Mouse mouse;

	public Input()
	{
		if (Gel.input == null)
		{

			Gel.input = this;

			keyboard = new Keyboard();
			mouse = new Mouse();
		} else
		{
			throw new IllegalStateException("Can not make more than one instance of Input");
		}
	}

	public Keyboard getKeyboard()
	{
		return keyboard;
	}

	public Mouse getMouse()
	{
		return mouse;
	}

	public void create()
	{
		keyboard.create();
		mouse.create();
	}

	public void update()
	{
		keyboard.update();
		mouse.update();
	}

	public void dispose()
	{
		keyboard.dispose();
		mouse.dispose();
	}

}

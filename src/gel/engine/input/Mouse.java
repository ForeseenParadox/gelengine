/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.input;

import org.lwjgl.LWJGLException;

public class Mouse
{

	public static final int NUM_BUTTONS = 0xFF;

	private static Mouse instance;

	private boolean[] buttonsDown;
	private boolean[] buttonsThisFrame;

	public Mouse()
	{
		if (instance == null)
		{
			instance = this;

			buttonsDown = new boolean[NUM_BUTTONS];
			buttonsThisFrame = new boolean[NUM_BUTTONS];
		} else
		{
			throw new IllegalStateException("Can not make more than one instance of Mouse");
		}
	}

	public boolean hasWheel()
	{
		return org.lwjgl.input.Mouse.hasWheel();
	}

	public boolean isInWindow()
	{
		return org.lwjgl.input.Mouse.isInsideWindow();
	}

	public boolean isGrabbed()
	{
		return org.lwjgl.input.Mouse.isGrabbed();
	}

	public boolean isButtonDown(int button)
	{
		return buttonsDown[button];
	}

	public boolean wasButtonPressed(int button)
	{
		return buttonsThisFrame[button];
	}

	public int getX()
	{
		return org.lwjgl.input.Mouse.getX();
	}

	public int getY()
	{
		return org.lwjgl.input.Mouse.getY();
	}

	public int getDX()
	{
		return org.lwjgl.input.Mouse.getDX();
	}

	public int getDY()
	{
		return org.lwjgl.input.Mouse.getDY();
	}

	public int getDWheel()
	{
		return org.lwjgl.input.Mouse.getDWheel();
	}

	public void setGrabbed(boolean grabbed)
	{
		org.lwjgl.input.Mouse.setGrabbed(grabbed);
	}

	public void setPosition(int x, int y)
	{
		org.lwjgl.input.Mouse.setCursorPosition(x, y);
	}

	public void create()
	{
		try
		{
			org.lwjgl.input.Mouse.create();
		} catch (LWJGLException e)
		{
			System.err.println("Error creating mouse");
			e.printStackTrace();
		}
	}

	public void update()
	{
		for (int i = 0; i < NUM_BUTTONS; i++)
		{
			if (org.lwjgl.input.Mouse.isButtonDown(i) && !buttonsDown[i])
			{
				buttonsThisFrame[i] = true;
			} else
			{
				buttonsThisFrame[i] = false;
			}
		}

		for (int i = 0; i < NUM_BUTTONS; i++)
		{
			if (org.lwjgl.input.Mouse.isButtonDown(i))
			{
				buttonsDown[i] = true;
			} else
			{
				buttonsDown[i] = false;
			}
		}

	}

	public void dispose()
	{
		org.lwjgl.input.Mouse.destroy();
	}

}

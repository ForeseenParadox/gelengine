/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.math;

public class Matrix2f
{

	private float[][] mat;

	public Matrix2f()
	{
		this.mat = new float[2][2];
	}

	public Matrix2f(Matrix2f mat)
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				setElement(x, y, mat.getElement(x, y));
			}
		}
	}

	public float getElement(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < 2 && y < 2)
		{
			return mat[y][x];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public float[] getRow(int row)
	{
		if (row >= 0 && row < 2)
		{
			return mat[row];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public float[] getColumn(int col)
	{
		if (col >= 0 && col < 2)
		{
			float[] res = new float[2];
			for (int i = 0; i < 2; i++)
			{
				res[i] = mat[i][col];
			}
			return res;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void setElement(int x, int y, float value)
	{
		if (x >= 0 && y >= 0 && x < 2 && y < 2)
		{
			mat[y][x] = value;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to set element that was out of matrix bounds");
		}
	}

	public void setRow(int row, float[] value)
	{
		if (row >= 0 && row < 2)
		{
			if (value.length < 3)
			{
				mat[row] = value;
			} else
			{
				throw new IllegalArgumentException("Row too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void setColumn(int col, float[] value)
	{
		if (col >= 0 && col < 2)
		{
			if (value.length < 3)
			{
				for (int i = 0; i < 2; i++)
				{
					mat[i][col] = value[i];
				}
			} else
			{
				throw new IllegalArgumentException("Column too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void set(Matrix2f mat)
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				setElement(x, y, mat.getElement(x, y));
			}
		}
	}

	public Matrix2f add(Matrix2f mat)
	{
		Matrix2f res = new Matrix2f();
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				res.setElement(x, y, getElement(x, y) + mat.getElement(x, y));
			}
		}
		return res;
	}

	public Matrix2f subtract(Matrix2f mat)
	{
		Matrix2f res = new Matrix2f();
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				res.setElement(x, y, getElement(x, y) - mat.getElement(x, y));
			}
		}
		return res;
	}

	public Matrix2f multiply(Matrix2f mat)
	{
		Matrix2f res = new Matrix2f();
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				Vector2f row = new Vector2f(getRow(y));
				Vector2f col = new Vector2f(mat.getColumn(x));
				res.setElement(x, y, (float) row.dotProduct(col));
			}
		}
		return res;
	}

	public Matrix2f transpose()
	{
		Matrix2f res = new Matrix2f();
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				res.setElement(y, x, getElement(x, y));
			}
		}
		return res;
	}

	public void scale(float scaler)
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				setElement(x, y, getElement(x, y) * scaler);
			}
		}
	}

	public void setIdentity()
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				if (x == y)
				{
					setElement(x, y, 1);
				} else
				{
					setElement(x, y, 0);
				}
			}
		}
	}

	public void setZero()
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				setElement(x, y, 0);
			}
		}
	}

}

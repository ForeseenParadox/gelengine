/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.math;

public class Matrix3f
{

	private float[][] mat;

	public Matrix3f()
	{
		this.mat = new float[3][3];
	}

	public Matrix3f(Matrix3f mat)
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				getElement(x, y, mat.getElement(x, y));
			}
		}
	}

	public float getElement(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < 3 && y < 3)
		{
			return mat[y][x];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public float[] getRow(int row)
	{
		if (row >= 0 && row < 3)
		{
			return mat[row];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public float[] getColumn(int col)
	{
		if (col >= 0 && col < 3)
		{
			float[] res = new float[3];
			for (int i = 0; i < 3; i++)
			{
				res[i] = mat[i][col];
			}
			return res;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void getElement(int x, int y, float value)
	{
		if (x >= 0 && y >= 0 && x < 3 && y < 3)
		{
			mat[y][x] = value;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to set element that was out of matrix bounds");
		}
	}

	public void setRow(int row, float[] value)
	{
		if (row >= 0 && row < 3)
		{
			if (value.length < 3)
			{
				mat[row] = value;
			} else
			{
				throw new IllegalArgumentException("Row too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void setColumn(int col, float[] value)
	{
		if (col >= 0 && col < 3)
		{
			if (value.length < 3)
			{
				for (int i = 0; i < 3; i++)
				{
					mat[i][col] = value[i];
				}
			} else
			{
				throw new IllegalArgumentException("Column too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void set(Matrix3f mat)
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				getElement(x, y, mat.getElement(x, y));
			}
		}
	}

	public Matrix3f add(Matrix3f mat)
	{
		Matrix3f res = new Matrix3f();
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				res.getElement(x, y, getElement(x, y) + mat.getElement(x, y));
			}
		}
		return res;
	}

	public Matrix3f subtract(Matrix3f mat)
	{
		Matrix3f res = new Matrix3f();
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				res.getElement(x, y, getElement(x, y) - mat.getElement(x, y));
			}
		}
		return res;
	}

	public Matrix3f multiply(Matrix3f mat)
	{
		Matrix3f res = new Matrix3f();
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				Vector3f row = new Vector3f(getRow(y));
				Vector3f col = new Vector3f(mat.getColumn(x));
				res.getElement(x, y, (float) row.dot(col));
			}
		}
		return res;
	}

	public Matrix3f transpose()
	{
		Matrix3f res = new Matrix3f();
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				res.getElement(y, x, getElement(x, y));
			}
		}
		return res;
	}

	public void scale(float scaler)
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				getElement(x, y, getElement(x, y) * scaler);
			}
		}
	}

	public void setIdentity()
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				if (x == y)
				{
					getElement(x, y, 1);
				} else
				{
					getElement(x, y, 0);
				}
			}
		}
	}

	public void setZero()
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				getElement(x, y, 0);
			}
		}
	}

}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.math;

public class Matrix4f
{

	private float[][] mat;

	public Matrix4f()
	{
		this.mat = new float[4][4];
	}

	public Matrix4f(Matrix4f mat)
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				setElement(x, y, mat.getElement(x, y));
			}
		}
	}

	public float getElement(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < 4 && y < 4)
		{
			return mat[y][x];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public float[] getRow(int row)
	{
		if (row >= 0 && row < 4)
		{
			return mat[row];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public float[] getColumn(int col)
	{
		if (col >= 0 && col < 4)
		{
			float[] res = new float[4];
			for (int i = 0; i < 4; i++)
			{
				res[i] = mat[i][col];
			}
			return res;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void setElement(int row, int col, float value)
	{
		if (row >= 0 && col >= 0 && row < 4 && col < 4)
		{
			mat[col][row] = value;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to set element that was out of matrix bounds");
		}
	}

	public void setRow(int row, float[] value)
	{
		if (row >= 0 && row < 4)
		{
			if (value.length < 3)
			{
				mat[row] = value;
			} else
			{
				throw new IllegalArgumentException("Row too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void setColumn(int col, float[] value)
	{
		if (col >= 0 && col < 4)
		{
			if (value.length < 3)
			{
				for (int i = 0; i < 4; i++)
				{
					mat[i][col] = value[i];
				}
			} else
			{
				throw new IllegalArgumentException("Column too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	public void set(Matrix4f mat)
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				setElement(x, y, mat.getElement(x, y));
			}
		}
	}

	public Matrix4f add(Matrix4f mat)
	{
		Matrix4f res = new Matrix4f();
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				res.setElement(x, y, getElement(x, y) + mat.getElement(x, y));
			}
		}
		return res;
	}

	public Matrix4f subtract(Matrix4f mat)
	{
		Matrix4f res = new Matrix4f();
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				res.setElement(x, y, getElement(x, y) - mat.getElement(x, y));
			}
		}
		return res;
	}

	public Matrix4f multiply(Matrix4f mat)
	{
		Matrix4f res = new Matrix4f();
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				Vector4f row = new Vector4f(getRow(y));
				Vector4f col = new Vector4f(mat.getColumn(x));
				res.setElement(x, y, (float) row.dot(col));
			}
		}
		return res;
	}

	public Matrix4f transpose()
	{
		Matrix4f res = new Matrix4f();
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				res.setElement(y, x, getElement(x, y));
			}
		}
		return res;
	}

	public void scale(float scaler)
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				setElement(x, y, getElement(x, y) * scaler);
			}
		}
	}

	public void setIdentity()
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				if (x == y)
				{
					setElement(x, y, 1);
				} else
				{
					setElement(x, y, 0);
				}
			}
		}
	}

	public void setZero()
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				setElement(x, y, 0);
			}
		}
	}

}

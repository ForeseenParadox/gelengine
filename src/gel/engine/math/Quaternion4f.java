/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.math;

public class Quaternion4f
{

	private float x;
	private float y;
	private float z;
	private float w;

	public Quaternion4f()
	{
		this(0, 0, 0, 0);
	}

	public Quaternion4f(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Quaternion4f(Vector3f axis, float rotation)
	{

		float cos = (float) Math.cos(rotation);
		float sin = (float) Math.sin(rotation);

		this.x = axis.getX() * sin;
		this.y = axis.getY() * sin;
		this.z = axis.getZ() * sin;
		this.w = cos;

		// this.w = Math.cos(rotation / 2);
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public float getZ()
	{
		return z;
	}

	public float getW()
	{
		return w;
	}

	public float length()
	{
		return (float) Math.sqrt(x * x + y * y + z * z + w * w);
	}

	public Quaternion4f scale(float scaler)
	{
		this.x *= scaler;
		this.y *= scaler;
		this.z *= scaler;
		this.w *= scaler;
		return this;
	}

	public Quaternion4f normalize()
	{
		float length = length();
		x /= length;
		y /= length;
		z /= length;
		w /= length;
		return this;
	}

	public Quaternion4f conjugate()
	{
		return new Quaternion4f(-x, -y, -z, w);
	}

	public Quaternion4f add(Quaternion4f other)
	{
		return new Quaternion4f(x + other.getX(), y + other.getY(), z + other.getZ(), w + other.getW());
	}

	public Quaternion4f sub(Quaternion4f other)
	{
		return new Quaternion4f(x - other.getX(), y - other.getY(), z - other.getZ(), w - other.getW());
	}

	public Quaternion4f multiply(Quaternion4f other)
	{
		Quaternion4f res = new Quaternion4f();

		res.setW(w * other.getW() - x * other.getX() - y * other.getY() - z * other.getZ());
		res.setX(w * other.getX() + x * other.getW() + y * other.getZ() - z * other.getY());
		res.setY(w * other.getY() + y * other.getW() + z * other.getX() - x * other.getZ());
		res.setZ(w * other.getZ() + z * other.getW() + x * other.getY() - y * other.getX());

		return res;
	}

	public Quaternion4f inverse()
	{
		Quaternion4f conjugate = conjugate();
		float length = length();
		float l2 = length * length;
		return new Quaternion4f(conjugate.getX() / l2, conjugate.getY() / l2, conjugate.getZ() / l2, conjugate.getW() / l2);
	}

	public float dot(Quaternion4f other)
	{
		return x * other.getX() + y * other.getY() + z * other.getZ() + w * other.getW();
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public void setZ(float z)
	{
		this.z = z;
	}

	public void setW(float w)
	{
		this.w = w;
	}

}

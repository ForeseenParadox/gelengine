/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.math;

import gel.engine.utils.MathUtils;

public class Transform
{

	private Vector3f translation;
	private Vector3f rotation;
	private Vector3f scale;

	public Transform()
	{
		translation = new Vector3f();
		rotation = new Vector3f();
		scale = new Vector3f(1, 1, 1);
	}

	public Vector3f getTranslation()
	{
		return translation;
	}

	public Vector3f getRotation()
	{
		return rotation;
	}

	public Vector3f getScale()
	{
		return scale;
	}

	public Matrix4f getTransformMatrix()
	{
		Matrix4f tMat = MathUtils.translationMatrix(translation);
		Matrix4f rMat = MathUtils.rotationMatrix(rotation);
		Matrix4f sMat = MathUtils.scaleMatrix(scale);
		return tMat.multiply(rMat.multiply(sMat));
	}

	public void setTranslation(Vector3f translation)
	{
		this.translation = translation;
	}

	public void setRotation(Vector3f rotation)
	{
		this.rotation = rotation;
	}

	public void setScale(Vector3f scale)
	{
		this.scale = scale;
	}

}

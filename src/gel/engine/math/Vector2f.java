/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.math;

public class Vector2f
{

    private float x;
    private float y;

    public Vector2f()
    {

    }

    public Vector2f(float value)
    {
        this.x = value;
        this.y = value;
    }

    public Vector2f(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector2f(float[] vec)
    {
        if (vec.length == 2)
        {
            this.x = vec[0];
            this.y = vec[1];
        } else
        {
            throw new IllegalArgumentException("Array incorrect size for vector2f");
        }
    }

    public float length()
    {
        return (float) Math.sqrt(x * x + y * y);
    }

    public float lengthSquared()
    {
        return x * x + y * y;
    }

    public Vector2f add(Vector2f vec)
    {
        return new Vector2f(x + vec.getX(), y + vec.getY());
    }

    public Vector2f subtract(Vector2f vec)
    {
        return new Vector2f(x - vec.getX(), y - vec.getY());
    }

    public Vector2f multiply(float scaler)
    {
        return new Vector2f(x * scaler, y * scaler);
    }

    public Vector2f multiply(Matrix2f mat)
    {
        // implement later
        return null;
    }

    public Vector2f divide(float value)
    {
        return new Vector2f(x / value, y / value);
    }

    public Vector2f negate()
    {
        return new Vector2f(-x, -y);
    }

    public float distance(Vector2f vector)
    {
        return subtract(vector).length();
    }

    public float distanceSquared(Vector2f other)
    {
        return subtract(other).lengthSquared();
    }

    public Vector2f lerp(Vector2f target, float l)
    {
        return subtract(target).multiply(l);
    }

    public float angleBetween(Vector2f vector)
    {
        return (float) Math.acos(normalized().dotProduct(vector.normalized()));
    }

    public float dotProduct(Vector2f vec)
    {
        return x * vec.getX() + y * vec.getY();
    }

    public Vector2f normalized()
    {
        float length = length();
        return new Vector2f(x / length, y / length);
    }

    public float getX()
    {
        return x;
    }

    public float getY()
    {
        return y;
    }

    public void setX(float x)
    {
        this.x = x;
    }

    public void setY(float y)
    {
        this.y = y;
    }

    public void set(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public void set(float v)
    {
        this.x = v;
        this.y = v;
    }

    public void set(Vector2f vec)
    {
        this.x = vec.getX();
        this.y = vec.getY();
    }

    public void set(float[] vec)
    {
        if (vec.length == 2)
        {
            this.x = vec[0];
            this.y = vec[1];
        } else
        {
            throw new IllegalArgumentException("Array incorrect size for vector2f");
        }
    }

    public void zero()
    {
        x = 0;
        y = 0;
    }

    public boolean equals(Vector2f other)
    {
        if (x == other.getX() && y == other.getY())
        {
            return true;
        } else
        {
            return false;
        }
    }

    @Override
    public boolean equals(Object other)
    {
        if (other instanceof Vector2f)
        {
            return equals((Vector2f) other);
        } else
        {
            return false;
        }
    }

    @Override
    public String toString()
    {
        return "[" + x + ", " + y + "]";
    }

}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.math;

public class Vector3f
{

	private float x;
	private float y;
	private float z;

	public Vector3f()
	{
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}

	public Vector3f(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3f(float v)
	{
		this.x = v;
		this.y = v;
		this.z = v;
	}

	public Vector3f(Vector2f vec, float z)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = z;
	}

	public Vector3f(Vector3f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
	}

	public Vector3f(float[] vec)
	{
		if (vec.length == 2)
		{
			this.x = vec[0];
			this.y = vec[1];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector");
		}
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public float getZ()
	{
		return z;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public void setZ(float z)
	{
		this.z = z;
	}

	public void set(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void set(float v)
	{
		this.x = v;
		this.y = v;
		this.z = v;
	}

	public void set(Vector2f vec, float z)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = z;
	}

	public void set(Vector3f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
	}

	public void set(float[] vec)
	{
		if (vec.length == 3)
		{
			this.x = vec[0];
			this.y = vec[1];
			this.z = vec[2];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector");
		}
	}

	// vec operators

	public Vector3f add(Vector3f vec)
	{
		return new Vector3f(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ());
	}

	public Vector3f subtract(Vector3f vec)
	{
		return new Vector3f(this.x - vec.getX(), this.y - vec.getY(), this.z - vec.getZ());
	}

	public Vector3f multiply(Vector3f vec)
	{
		return new Vector3f(this.x * vec.getX(), this.y * vec.getY(), this.z * vec.getZ());
	}

	public Vector3f pow(double power)
	{
		return new Vector3f((float) Math.pow(this.x, power), (float) Math.pow(this.y, power), (float) Math.pow(this.z, power));
	}

	public double length()
	{
		return Math.sqrt(x * x + y * y + z * z);
	}

	public double dot(Vector3f vec)
	{
		return this.x * vec.getX() + this.y * vec.getY() + this.z * vec.getZ();
	}

	public Vector3f cross(Vector3f vec)
	{
		float newX = y * vec.getZ() - z * vec.getY();
		float newY = z * vec.getX() - x * vec.getZ();
		float newZ = x * vec.getY() - y * vec.getX();
		return new Vector3f(newX, newY, newZ);
	}

	public void normalize()
	{
		double length = length();
		this.x /= length;
		this.y /= length;
		this.z /= length;
	}

	public void scale(float scaler)
	{
		this.x *= scaler;
		this.y *= scaler;
		this.z *= scaler;
	}

	public void invertX()
	{
		this.x *= -1;
	}

	public void invertY()
	{
		this.y *= -1;
	}

	public void invertZ()
	{
		this.z *= -1;
	}

	public void invert()
	{
		scale(-1);
	}

	public boolean equals(Vector3f other)
	{
		return x == other.getX() && y == other.getY() && z == other.getZ();
	}

}

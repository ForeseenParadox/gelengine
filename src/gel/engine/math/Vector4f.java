/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.math;

public class Vector4f
{

	private float x;
	private float y;
	private float z;
	private float w;

	public Vector4f()
	{
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.w = 0;
	}

	public Vector4f(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Vector4f(float v)
	{
		this.x = v;
		this.y = v;
		this.z = v;
		this.w = v;
	}

	public Vector4f(Vector2f vec, float z, float w)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = z;
		this.w = w;
	}

	public Vector4f(Vector3f vec, float w)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
		this.w = w;
	}

	public Vector4f(Vector4f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
		this.w = vec.getW();
	}

	public Vector4f(float[] vec)
	{
		if (vec.length == 4)
		{
			this.x = vec[0];
			this.y = vec[1];
			this.z = vec[2];
			this.w = vec[3];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector");
		}
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public float getZ()
	{
		return z;
	}

	public float getW()
	{
		return w;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public void setZ(float z)
	{
		this.z = z;
	}

	public void setW(float w)
	{
		this.w = w;
	}

	public void set(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public void set(float v)
	{
		this.x = v;
		this.y = v;
		this.z = v;
		this.w = v;
	}

	public void set(Vector2f vec, float z, float w)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = z;
		this.w = w;
	}

	public void set(Vector3f vec, float w)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
		this.w = w;
	}

	public void set(Vector4f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
		this.w = vec.getW();
	}

	public void set(float[] vec)
	{
		if (vec.length == 4)
		{
			this.x = vec[0];
			this.y = vec[1];
			this.z = vec[2];
			this.w = vec[3];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector");
		}
	}

	// vec operators

	public Vector4f add(Vector4f vec)
	{
		return new Vector4f(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ(), this.w + vec.getW());
	}

	public Vector4f subtract(Vector4f vec)
	{
		return new Vector4f(this.x - vec.getX(), this.y - vec.getY(), this.z - vec.getZ(), this.w - vec.getW());
	}

	public Vector4f multiply(Vector4f vec)
	{
		return new Vector4f(this.x * vec.getX(), this.y * vec.getY(), this.z * vec.getZ(), this.w * vec.getW());
	}

	public Vector4f multiply(Matrix4f mat)
	{
		Vector4f res = new Vector4f();
		res.setX(mat.getElement(0, 0) * x + mat.getElement(0, 1) * y + mat.getElement(0, 2) * z + mat.getElement(0, 3) * w);
		res.setY(mat.getElement(1, 0) * x + mat.getElement(1, 1) * y + mat.getElement(1, 2) * z + mat.getElement(1, 3) * w);
		res.setZ(mat.getElement(2, 0) * x + mat.getElement(2, 1) * y + mat.getElement(2, 2) * z + mat.getElement(2, 3) * w);
		res.setW(mat.getElement(3, 0) * x + mat.getElement(3, 1) * y + mat.getElement(3, 2) * z + mat.getElement(3, 3) * w);
		return res;
	}

	public Vector4f pow(double power)
	{
		return new Vector4f((float) Math.pow(this.x, power), (float) Math.pow(this.y, power), (float) Math.pow(this.z, power), (float) Math.pow(this.w, power));
	}

	public double length()
	{
		return Math.sqrt(x * x + y * y + z * z + w * w);
	}

	public double dot(Vector4f vec)
	{
		return this.x * vec.getX() + this.y * vec.getY() + this.z * vec.getZ() + this.w * vec.getW();
	}

	public void normalize()
	{
		float length = (float) length();
		this.x /= length;
		this.y /= length;
		this.z /= length;
		this.w /= length;
	}

	public void scale(float scaler)
	{
		this.x *= scaler;
		this.y *= scaler;
		this.z *= scaler;
		this.w *= scaler;
	}

	public void invertX()
	{
		this.x *= -1;
	}

	public void invertY()
	{
		this.y *= -1;
	}

	public void invertZ()
	{
		this.z *= -1;
	}

	public void invertW()
	{
		this.w *= -1;
	}

	public void invert()
	{
		scale(-1);
	}

	public void translate(float x, float y, float z, float w)
	{
		this.x += x;
		this.y += y;
		this.z += z;
		this.w += w;
	}

	public void mult(float x, float y, float z, float w)
	{
		this.x *= x;
		this.y *= y;
		this.z *= z;
		this.w *= w;
	}

}

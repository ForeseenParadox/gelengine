package gel.engine.net;

import gel.engine.net.packet.Packet;

public interface PacketListener
{
	
	public void onPacketSend(Packet packet);
	
	public void onPacketReceive(Packet packet);

}

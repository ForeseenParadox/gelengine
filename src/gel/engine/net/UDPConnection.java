/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.net;

import gel.engine.core.Gel;
import gel.engine.net.packet.Packet;
import gel.engine.net.packet.PacketPing;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class UDPConnection
{

	// make a limit to how many packets can be sent/received per second

	public static final int MAX_PACKET_SIZE = 1024;

	private static int instances = 0;

	private boolean connected;
	private DatagramSocket socket;
	private Thread packetInThread;
	private Thread packetOutThread;
	private InetAddress serverIP;
	private int serverPort;
	private List<Packet> packetOutBuffer;
	private List<PacketListener> packetListeners;

	public UDPConnection(String serverIp, int serverPort)
	{
		this(null, -1, serverIp, serverPort);
	}

	public UDPConnection(int clientPort, String serverIp, int serverPort)
	{
		this(null, clientPort, serverIp, serverPort);
	}

	public UDPConnection(String clientIp, int clientPort, String serverIp, int serverPort)
	{

		try
		{
			if (clientIp == null && clientPort == -1)
			{
				socket = new DatagramSocket();
			} else if (clientIp == null && clientPort != -1)
			{
				socket = new DatagramSocket(clientPort);
			} else
			{
				socket = new DatagramSocket(clientPort, InetAddress.getByName(clientIp));
			}
			
			packetInThread = new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					while (connected)
					{
						try
						{
							byte[] dataStore = new byte[MAX_PACKET_SIZE];
							DatagramPacket packet = new DatagramPacket(dataStore, dataStore.length);
							socket.receive(packet);
							byte id = dataStore[0];

							byte[] packetData = new byte[dataStore.length - 1];
							for (int i = 1; i < dataStore.length; i++)
							{
								packetData[i - 1] = dataStore[i];
							}

							Packet pack = new Packet((int) id, packetData);

							for (PacketListener listener : packetListeners)
							{
								listener.onPacketReceive(pack);
							}

						} catch (IOException e)
						{
							e.printStackTrace();
							System.out.println("Disconnecting from " + getServerIP());
							disconnect();
						}
					}
				}

			}, "UDP Packet Input Thread-" + instances);

			packetOutThread = new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					while (connected)
					{
						for (int i = 0; i < packetOutBuffer.size(); i++)
						{
							Packet packet = packetOutBuffer.get(i);
							try
							{
								packet.compile();
								byte[] data = packet.getData();
								DatagramPacket datagramPacket = new DatagramPacket(data, data.length);
								socket.send(datagramPacket);
								for (PacketListener listener : packetListeners)
								{
									listener.onPacketSend(packet);
								}
								packetOutBuffer.remove(packet);
							} catch (IOException e)
							{
								e.printStackTrace();
							}
						}
					}
				}

			}, "UDP Packet Output Thread-" + instances);

			this.serverIP = InetAddress.getByName(serverIp);
			this.serverPort = serverPort;
			packetOutBuffer = new ArrayList<Packet>();
			packetListeners = new ArrayList<PacketListener>();

		} catch (SocketException e)
		{
			e.printStackTrace();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		}

		instances++;

	}

	public String getClientIp()
	{
		return socket.getLocalAddress().getHostAddress();
	}

	public int getClientPort()
	{
		return socket.getLocalPort();
	}

	public void addPacketListener(PacketListener listener)
	{
		packetListeners.add(listener);
	}

	public void removePacketListener(PacketListener listener)
	{
		packetListeners.remove(listener);
	}

	public boolean isConnected()
	{
		return connected;
	}

	public String getServerIP()
	{
		return serverIP.getHostAddress();
	}

	public int getServerPort()
	{
		return serverPort;
	}

	public synchronized void connect()
	{
		socket.connect(serverIP, serverPort);
		connected = true;

		packetInThread.start();
		packetOutThread.start();
	}

	public synchronized void disconnect()
	{

		socket.close();

		connected = false;
		/*
		 * try { packetInThread.join(); packetOutThread.join(); } catch
		 * (InterruptedException e) { e.printStackTrace(); }
		 */

	}

	public void sendPacket(Packet packet)
	{
		packetOutBuffer.add(packet);
	}

	@Deprecated
	public boolean ping()
	{

		if (connected)
		{

			PacketPing ping = new PacketPing();
			sendPacket(ping);

			double startTime = System.nanoTime() / 1e6f;
			boolean timeOut = false;
			List<Packet> packets = null;

			/*
			 * while ((packets = getReceivedPackets()).isEmpty() && !timeOut) {
			 * try { Thread.sleep(1); } catch (InterruptedException e) {
			 * e.printStackTrace(); }
			 * 
			 * if ((System.nanoTime() / 1e6f - startTime) >= 2500) { timeOut =
			 * true; } }
			 */

			if (!timeOut)
			{
				double endTime = System.nanoTime() / 1e6f;
				double delta = endTime - startTime;
				boolean success = false;

				for (Packet packet : packets)
				{
					if (packet instanceof PacketPing)
					{
						success = true;
					}
				}

				if (success)
				{
					Gel.log.info("Ping success in " + delta);
					return true;
				} else
				{
					Gel.log.warning("Received message from server, but received incorrect packet!");
				}
			} else
			{
				Gel.log.warning("Ping failed at: " + getServerIP() + ":" + getServerPort());
			}

		}

		return false;
	}

}

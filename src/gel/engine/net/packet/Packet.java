/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.net.packet;

public class Packet
{

	public static final int PACKET_PING = 0x00;

	private int id;
	private byte[] data;

	public Packet(int id)
	{
		this(id, null);
	}

	public Packet(int id, byte[] data)
	{
		this.id = id;
		this.data = data;
	}

	public int getID()
	{
		return id;
	}

	public byte[] getData()
	{
		return data;
	}

	public void setData(byte[] data)
	{
		this.data = data;
	}
	
	public void compile()
	{
		
	}
	
	public void decompile()
	{
		
	}

}

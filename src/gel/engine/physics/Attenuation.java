/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.physics;

public class Attenuation
{

	private float constant;
	private float linear;
	private float quadratic;

	public Attenuation(float constant, float linear, float quadratic)
	{
		this.constant = constant;
		this.linear = linear;
		this.quadratic = quadratic;
	}

	public float getConstant()
	{
		return constant;
	}

	public float getLinear()
	{
		return linear;
	}

	public float getQuadratic()
	{
		return quadratic;
	}

	public float attenuationAt(float distance)
	{
		return 1.0f / (quadratic * distance * distance + linear * distance + constant);
	}

	public void setConstant(float constant)
	{
		this.constant = constant;
	}

	public void setLinear(float linear)
	{
		this.linear = linear;
	}

	public void setQuadratic(float quadratic)
	{
		this.quadratic = quadratic;
	}

}

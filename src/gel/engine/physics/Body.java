/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.physics;

import gel.engine.math.Vector2f;

public class Body
{

	private BodyType type;
	private BoundingVolume2D volume;
	private Vector2f netForce;
	private Vector2f velocity;
	private float mass;
	private float damping;

	public Body(BodyType type, BoundingVolume2D volume, float mass, float damping)
	{
		this.type = type;
		this.volume = volume;
		this.netForce = new Vector2f();
		this.mass = mass;
		this.velocity = new Vector2f();
		this.damping = damping;
	}

	public BodyType getType()
	{
		return type;
	}

	public BoundingVolume2D getBoundingVolume()
	{
		return volume;
	}

	public Vector2f getNetForce()
	{
		return netForce;
	}

	public Vector2f getPosition()
	{
		return volume.getPosition();
	}

	public Vector2f getVelocity()
	{
		return velocity;
	}

	public float getMass()
	{
		return mass;
	}

	public float getDamping()
	{
		return damping;
	}

	public void setType(BodyType type)
	{
		this.type = type;
	}

	public void setBoundingVolume(BoundingVolume2D volume)
	{
		this.volume = volume;
	}

	public void setPosition(Vector2f position)
	{
		volume.setPosition(position);
	}

	public void setVelocity(Vector2f velocity)
	{
		this.velocity = velocity;
	}

	public void setMass(float mass)
	{
		this.mass = mass;
	}

	public void setDamping(float damping)
	{
		this.damping = damping;
	}

	public void addForce(Vector2f force)
	{
		this.netForce.set(netForce.add(force));
	}

	public void integrate()
	{
		// calculate acceleration and add it to velocity
		Vector2f acceleration = new Vector2f(netForce.getX() / mass, netForce.getY() / mass);
		velocity.set(velocity.add(acceleration));

		// integrate position
		volume.position.setX(volume.position.getX() + velocity.getX());
		volume.position.setY(volume.position.getY() + velocity.getY());

		velocity.multiply(1 - damping);

		// reset net force
		netForce.set(0, 0);
	}

}

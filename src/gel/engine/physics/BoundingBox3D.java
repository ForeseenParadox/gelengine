package gel.engine.physics;

import gel.engine.math.Vector3f;

public class BoundingBox3D extends BoundingVolume3D
{

    private float width;
    private float height;
    private float depth;

    public BoundingBox3D(Vector3f position, float w, float h, float d)
    {
        this.position = position;
        this.width = w;
        this.height = h;
        this.depth = d;
    }

    public float getWidth()
    {
        return width;
    }

    public float getHeight()
    {
        return height;
    }

    public float getDepth()
    {
        return depth;
    }

    public void setWidth(float width)
    {
        this.width = width;
    }

    public void setHeight(float height)
    {
        this.height = height;
    }

    public void setDepth(float depth)
    {
        this.depth = depth;
    }

}

package gel.engine.physics;

public class BoundingSphere extends BoundingVolume3D
{

    private float radius;

    public float getRadius()
    {
        return radius;
    }

    public void setRadius(float radius)
    {
        this.radius = radius;
    }

}

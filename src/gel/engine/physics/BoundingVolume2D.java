/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.physics;

import gel.engine.math.Vector2f;

public abstract class BoundingVolume2D
{

	protected Vector2f position;

	public BoundingVolume2D()
	{
		position = new Vector2f();
	}

	public BoundingVolume2D(Vector2f position)
	{
		this.position = position;
	}

	public Vector2f getPosition()
	{
		return position;
	}

	public void setPosition(Vector2f position)
	{
		this.position = position;
	}

	public boolean intersects(BoundingVolume2D other)
	{
		if (this instanceof BoundingBox2D)
		{
			if (other instanceof BoundingBox2D)
			{
				return boxBox((BoundingBox2D) this, (BoundingBox2D) other);
			} else if (other instanceof BoundingCircle)
			{
				return boxCircle((BoundingBox2D) this, (BoundingCircle) other);
			} else
			{
				return false;
			}
		} else if (this instanceof BoundingCircle)
		{
			if (other instanceof BoundingBox2D)
			{
				return boxCircle((BoundingBox2D) other, (BoundingCircle) this);
			} else if (other instanceof BoundingCircle)
			{
				return circleCircle((BoundingCircle) this, (BoundingCircle) other);
			} else
			{
				return false;
			}
		} else
		{
			return false;
		}
	}

	private boolean boxBox(BoundingBox2D box1, BoundingBox2D box2)
	{
		Vector2f p1 = box1.position;
		Vector2f p2 = box2.position;
		if (p1.getX() > p2.getX() + box2.getWidth() || p1.getX() + box1.getWidth() < p2.getX() || p1.getY() > p2.getY() + box2.getHeight() || p1.getY() + box1.getHeight() < p2.getY())
		{
			return false;
		} else
		{
			return true;
		}
	}

	private boolean circleCircle(BoundingCircle c1, BoundingCircle c2)
	{
		float radiusSum = c1.getRadius() + c2.getRadius();
		float dx = c1.position.getX() - c2.position.getX();
		float dy = c1.position.getY() - c2.position.getY();

		if (dx * dx + dy * dy <= radiusSum * radiusSum)
		{
			return true;
		} else
		{
			return false;
		}

	}

	private boolean boxCircle(BoundingBox2D box, BoundingCircle circle)
	{
		for (float x = box.position.getX(); x < box.position.getX() + box.getWidth(); x += box.getWidth() / 10f)
		{
			for (float y = box.position.getY(); y < box.position.getY() + box.getHeight(); y += box.getHeight() / 10f)
			{
				float dx = x - circle.position.getX();
				float dy = y - circle.position.getY();

				if (dx * dx + dy * dy <= circle.getRadius() * circle.getRadius())
				{
					return true;
				}
			}
		}
		return false;
	}
}

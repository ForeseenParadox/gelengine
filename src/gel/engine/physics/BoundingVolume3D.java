package gel.engine.physics;

import gel.engine.math.Vector3f;

public class BoundingVolume3D
{

	protected Vector3f position;

	public Vector3f getPosition()
	{
		return position;
	}

	public void setPosition(Vector3f position)
	{
		this.position = position;
	}

	public boolean intersects(BoundingVolume3D other, Vector3f translate1, Vector3f translate2)
	{

		Vector3f p1 = position.add(translate1);
		Vector3f p2 = other.getPosition().add(translate2);

		System.out.println(p2.getX() + " " + p2.getY() + " " + p2.getZ());

		if (this instanceof BoundingBox3D)
		{
			if (other instanceof BoundingBox3D)
			{
				return cubeCube((BoundingBox3D) this, (BoundingBox3D) other, p1, p2);
			} else if (other instanceof BoundingSphere)
			{
				return cubeSphere((BoundingBox3D) this, (BoundingSphere) other, p1, p2);
			} else
			{
				return false;
			}
		} else if (this instanceof BoundingSphere)
		{
			if (other instanceof BoundingBox3D)
			{
				return cubeSphere((BoundingBox3D) other, (BoundingSphere) this, p1, p2);
			} else if (other instanceof BoundingSphere)
			{
				return sphereSphere((BoundingSphere) this, (BoundingSphere) other, p1, p2);
			} else
			{
				return false;
			}
		} else
		{
			return false;
		}
	}

	private boolean cubeCube(BoundingBox3D cube1, BoundingBox3D cube2, Vector3f p1, Vector3f p2)
	{
		if (p1.getX() + cube1.getWidth() < p2.getX() || p1.getX() > p2.getX() + cube2.getWidth() || p1.getY() + cube1.getHeight() < p2.getY() || p1.getY() > p2.getY() + cube2.getHeight() || p1.getZ() + cube1.getDepth() < p2.getZ() || p1.getZ() > p2.getZ() + cube2.getDepth())
		{
			return false;
		} else
		{
			return true;
		}
	}

	private boolean sphereSphere(BoundingSphere s1, BoundingSphere s2, Vector3f p1, Vector3f p2)
	{
		float radSum = s1.getRadius() + s2.getRadius();
		float dx = p2.getX() - p1.getX();
		float dy = p2.getY() - p1.getY();
		float dz = p2.getZ() - p1.getZ();

		if (dx * dx + dy * dy + dz * dz <= radSum * radSum)
		{
			return true;
		} else
		{
			return false;
		}

	}

	private boolean cubeSphere(BoundingBox3D cube, BoundingSphere sphere, Vector3f p1, Vector3f p2)
	{
		for (float x = p1.getX(); x < p1.getX() + cube.getWidth(); x += cube.getWidth() / 10f)
		{
			for (float y = p1.getY(); y < p1.getY() + cube.getHeight(); y += cube.getHeight() / 10f)
			{
				for (float z = p1.getZ(); z < p1.getZ() + cube.getDepth(); z += cube.getDepth() / 10f)
				{
					float dx = p2.getX() - x;
					float dy = p2.getY() - y;
					float dz = p2.getZ() - z;

					if (dx * dx + dy * dy + dz * dz <= sphere.getRadius() * sphere.getRadius())
					{
						return true;
					}
				}
			}

		}
		return false;
	}

}
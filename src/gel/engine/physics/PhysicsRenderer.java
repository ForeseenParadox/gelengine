/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.physics;

import gel.engine.graphics.batch.ShapeBatch;

public class PhysicsRenderer
{

	private ShapeBatch batch;

	public PhysicsRenderer()
	{
		batch = new ShapeBatch();
	}

	public void render(PhysicsWorld world)
	{
		batch.begin();
		for (Body body : world.getBodies())
		{
			if (body.getBoundingVolume() instanceof BoundingBox2D)
			{
				BoundingBox2D box = (BoundingBox2D) body.getBoundingVolume();
				batch.renderRect(box.getPosition().getX(), box.getPosition().getY(), box.getWidth(), box.getHeight(), box.getWidth() / 2, box.getHeight() / 2, 0);
			} else if (body.getBoundingVolume() instanceof BoundingCircle)
			{
				BoundingCircle circle = (BoundingCircle) body.getBoundingVolume();
				batch.renderCircle(circle.getPosition().getX(), circle.getPosition().getY(), circle.getRadius());
			}
		}
		batch.end();
	}

}

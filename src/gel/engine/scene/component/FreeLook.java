/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.scene.component;

import gel.engine.core.Gel;
import gel.engine.math.Vector2f;
import gel.engine.scene.object.GameObject;

public class FreeLook extends GameComponent
{

	private boolean grab = true;

	public FreeLook()
	{
		super();
	}

	public FreeLook(GameObject parent)
	{
		super(parent);
	}

	@Override
	public void update()
	{

		if (Gel.input.getMouse().wasButtonPressed(0))
		{
			grab = !grab;
		}

		Gel.input.getMouse().setGrabbed(grab);

		if (grab)
		{
			Vector2f center = new Vector2f(Gel.window.getWidth() / 2, Gel.window.getHeight() / 2);

			float dx = Gel.input.getMouse().getX() - center.getX();
			float dy = Gel.input.getMouse().getY() - center.getY();

			float rotY = parent.getTransform().getRotation().getY() - dx / 500f;
			float rotX = parent.getTransform().getRotation().getX() + dy / 500f;

			parent.getTransform().getRotation().setY(rotY);

			if (Math.abs(rotX) <= Math.toRadians(90))
			{
				parent.getTransform().getRotation().setX(rotX);
			}

			if (dx != 0 || dy != 0)
			{
				Gel.input.getMouse().setPosition((int) center.getX(), (int) center.getY());
			}
		}
	}

}

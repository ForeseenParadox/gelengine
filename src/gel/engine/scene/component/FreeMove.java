/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.scene.component;

import gel.engine.core.Gel;
import gel.engine.input.Keyboard;
import gel.engine.math.Vector3f;
import gel.engine.scene.object.GameObject;

public class FreeMove extends GameComponent
{

	public boolean move = true;
	public Vector3f movement = new Vector3f();

	public static final float SPEED_SCALE = 1 / 80f;

	public FreeMove()
	{
		super();
	}

	public FreeMove(GameObject parent)
	{
		super(parent);
	}

	@Override
	public void update()
	{

		if (move)
		{
			Vector3f translation = parent.getTransform().getTranslation();
			Vector3f rotation = parent.getTransform().getRotation();

			boolean forwardMove = false;
			boolean upMove = false;

			if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_W))
			{
				movement.setZ((float) Math.sin(-rotation.getY() + Math.toRadians(90)) * SPEED_SCALE);
				movement.setX((float) Math.cos(-rotation.getY() + Math.toRadians(90)) * SPEED_SCALE);
				forwardMove = true;
			}
			if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_S))
			{
				movement.setZ(-((float) Math.sin(-rotation.getY() + Math.toRadians(90)) * SPEED_SCALE));
				movement.setX(-((float) Math.cos(-rotation.getY() + Math.toRadians(90)) * SPEED_SCALE));
				forwardMove = true;
			}
			if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_A))
			{
				movement.setZ((float) Math.sin(-rotation.getY()) * SPEED_SCALE);
				movement.setX((float) Math.cos(-rotation.getY()) * SPEED_SCALE);
				forwardMove = true;
			}
			if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_D))
			{
				movement.setZ(-((float) Math.sin(-rotation.getY()) * SPEED_SCALE));
				movement.setX(-((float) Math.cos(-rotation.getY()) * SPEED_SCALE));
				forwardMove = true;
			}
			if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_SPACE))
			{
				movement.setY(-SPEED_SCALE);
				upMove = true;
			}
			if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_LSHIFT))
			{
				movement.setY(SPEED_SCALE);
				upMove = true;
			}

			if (!forwardMove)
			{
				movement.setX(0);
				movement.setZ(0);
			}
			if (!upMove)
			{
				movement.setY(0);
			}
			Vector3f result = translation.add(movement);

			translation.set(result);
		}

	}

}

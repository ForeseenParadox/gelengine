/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.scene.component;

import gel.engine.core.Gel;
import gel.engine.graphics.BlendFunction;
import gel.engine.graphics.Color;
import gel.engine.graphics.Material;
import gel.engine.graphics.RenderCore.DepthFunction;
import gel.engine.graphics.lighting.AmbientLight;
import gel.engine.graphics.lighting.DirectionalLight;
import gel.engine.graphics.lighting.PointLight;
import gel.engine.graphics.mesh.Mesh;
import gel.engine.graphics.shader.forward.ForwardAmbient;
import gel.engine.graphics.shader.forward.ForwardDirectional;
import gel.engine.graphics.shader.forward.ForwardPoint;
import gel.engine.math.Matrix4f;
import gel.engine.math.Vector3f;
import gel.engine.scene.object.GameObject;

import java.util.ArrayList;
import java.util.List;

public class MeshRenderer extends GameComponent
{

    private PerspectiveCamera camera;
    private ForwardAmbient fAmbient;
    private ForwardDirectional fDirectional;
    private ForwardPoint fPoint;

    public List<Mesh> meshes = new ArrayList<Mesh>();
    private Matrix4f modelMatrix;
    private Material material;
    private AmbientLight ambientLight;
    private List<DirectionalLight> directionalLights;
    private List<PointLight> pointLights;

    public MeshRenderer(GameObject parent, PerspectiveCamera camera)
    {
        super(parent);

        this.camera = camera;

        fAmbient = new ForwardAmbient("shaders/forward-vertex.vs", "shaders/forward-ambient.fs", true);
        fDirectional = new ForwardDirectional("shaders/forward-vertex.vs", "shaders/forward-directional.fs", true);
        fPoint = new ForwardPoint("shaders/forward-vertex.vs", "shaders/forward-point.fs", true);

        modelMatrix = new Matrix4f();
        modelMatrix.setIdentity();

        ambientLight = new AmbientLight(new Color(1, 1, 1));

        directionalLights = new ArrayList<DirectionalLight>();
        pointLights = new ArrayList<PointLight>();

    }

    public PerspectiveCamera getCamera()
    {
        return camera;
    }

    public ForwardAmbient getfAmbient()
    {
        return fAmbient;
    }

    public ForwardDirectional getfDirectional()
    {
        return fDirectional;
    }

    public ForwardPoint getfPoint()
    {
        return fPoint;
    }

    public Matrix4f getModelMatrix()
    {
        return modelMatrix;
    }

    public Material getMaterial()
    {
        return material;
    }

    public AmbientLight getAmbientLight()
    {
        return ambientLight;
    }

    public List<DirectionalLight> getDirectionalLights()
    {
        return directionalLights;
    }

    public List<PointLight> getPointLights()
    {
        return pointLights;
    }

    public void setCamera(PerspectiveCamera camera)
    {
        this.camera = camera;
    }

    public void setfAmbient(ForwardAmbient fAmbient)
    {
        this.fAmbient = fAmbient;
    }

    public void setfDirectional(ForwardDirectional fDirectional)
    {
        this.fDirectional = fDirectional;
    }

    public void setfPoint(ForwardPoint fPoint)
    {
        this.fPoint = fPoint;
    }

    public void setModelMatrix(Matrix4f modelMatrix)
    {
        this.modelMatrix = modelMatrix;
    }

    public void setMaterial(Material material)
    {
        this.material = material;
    }

    public void setAmbientLight(AmbientLight ambientLight)
    {
        this.ambientLight = ambientLight;
    }

    public void setDirectionalLights(List<DirectionalLight> directionalLights)
    {
        this.directionalLights = directionalLights;
    }

    public void setPointLights(List<PointLight> pointLights)
    {
        this.pointLights = pointLights;
    }

    @Override
    public void render()
    {

        fAmbient.bind();
        fAmbient.updateUniforms(camera.getViewProjectionMatrix(), modelMatrix, ambientLight.getColor().toVector3f());
        fAmbient.setVector3f("eye", parent.getTransform().getTranslation());
        fAmbient.setVector3f("fogColor", new Vector3f(0.4f, 0.4f, 0.6f));
        fAmbient.setFloat("fogDensity", 0.0025f);

        Gel.graphics.setBlendingEnabled(false);

        for (Mesh mesh : meshes)
        {
            mesh.render();
        }

        Gel.graphics.setBlendingEnabled(true);
        Gel.graphics.setBlendFunction(BlendFunction.ADDITIVE);
        Gel.graphics.setDepthMask(false);
        Gel.graphics.setDepthFunction(DepthFunction.EQUAL_TO);

        fDirectional.bind();

        for (DirectionalLight light : directionalLights)
        {
            fDirectional.updateUniforms(camera.getViewProjectionMatrix(), modelMatrix, material, light, parent.getTransform().getTranslation());
            for (Mesh mesh : meshes)
            {
                mesh.render();
            }
        }

        fPoint.bind();

        for (PointLight light : pointLights)
        {
            fPoint.updateUniforms(camera.getViewProjectionMatrix(), modelMatrix, material, light, parent.getTransform().getTranslation());
            for (Mesh mesh : meshes)
            {
                mesh.render();
            }
        }

        Gel.graphics.setDepthMask(true);
        Gel.graphics.setDepthFunction(DepthFunction.LESS_THAN);

    }
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.scene.component;

import gel.engine.core.Gel;
import gel.engine.math.Matrix4f;
import gel.engine.math.Vector3f;
import gel.engine.utils.MathUtils;

public class OrthographicCamera
{

	private Vector3f translation;
	private Vector3f rotation;
	private Vector3f scale;
	private float leftClippingPlane;
	private float rightClippingPlane;
	private float bottomClippingPlane;
	private float topClippingPlane;
	private float zNearClippingPlane;
	private float zFarClippingPlane;
	private Matrix4f viewProjectionMatrix;
	private Matrix4f projectionMatrix;
	private Matrix4f viewMatrix;
	private Matrix4f translationMatrix;
	private Matrix4f rotationMatrix;
	private Matrix4f scaleMatrix;

	public OrthographicCamera()
	{
		this(0, 0, 0, 0, 0, 0);
	}

	public OrthographicCamera(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		viewProjectionMatrix = new Matrix4f();
		projectionMatrix = new Matrix4f();
		viewMatrix = new Matrix4f();
		translationMatrix = new Matrix4f();
		rotationMatrix = new Matrix4f();
		scaleMatrix = new Matrix4f();
		initOrthographicFrustum(left, right, top, bottom, zNear, zFar);

		translation = new Vector3f();
		rotation = new Vector3f();
		scale = new Vector3f();
	}

	public Vector3f getTranslation()
	{
		return translation;
	}

	public Vector3f getRotation()
	{
		return rotation;
	}

	public Vector3f getScale()
	{
		return scale;
	}

	public float getLeftClippingPlane()
	{
		return leftClippingPlane;
	}

	public float getRightClippingPlane()
	{
		return rightClippingPlane;
	}

	public float getBottomClippingPlane()
	{
		return bottomClippingPlane;
	}

	public float getTopClippingPlane()
	{
		return topClippingPlane;
	}

	public float getZNearClippingPlane()
	{
		return zNearClippingPlane;
	}

	public float getZFarClippingPlane()
	{
		return zFarClippingPlane;
	}

	public Matrix4f getViewProjectionMatrix()
	{
		return viewProjectionMatrix;
	}

	public Matrix4f getProjectionMatrix()
	{
		return projectionMatrix;
	}

	public Matrix4f getViewMatrix()
	{
		return viewMatrix;
	}

	public Matrix4f getTranslationMatrix()
	{
		return translationMatrix;
	}

	public Matrix4f getRotationMatrix()
	{
		return rotationMatrix;
	}

	public Matrix4f getScaleMatrix()
	{
		return scaleMatrix;
	}

	public void setTranslation(Vector3f translation)
	{
		this.translation = translation;
	}

	public void setRotation(Vector3f rot)
	{
		this.rotation = rot;
	}

	public void setScale(Vector3f scale)
	{
		this.scale = scale;
	}

	public void setLeftClippingPlane(float leftClippingPlane)
	{
		this.leftClippingPlane = leftClippingPlane;
	}

	public void setRightClippingPlane(float rightClippingPlane)
	{
		this.rightClippingPlane = rightClippingPlane;
	}

	public void setBottomClippingPlane(float bottomClippingPlane)
	{
		this.bottomClippingPlane = bottomClippingPlane;
	}

	public void setTopClippingPlane(float topClippingPlane)
	{
		this.topClippingPlane = topClippingPlane;
	}

	public void setZNearClippingPlane(float zNearClippingPlane)
	{
		this.zNearClippingPlane = zNearClippingPlane;
	}

	public void setzZFarClippingPlane(float zFarClippingPlane)
	{
		this.zFarClippingPlane = zFarClippingPlane;
	}

	public void initOrthographicFrustum()
	{
		initOrthographicFrustum(0, Gel.window.getWidth(), 0, Gel.window.getHeight(), -1, 1);
	}

	public void initOrthographicFrustum(float left, float right, float top, float bottom, float zNear, float zFar)
	{
		this.leftClippingPlane = left;
		this.rightClippingPlane = right;
		this.bottomClippingPlane = bottom;
		this.topClippingPlane = top;
		this.zNearClippingPlane = zNear;
		this.zFarClippingPlane = zFar;
	}

	public void update()
	{
		projectionMatrix = MathUtils.orthoMatrix(leftClippingPlane, rightClippingPlane, bottomClippingPlane, topClippingPlane, zNearClippingPlane, zFarClippingPlane);
		MathUtils.translationMatrix(translationMatrix, translation);
		MathUtils.rotationMatrix(rotationMatrix, rotation);
		MathUtils.scaleMatrix(scaleMatrix, scale.getX(), scale.getY(), scale.getZ());
		viewMatrix = scaleMatrix.multiply(rotationMatrix.multiply(translationMatrix));
		viewProjectionMatrix = viewMatrix.multiply(projectionMatrix);
	}

}

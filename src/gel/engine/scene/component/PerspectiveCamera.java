/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.scene.component;

import gel.engine.core.Gel;
import gel.engine.math.Matrix4f;
import gel.engine.scene.object.GameObject;
import gel.engine.utils.MathUtils;

public class PerspectiveCamera extends GameComponent
{

	private float fov;
	private float aspect;
	private float zNear;
	private float zFar;
	private Matrix4f projectionMatrix;
	private Matrix4f viewMatrix;
	private Matrix4f viewProjectionMatrix;

	public PerspectiveCamera()
	{
		this(null, 70, Gel.window.getAspectRatio(), 0.01f, 1000f);
	}

	public PerspectiveCamera(float fov, float aspect, float zNear, float zFar)
	{
		this(null, fov, aspect, zNear, zFar);
	}

	public PerspectiveCamera(GameObject parent, float fov, float aspect, float zNear, float zFar)
	{
		super(parent);
		this.fov = fov;
		this.aspect = aspect;
		this.zNear = zNear;
		this.zFar = zFar;
		update();
	}

	public float getFov()
	{
		return fov;
	}

	public float getAspect()
	{
		return aspect;
	}

	public float getzNear()
	{
		return zNear;
	}

	public float getzFar()
	{
		return zFar;
	}

	public Matrix4f getProjectionMatrix()
	{
		return projectionMatrix;
	}

	public Matrix4f getViewMatrix()
	{
		return viewMatrix;
	}

	public Matrix4f getViewProjectionMatrix()
	{
		return viewProjectionMatrix;
	}

	public void setFov(float fov)
	{
		this.fov = fov;
	}

	public void setAspect(float aspect)
	{
		this.aspect = aspect;
	}

	public void setzNear(float zNear)
	{
		this.zNear = zNear;
	}

	public void setzFar(float zFar)
	{
		this.zFar = zFar;
	}

	public void setViewMatrix(Matrix4f viewMatrix)
	{
		this.viewMatrix = viewMatrix;
	}

	@Override
	public void update()
	{

		if (projectionMatrix == null)
		{
			projectionMatrix = new Matrix4f();
		}

		MathUtils.perspectiveMatrix(projectionMatrix, fov, aspect, zNear, zFar);
		viewMatrix = parent.getTransform().getTransformMatrix();
		viewProjectionMatrix = viewMatrix.multiply(projectionMatrix);
	}
}

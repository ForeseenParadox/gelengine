/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.scene.object;

import gel.engine.math.Transform;
import gel.engine.scene.component.GameComponent;

import java.util.ArrayList;
import java.util.List;

public class GameObject
{

	private List<GameObject> children;
	private List<GameComponent> components;
	private Transform transform;

	public GameObject()
	{
		children = new ArrayList<GameObject>();
		components = new ArrayList<GameComponent>();
		transform = new Transform();
	}

	public List<GameObject> getChildren()
	{
		return children;
	}

	public List<GameComponent> getComponents()
	{
		return components;
	}

	public Transform getTransform()
	{
		return transform;
	}

	public void setChildren(List<GameObject> children)
	{
		this.children = children;
	}

	public void setComponents(List<GameComponent> components)
	{
		this.components = components;
	}

	public void setTransform(Transform transform)
	{
		this.transform = transform;
	}

	public void update()
	{
		for (GameComponent component : components)
		{
			component.update();
		}

		for (GameObject object : children)
		{
			object.update();
		}
	}

	public void render()
	{
		for (GameComponent component : components)
		{
			component.render();
		}

		for (GameObject object : children)
		{
			object.render();
		}
	}

}

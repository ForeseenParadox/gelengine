package gel.engine.td;

import gel.engine.math.Vector2f;
import gel.engine.utils.Delay;

import java.util.ArrayList;
import java.util.List;

public class BasicShooter extends PlayerUnit
{

    private Delay shoot;

    public BasicShooter(Vector2f pos)
    {
        super(pos, Textures.R_BASIC_SHOOTER, 100, 10);
        shoot = new Delay(200);
    }

    @Override
    public void update(World world)
    {
        final float radius = 200;

        if (world.getEnemyUnits().size() > 0 && shoot.isReady())
        {
            List<EnemyUnit> eUnits = new ArrayList<EnemyUnit>();
            for (EnemyUnit unit : world.getEnemyUnits())
            {
                Vector2f vec = unit.midpoint().subtract(midpoint());
                float dist = vec.length();

                if (dist <= radius)
                {
                    eUnits.add(unit);
                }
            }

            EnemyUnit shortest = null;
            Vector2f m = midpoint();
            for (EnemyUnit unit : eUnits)
            {
                if (shortest == null)
                {
                    shortest = unit;
                    continue;
                }
                float dist1 = unit.midpoint().subtract(m).length();
                float dist2 = shortest.midpoint().subtract(m).length();
                if (dist1 < dist2)
                {
                    shortest = unit;
                }
            }

            if (shortest != null)
            {
                Vector2f vel = shortest.midpoint().subtract(midpoint()).normalized();
                vel.set(vel.multiply(8f));

                ProjectileBullet bullet = new ProjectileBullet(midpoint(), vel);

                world.getProjectiles().add(bullet);
            }
        }
    }
}

package gel.engine.td;

import gel.engine.graphics.animation.Animation;
import gel.engine.graphics.batch.ShapeBatch;
import gel.engine.math.Vector2f;
import gel.engine.physics.BoundingBox2D;

public class Dragon extends EnemyUnit
{

	public static final float SPEED = 1f;

	public Vector2f vel = new Vector2f();
	private Animation anim;
	private Direction dir = Direction.EAST;

	public Dragon(Vector2f pos)
	{
		super(pos, Textures.R_DRAGON_DBG, 10000, 1);

		//anim = new Animation(Textures.DRAGON, 128, 64, 0, 63, 128 * 4, 64, 100, new int[] { 0, 1, 2, 3, 2, 1 }).start();
		//setTexture(anim.getCurrentFrame().getTexture());
	}

	@Override
	public void update(World world)
	{

		//anim.update();
		//setTexture(anim.getCurrentFrame().getTexture());

		MainBase base = world.getMainBase();

		BoundingBox2D bbBase = new BoundingBox2D(base.pos, Tile.SIZE, Tile.SIZE);
		BoundingBox2D bbThis = new BoundingBox2D(pos, texture.getWidth(), texture.getHeight());

		if (bbBase.intersects(bbThis))
		{
			base.setHealth(base.getHealth() - damage);
		} else
		{
			TileLocation current = new TileLocation((int) (pos.getX() / Tile.SIZE), (int) (pos.getY() / Tile.SIZE));
			TileLocation original = new TileLocation(current);

			if (dir == Direction.EAST)
			{
				
				current.setX(current.getX() + 1);
			} else if (dir == Direction.WEST)
			{
				current.setX(current.getX() - 1);
			} else if (dir == Direction.NORTH)
			{
				current.setY(current.getY() + 1);
			} else if (dir == Direction.SOUTH)
			{
				current.setY(current.getY() - 1);
			}

			Tile t = world.getTileAt(current);

			if (t.isSolid())
			{

				if (dir == Direction.EAST)
				{
					pos.setX((current.getX() - SPEED) * Tile.SIZE);
				} else if (dir == Direction.WEST)
				{
					pos.setX((current.getX() + SPEED) * Tile.SIZE);
				} else if (dir == Direction.NORTH)
				{
					pos.setY((current.getY() - SPEED) * Tile.SIZE);
				} else if (dir == Direction.SOUTH)
				{
					pos.setY((current.getY() + SPEED) * Tile.SIZE);
				}

				if (dir == Direction.EAST || dir == Direction.WEST)
				{
					TileLocation north = new TileLocation(original.getX(), original.getY() + 1);
					if (!world.getTileAt(north).isSolid())
					{
						dir = Direction.NORTH;
					} else
					{
						TileLocation south = new TileLocation(original.getX(), original.getY() - 1);
						if (!world.getTileAt(south).isSolid())
						{
							dir = Direction.SOUTH;
						}
					}

				} else if (dir == Direction.NORTH || dir == Direction.SOUTH)
				{
					TileLocation east = new TileLocation(original.getX() + 1, original.getY());
					if (!world.getTileAt(east).isSolid())
					{
						dir = Direction.EAST;
					} else
					{
						TileLocation west = new TileLocation(original.getX() - 1, original.getY());
						if (!world.getTileAt(west).isSolid())
						{
							dir = Direction.WEST;
						}
					}

				}
			} else
			{
				pos.set(pos.add(vel));
			}

			if (dir == Direction.EAST)
			{
				vel.setX(SPEED);
				vel.setY(0);
			} else if (dir == Direction.WEST)
			{
				vel.setX(-SPEED);
				vel.setY(0);
			} else if (dir == Direction.NORTH)
			{
				vel.setX(0);
				vel.setY(SPEED);
			} else if (dir == Direction.SOUTH)
			{
				vel.setX(0);
				vel.setY(-SPEED);
			}
		}

		rot = (float) -Math.atan2(vel.getY(), vel.getX()) + (float) Math.toRadians(90);

	}

}

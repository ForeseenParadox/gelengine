package gel.engine.td;

import gel.engine.graphics.texture.TextureRegion;
import gel.engine.math.Vector2f;

public abstract class EnemyUnit extends Unit
{

    public EnemyUnit(Vector2f pos, TextureRegion tex, float startHealth, float damage)
    {
        super(pos, tex, startHealth, damage);
    }

}

package gel.engine.td;

import gel.engine.graphics.batch.SpriteBatch;
import gel.engine.graphics.texture.TextureRegion;
import gel.engine.math.Vector2f;
import gel.engine.physics.BoundingBox2D;

public abstract class Entity
{

    public Vector2f pos;
    protected float rot;
    protected TextureRegion texture;
    protected boolean removed;

    public Entity()
    {
        pos = new Vector2f();
    }

    public Entity(Vector2f vec, TextureRegion texture)
    {
        this.pos = vec;
        this.texture = texture;
    }

    public BoundingBox2D boundingBox()
    {
        return new BoundingBox2D(pos, texture.getWidth(), texture.getHeight());
    }

    public Vector2f midpoint()
    {
        return new Vector2f(pos.getX() + texture.getWidth() / 2, pos.getY() + texture.getHeight() / 2);
    }

    public float getRotation()
    {
        return rot;
    }

    public TextureRegion getTexture()
    {
        return texture;
    }

    public void setRotation(float rot)
    {
        this.rot = rot;
    }

    public void setTexture(TextureRegion texture)
    {
        this.texture = texture;
    }

    public abstract void update(World world);

    public void render(World world)
    {
        SpriteBatch sb = TDGame.getSpriteBatch();
        sb.render(texture, texture.getWidth() / 2, texture.getHeight() / 2, pos.getX(), pos.getY(), rot, 1, 1);
    }

}

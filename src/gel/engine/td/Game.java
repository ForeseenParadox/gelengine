package gel.engine.td;

import gel.engine.core.Gel;
import gel.engine.graphics.BlendFunction;

public class Game
{

	private World world;
	private int money = 5000;

	public Game()
	{

	}

	public World getWorld()
	{
		return world;
	}

	public int getMoney()
	{
		return money;
	}

	public void setMoney(int money)
	{
		this.money = money;
	}

	public void start(World world)
	{
		this.world = world;
	}

	public void update()
	{
		if (world != null)
		{
			world.update();
		}
	}

	public void render()
	{
		if (world != null)
		{
			world.render(TDGame.getSpriteBatch(), TDGame.getLightRenderer());
		}
		
		Gel.graphics.setBlendingEnabled(true);
		Gel.graphics.setBlendFunction(BlendFunction.ADDITIVE);
		
		TDGame.getFontRenderer().render(TDGame.getArial18(), "Money: " + money, 10, Gel.window.getHeight() - 50);
	}
}

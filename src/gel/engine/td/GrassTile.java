package gel.engine.td;

public class GrassTile extends Tile
{

	public GrassTile()
	{
		super(Textures.GRASS_TILE);
		solid = true;
	}

}

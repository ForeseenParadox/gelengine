package gel.engine.td;

import gel.engine.core.Core;

public class Main
{

	public static final String TITLE = "Tower Defense";
	public static final int WIDTH = 16 * 80;
	public static final int HEIGHT = 9 * 80;
	public static final boolean RESIZABLE = false;

	public static void main(String[] args)
	{
		new Core(new TDGame(), true, TITLE, WIDTH, HEIGHT, false, -1, 60, 60, 60); // app,
	}
}

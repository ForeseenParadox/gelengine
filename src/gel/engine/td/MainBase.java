package gel.engine.td;

import gel.engine.graphics.texture.TextureRegion;
import gel.engine.math.Vector2f;

public class MainBase extends PlayerUnit
{

    public MainBase(Vector2f pos, TextureRegion tex)
    {
        super(pos, tex, 10000000, 0);
    }

    @Override
    public void update(World world)
    {
    }
}

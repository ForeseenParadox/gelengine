package gel.engine.td;

import gel.engine.core.Gel;
import gel.engine.graphics.texture.TextureRegion;
import gel.engine.math.Vector2f;
import gel.engine.physics.BoundingBox2D;

import java.util.ArrayList;
import java.util.List;

public class Projectile extends Entity
{

    private Vector2f vel;
    private Vector2f startPos;
    private float range = Gel.window.getWidth() > Gel.window.getHeight() ? Gel.window.getWidth() : Gel.window.getHeight();

    public Projectile(Vector2f pos, TextureRegion tex, Vector2f vel)
    {
        super(pos, tex);
        this.startPos = new Vector2f(pos.getX(), pos.getY());
        this.vel = vel;
    }

    public Vector2f getVelocity()
    {
        return vel;
    }

    public List<EnemyUnit> collidesWithAnyEnemey(World world)
    {
        List<EnemyUnit> res = new ArrayList<EnemyUnit>();
        for (EnemyUnit unit : world.getEnemyUnits())
        {
            BoundingBox2D bbEU = new BoundingBox2D(unit.pos, unit.getTexture().getWidth(), unit.getTexture().getHeight());
            BoundingBox2D bbThis = new BoundingBox2D(pos, texture.getWidth(), texture.getHeight());

            if (bbEU.intersects(bbThis))
            {
                res.add(unit);
            }
        }
        return res;
    }

    public void removeOnCollision(World world)
    {
        if (collidesWithAnyEnemey(world).size() > 0)
        {
            removed = true;
        }
    }

    public boolean passedRange()
    {
        if (pos.subtract(startPos).length() >= range)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public void rotateTowardsVelocity()
    {
        rot = (float) -Math.atan2(vel.getY(), vel.getX());
    }

    @Override
    public void update(World world)
    {
        pos.set(pos.add(vel));
        rotateTowardsVelocity();
        if (passedRange())
        {
            removed = true;
        }
    }
}

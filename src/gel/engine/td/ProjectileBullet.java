package gel.engine.td;

import gel.engine.graphics.Color;
import gel.engine.graphics.lighting.PointLight;
import gel.engine.math.Vector2f;
import gel.engine.math.Vector3f;

import java.util.List;

public class ProjectileBullet extends Projectile
{

	private PointLight light;

	public ProjectileBullet(Vector2f pos, Vector2f vel)
	{
		super(pos, Textures.R_BULLET, vel);
		light = new PointLight(new Vector3f(pos.getX(), pos.getY(), 0), Color.BLUE, 1f, 1f, 0, 0.1f, 0f);
	}

	@Override
	public void update(World world)
	{
		final float damage = 40f;

		light.setLinearAtten(light.getLinearAtten() + 0.001f);

		Vector2f m = midpoint();
		light.setPosition(new Vector3f(m.getX(), m.getY(), 0));

		List<EnemyUnit> units = collidesWithAnyEnemey(world);
		for (EnemyUnit unit : collidesWithAnyEnemey(world))
		{
			unit.setHealth(unit.getHealth() - damage);

			if (unit.getHealth() <= 0)
			{
				world.getGame().setMoney(world.getGame().getMoney() + 10);
			}
		}

		if (units.size() > 0)
		{
			removed = true;
		}

		super.update(world);
	}

	@Override
	public void render(World world)
	{
		super.render(world);
		world.renderPointLight(light);
	}
}

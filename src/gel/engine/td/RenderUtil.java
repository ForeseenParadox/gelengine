package gel.engine.td;

import gel.engine.graphics.Color;
import gel.engine.graphics.batch.ShapeBatch;

public class RenderUtil
{

    public static void renderHealthBar(ShapeBatch batch, float x, float y, float width, float height, float percentFilled)
    {
        if (percentFilled < 0)
        {
            percentFilled = 0;
        }

        batch.setColor(Color.RED);
        batch.renderFilledRect(x, y, width, height);
        batch.setColor(Color.GREEN);
        batch.renderFilledRect(x, y, (percentFilled * width) / 100, height);
        batch.setColor(Color.WHITE);
        batch.renderRect(x, y, width, height, 0, 0, 0);
    }
}

package gel.engine.td;

import gel.engine.core.Application;
import gel.engine.core.Gel;
import gel.engine.graphics.BlendFunction;
import gel.engine.graphics.Color;
import gel.engine.graphics.batch.ShapeBatch;
import gel.engine.graphics.batch.SpriteBatch;
import gel.engine.graphics.font.FontRenderer;
import gel.engine.graphics.font.FontType;
import gel.engine.graphics.font.TrueTypeFont;
import gel.engine.graphics.lighting.Light2DRenderer;
import gel.engine.utils.FPSLogger;

public class TDGame extends Application
{

	private static SpriteBatch spriteBatch;
	private static ShapeBatch shapeBatch;
	private static Light2DRenderer lightRenderer;
	private static FontRenderer fontRenderer;
	private static TrueTypeFont arial18;

	private Game game;
	private FPSLogger fpsLogger = new FPSLogger();

	public static SpriteBatch getSpriteBatch()
	{
		return spriteBatch;
	}

	public static ShapeBatch getShapeBatch()
	{
		return shapeBatch;
	}

	public static Light2DRenderer getLightRenderer()
	{
		return lightRenderer;
	}

	public static FontRenderer getFontRenderer()
	{
		return fontRenderer;
	}

	public static TrueTypeFont getArial18()
	{
		return arial18;
	}

	@Override
	public void init()
	{
		// init graphics
		spriteBatch = new SpriteBatch();
		shapeBatch = new ShapeBatch();
		lightRenderer = new Light2DRenderer();
		fontRenderer = new FontRenderer();
		arial18 = new TrueTypeFont(FontType.ARIAL, 18, true);

		spriteBatch.getAmbient().setColor(new Color(0.4f, 0.4f, 0.4f));

		// load textures
		Textures.BASES.load();
		Textures.DRAGON.load();
		Textures.BULLET.load();
		Textures.BASIC_SHOOTER.load();
		Textures.LEVEL1_TILES.load();
		Textures.DRAGON_DBG.load();

		game = new Game();

		TileMap map = new TileMap("worlds/test.png");

		game.start(new World(game, map, new TileLocation(0, 2)));

	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		game.update();

		Gel.window.setTitle("FPS: " + Gel.core.getFps());

	}

	@Override
	public void render()
	{
		Gel.graphics.setBlendingEnabled(true);
		Gel.graphics.setBlendFunction(BlendFunction.ALPHA_BLEND);
		spriteBatch.begin();
		shapeBatch.begin();
		game.render();
		spriteBatch.end();
		shapeBatch.end();
		Gel.graphics.setBlendingEnabled(false);
	}

	@Override
	public void dispose()
	{

	}

}

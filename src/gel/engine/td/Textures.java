package gel.engine.td;

import gel.engine.graphics.Bitmap;
import gel.engine.graphics.texture.Texture;
import gel.engine.graphics.texture.TextureRegion;

public class Textures
{

	public static final Texture DRAGON_DBG = new Texture(new Bitmap(32, 32).fill(0xFFFF0000));
	public static final TextureRegion R_DRAGON_DBG = new TextureRegion(DRAGON_DBG);

	public static final Texture DRAGON = new Texture("textures/greenDragonFTD.png");
	public static final TextureRegion R_CHARGER = new TextureRegion(DRAGON);

	public static final Texture BULLET = new Texture(new Bitmap(10, 2).fill(0xFFFFFF00));
	public static final TextureRegion R_BULLET = new TextureRegion(BULLET);

	public static final Texture BASIC_SHOOTER = new Texture(new Bitmap(20, 20).fill(0xFF00FF00));
	public static final TextureRegion R_BASIC_SHOOTER = new TextureRegion(BASIC_SHOOTER);

	public static final Texture BASES = new Texture("textures/buildingBases.png");
	public static final TextureRegion MAIN_BASE = new TextureRegion(BASES, 0, 0, 48, 47);

	public static final Texture LEVEL1_TILES = new Texture("textures/level1.png");
	public static final TextureRegion GRASS_TILE = new TextureRegion(LEVEL1_TILES, 32, 32, 32, 32);
	public static final TextureRegion DIRT_TILE = new TextureRegion(LEVEL1_TILES, 0, 64, 33, 32);

}

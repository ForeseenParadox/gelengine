package gel.engine.td;

import gel.engine.graphics.texture.TextureRegion;

public abstract class Tile
{

	public static final int SIZE = 32;

	protected boolean solid;
	protected TextureRegion texture;

	public Tile(TextureRegion texture)
	{
		this.texture = texture;
	}

	public boolean isSolid()
	{
		return solid;
	}

	public void setSolid(boolean solid)
	{
		this.solid = solid;
	}

	public TextureRegion getTexture()
	{
		return texture;
	}

	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

	public void render(float x, float y)
	{
		TDGame.getSpriteBatch().render(texture, 0, 0, x, y, 0, 1, 1);
	}
}

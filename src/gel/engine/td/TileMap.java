package gel.engine.td;

import gel.engine.graphics.Bitmap;
import gel.engine.utils.IOUtils;

import java.awt.image.BufferedImage;

public class TileMap
{

	private int width;
	private int height;
	private Tile[] tiles;

	public TileMap(String path)
	{
		BufferedImage image = IOUtils.loadInteralImage(path);
		width = image.getWidth();
		height = image.getHeight();

		Bitmap bmp = new Bitmap(image).flipY();
		int[] data = bmp.getData();

		tiles = new Tile[width * height];

		for (int i = 0; i < data.length; i++)
		{
			int t = data[i];

			Tile tile = null;

			if (t == 0xFF5C2326)
			{
				tile = new DirtPathTile();
			} else if (t == 0xFF00FF00)
			{
				tile = new GrassTile();
			}

			tiles[i] = tile;

		}

	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Tile[] getTiles()
	{
		return tiles;
	}

}

package gel.engine.td;

import gel.engine.graphics.batch.ShapeBatch;
import gel.engine.graphics.texture.TextureRegion;
import gel.engine.math.Vector2f;

public abstract class Unit extends Entity
{

	public final float startingHealth;
	protected float health;
	protected float damage;

	public Unit(Vector2f pos, TextureRegion tex, float startHealth, float damage)
	{
		super(pos, tex);
		this.startingHealth = startHealth;
		this.health = startHealth;
		this.damage = damage;
	}

	public float getHealth()
	{
		return health;
	}

	public float getDamage()
	{
		return damage;
	}

	public void setHealth(float health)
	{
		this.health = health;
	}

	public void setDamage(float damage)
	{
		this.damage = damage;
	}

	@Override
	public void render(World world)
	{
		super.render(world);
		ShapeBatch sb = TDGame.getShapeBatch();
		float hbwidth = texture.getWidth() * 1.5f;
		float hbheight = 10;
		RenderUtil.renderHealthBar(sb, pos.getX() - texture.getWidth() / 4, pos.getY() + texture.getHeight() + 10, hbwidth, hbheight, (health / startingHealth) * 100);
	}
}

package gel.engine.td;

import gel.engine.core.Gel;
import gel.engine.graphics.batch.SpriteBatch;
import gel.engine.graphics.lighting.Light2DRenderer;
import gel.engine.graphics.lighting.PointLight;
import gel.engine.math.Vector2f;
import gel.engine.utils.Delay;
import gel.engine.window.Window;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class World
{

	protected static final Random RAND = new Random();

	private final Game game;

	private Tile[] tiles;
	private int width;
	private int height;
	private List<PlayerUnit> playerUnits;
	private List<EnemyUnit> enemyUnits;
	private List<Projectile> projectiles;
	private TileLocation spawn;

	private MainBase base;

	private Delay spawnTimer;
	private Delay spawnIncrease;

	private List<PointLight> pointLights = new ArrayList<PointLight>();

	public World(Game game, TileMap tileMap, TileLocation spawn)
	{
		this.game = game;
		this.spawn = spawn;

		tiles = tileMap.getTiles();
		width = tileMap.getWidth();
		height = tileMap.getHeight();

		playerUnits = new ArrayList<PlayerUnit>();
		enemyUnits = new ArrayList<EnemyUnit>();
		projectiles = new ArrayList<Projectile>();

		Window hwnd = Gel.window;
		base = new MainBase(new Vector2f(hwnd.getWidth() / 2 - Textures.MAIN_BASE.getWidth() / 2, hwnd.getHeight() / 2 - Textures.MAIN_BASE.getHeight() / 2), Textures.MAIN_BASE);
		playerUnits.add(base);

		spawnTimer = new Delay(2000);
		spawnIncrease = new Delay(1);
	}

	public Game getGame()
	{
		return game;
	}

	public List<PlayerUnit> getPlayerUnits()
	{
		return playerUnits;
	}

	public List<EnemyUnit> getEnemyUnits()
	{
		return enemyUnits;
	}

	public List<Projectile> getProjectiles()
	{
		return projectiles;
	}

	public MainBase getMainBase()
	{
		return base;
	}

	public Tile getTileAt(TileLocation loc)
	{
		return tiles[loc.getX() + (loc.getY() * width)];
	}

	public void updateShadowOccluders()
	{
		TDGame.getLightRenderer().occluders.clear();
		for (PlayerUnit unit : playerUnits)
		{
			TDGame.getLightRenderer().occluders.add(unit.boundingBox());
		}
	}

	public void update()
	{
		updateShadowOccluders();
		for (int i = 0; i < playerUnits.size(); i++)
		{
			PlayerUnit unit = playerUnits.get(i);
			if (unit.removed || unit.getHealth() <= 0)
			{
				playerUnits.remove(unit);
			} else
			{
				unit.update(this);
			}
		}
		for (int i = 0; i < enemyUnits.size(); i++)
		{
			EnemyUnit unit = enemyUnits.get(i);
			if (unit.removed || unit.getHealth() <= 0)
			{
				enemyUnits.remove(unit);
			} else
			{
				unit.update(this);
			}
		}
		for (int i = 0; i < projectiles.size(); i++)
		{
			Projectile unit = projectiles.get(i);
			if (unit.removed)
			{
				projectiles.remove(unit);
			} else
			{
				unit.update(this);
			}
		}

		if (spawnIncrease.isReady())
		{
			spawnTimer.setDelay(spawnTimer.getDelay() - 1);
			if (spawnTimer.getDelay() < 1)
			{
				spawnTimer.setDelay(1);
			}
		}
		if (spawnTimer.isReady())
		{
			enemyUnits.add(new Dragon(new Vector2f(spawn.getX() * Tile.SIZE, spawn.getY() * Tile.SIZE)));
		}

		if (Gel.input.getMouse().wasButtonPressed(0) && game.getMoney() >= 50)
		{
			game.setMoney(game.getMoney() - 50);
			Vector2f vec = new Vector2f(Gel.input.getMouse().getX() - 10, Gel.input.getMouse().getY() - 10);
			playerUnits.add(new BasicShooter(vec));
		}
	}

	public void renderPointLight(PointLight light)
	{
		pointLights.add(light);
	}

	public void render(SpriteBatch batch, Light2DRenderer renderer)
	{
		batch.begin();

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				if (tiles[x + (y * width)] != null)
				{
					tiles[x + (y * width)].render(x * Tile.SIZE, y * Tile.SIZE);
				}
			}
		}

		for (PlayerUnit unit : playerUnits)
		{
			unit.render(this);
		}
		for (EnemyUnit unit : enemyUnits)
		{
			unit.render(this);
		}
		for (Projectile p : projectiles)
		{
			p.render(this);
		}
		batch.end();

		for (PointLight light : pointLights)
		{
			renderer.renderPointLight(light, true);
		}
		pointLights.clear();

	}

}

package gel.engine.test;

import gel.engine.audio.AudioBuffer;
import gel.engine.audio.AudioSource;
import gel.engine.audio.WaveAudio;
import gel.engine.core.Application;
import gel.engine.core.Gel;
import gel.engine.utils.IOUtils;
import org.lwjgl.openal.AL10;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

public class AudioTest extends Application
{

    private AudioSource source;
    private AudioBuffer buffer;

    private String toBinaryString(byte b)
    {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < 8; i++)
        {
            byte s = (byte) ((b >> i) & 1);
            if (s == 0)
            {
                res.append("0");
            } else if (s == 1)
            {
                res.append("1");
            }
        }
        return res.toString();
    }

    @Override
    public void init()
    {
        buffer = new AudioBuffer();
        source = new AudioSource();

        try
        {
            WaveAudio audio = new WaveAudio("audio/pistol.wav");
           // buffer.sendAudioData(audio.getFormat(), audio.getData(), audio.getSampleRate());

            byte[] data = new byte[audio.getData().capacity()];
            audio.getData().get(data);
            for (int i = 0; i < data.length; i++)
            {
                //System.out.println(toBinaryString(data[i]));
            }
        } catch (UnsupportedAudioFileException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        int format = AL10.AL_FORMAT_MONO8;
        int freq = 1000;

        ByteBuffer data = IOUtils.createByteBuffer(100);
        Random rand = new Random();
        for (int i = 0; i < data.capacity(); i++)
        {
            byte[] res = new byte[1];
            rand.nextBytes(res);
            data.put(res[0]);
        }

        source.loadBufferToSource(buffer);
    }

    @Override
    public void resized()
    {

    }

    @Override
    public void update()
    {
        if (Gel.input.getMouse().wasButtonPressed(0))
        {
            source.play();
        }
    }

    @Override
    public void render()
    {

    }

    @Override
    public void dispose()
    {

    }
}

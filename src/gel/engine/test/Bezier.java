/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.test;

import gel.engine.core.Application;
import gel.engine.core.Gel;
import gel.engine.graphics.Bitmap;
import gel.engine.graphics.FrameBuffer;
import gel.engine.graphics.batch.ShapeBatch;
import gel.engine.graphics.batch.SpriteBatch;
import gel.engine.graphics.texture.Texture;
import gel.engine.input.Keyboard;
import gel.engine.math.Vector2f;

import org.lwjgl.opengl.GL11;

public class Bezier extends Application
{

	public static final float TIME_STEP = 0.05f / 2f;

	private ShapeBatch batch;
	private SpriteBatch batch2;
	private Vector2f P1 = new Vector2f(0, 0);
	private Vector2f P2 = new Vector2f(200, 100);
	private Vector2f P3 = new Vector2f(300, 200);
	private Vector2f P4 = new Vector2f(400, 300);
	private int point = 0;
	private Vector2f[] points = new Vector2f[(int) (1 / TIME_STEP)];

	private FrameBuffer fbo;
	private Texture texture;

	public Vector2f calcPoint(float t)
	{
		Vector2f res = new Vector2f();
		float t_ = 1 - t;
		res.setX(t_ * t_ * t_ * P1.getX() + 3 * t_ * t_ * t * P2.getX() + 3 * t_ * t * t * P3.getX() + t * t * t * P4.getX());
		res.setY(t_ * t_ * t_ * P1.getY() + 3 * t_ * t_ * t * P2.getY() + 3 * t_ * t * t * P3.getY() + t * t * t * P4.getY());
		return res;
	}

	public void calc()
	{
		for (int i = 0; i < points.length; i++)
		{
			points[i] = calcPoint(TIME_STEP * i);
		}
	}

	@Override
	public void init()
	{
		batch = new ShapeBatch();
		batch2 = new SpriteBatch();
		texture = new Texture(new Bitmap(16 * 60, 9 * 60)).load();
		// fbo = new FrameBuffer();
		// fbo.attachTexture(texture, GL30.GL_COLOR_ATTACHMENT0);
		calc();
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{

		if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_1))
		{
			point = 0;
		}

		if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_2))
		{
			point = 1;
		}

		if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_3))
		{
			point = 2;
		}

		if (Gel.input.getKeyboard().isKeyDown(Keyboard.KEY_4))
		{
			point = 3;
		}

		if (point == 0)
		{
			P1.setX(Gel.input.getMouse().getX());
			P1.setY(Gel.input.getMouse().getY());
		} else if (point == 1)
		{
			P2.setX(Gel.input.getMouse().getX());
			P2.setY(Gel.input.getMouse().getY());
		} else if (point == 2)
		{
			P3.setX(Gel.input.getMouse().getX());
			P3.setY(Gel.input.getMouse().getY());
		} else if (point == 3)
		{
			P4.setX(Gel.input.getMouse().getX());
			P4.setY(Gel.input.getMouse().getY());
		}

		calc();

		Gel.window.setTitle("FPS: " + Gel.core.getFps());

	}

	@Override
	public void render()
	{

		// fbo.bind();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

		batch.begin();

		for (Vector2f point : points)
		{
			batch.renderLineStrip(point.getX(), point.getY());
		}

		batch.renderLine(P1.getX(), P1.getY(), P2.getX(), P2.getY());
		batch.renderLine(P2.getX(), P2.getY(), P3.getX(), P3.getY());
		batch.renderLine(P3.getX(), P3.getY(), P4.getX(), P4.getY());

		batch.renderFilledRect(P1.getX() - 5, P1.getY() - 5, 10, 10);
		batch.renderFilledRect(P2.getX() - 5, P2.getY() - 5, 10, 10);
		batch.renderFilledRect(P3.getX() - 5, P3.getY() - 5, 10, 10);
		batch.renderFilledRect(P4.getX() - 5, P4.getY() - 5, 10, 10);

		batch.end();
		/*
		 * Gel.window.bindAsRenderTarget(); batch2.begin();
		 * batch2.render(texture, 0, 0); batch2.end();
		 */
	}

	@Override
	public void dispose()
	{

	}

}

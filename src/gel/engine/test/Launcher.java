/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.test;

import gel.engine.core.Core;

public class Launcher
{

	// Complete
	// Forward shading //
	// Forward ambient //
	// Forward directional //
	// Forward point //
	// Scene graph //
	// Audio engine //
	// Fixed free look //
	// Shadow casting //
	// Physics engine //
	// Frame buffers //
	// Render to texture
	// Better texture parameters support, like wrapping, filtering, etc.
	// Mipmapping and Anisotropic filtering
	// Edge antialiasing(Multisample antialiasing)
	// Fog

	// Not complete
	// Shadow mapping
	// Forward spot lights
	// Normal mapping
	// Ambient occclusion
	// Fully complete vector classes
	// Quaternions
	// Finalize font rendering
	// Seperate 2d functionality from 3d
	// Midi support
	// Remove cameras and mesh renderers as game components, they shouldn't be
	// apart of the scene graph.

	public static void main(String[] args)
	{
		new Core(new Bezier(), true, "Core Test", 1336, 760, false, -1, 60, 60, 60);
	}
}

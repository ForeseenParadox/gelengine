package gel.engine.test;

import gel.engine.net.PacketListener;
import gel.engine.net.UDPConnection;
import gel.engine.net.packet.Packet;

public class NetworkTest implements PacketListener
{

	private UDPConnection connection;

	public static void main(String[] args)
	{
		new NetworkTest();
	}

	public NetworkTest()
	{
		connection = new UDPConnection("192.168.1.75", 10, "192.168.1.88", 80);
		connection.connect();
		connection.addPacketListener(this);
		Packet data = new Packet(0, new byte[] { (byte) 'H', (byte) 'E', (byte) 'Y' });
		connection.sendPacket(data);
	}

	@Override
	public void onPacketSend(Packet packet)
	{
		System.out.println("Sending a ping packet from: " + connection.getClientIp() + ":" + connection.getClientPort());
	}

	@Override
	public void onPacketReceive(Packet packet)
	{
		if (packet.getID() == 0)
		{
			System.out.println("Got a ping packet from the server!");
			connection.disconnect();
		}
	}

}

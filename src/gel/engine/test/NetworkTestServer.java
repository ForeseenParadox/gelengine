package gel.engine.test;

import gel.engine.net.PacketListener;
import gel.engine.net.UDPConnection;
import gel.engine.net.packet.Packet;

public class NetworkTestServer implements PacketListener
{

	private UDPConnection connection;

	public static void main(String[] args)
	{
		new NetworkTestServer();
	}

	public NetworkTestServer()
	{
		connection = new UDPConnection("192.168.1.88", 80, "localhost", 3333);
		connection.connect();
		
		System.out.println("Listening...");
	}

	@Override
	public void onPacketSend(Packet packet)
	{

	}

	@Override
	public void onPacketReceive(Packet packet)
	{
		System.out.println("Got a packet from client!");
		connection.disconnect();
	}

}

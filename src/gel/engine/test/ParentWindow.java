package gel.engine.test;

import gel.engine.core.Core;

import java.awt.Canvas;

import javax.swing.JFrame;

public class ParentWindow
{

	private static JFrame window;
	private static Canvas parent;

	public static void main(String[] args)
	{
		window = new JFrame();
		window.setTitle("Core Test");
		window.setSize(800 + 20, 600 + 20);
		window.setLocationRelativeTo(null);
		parent = new Canvas();
		parent.setSize(800, 600);
		parent.setLocation(10, 10);
		window.add(parent);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);

		new Core(new TestGame(), parent, true, "Core Test", 800, 600, false, -1, 60, 60, 60);
	}

}

package gel.engine.test;

import gel.engine.core.Basic3DGame;
import gel.engine.core.Gel;
import gel.engine.graphics.BufferUsage;
import gel.engine.graphics.Primitive;
import gel.engine.graphics.VertexBuffer;
import gel.engine.graphics.mesh.Mesh;
import gel.engine.graphics.texture.Filter;
import gel.engine.graphics.texture.Texture;
import gel.engine.math.Vector3f;
import gel.engine.physics.BoundingBox3D;
import gel.engine.scene.component.FreeLook;
import gel.engine.scene.component.FreeMove;
import gel.engine.scene.component.MeshRenderer;
import gel.engine.scene.component.PerspectiveCamera;

public class Physics3D extends Basic3DGame
{

	private PerspectiveCamera camera;
	private MeshRenderer renderer;
	private FreeMove freeMove;
	private Mesh floor;
	private BoundingBox3D floorBox;
	private BoundingBox3D player;
	private Texture texture;

	@Override
	public void init()
	{
		super.init();
		camera = new PerspectiveCamera(root, 70, Gel.window.getAspectRatio(), 0.01f, 5000f);
		renderer = new MeshRenderer(root, camera);
		floor = Utils.generateAABBCube(new VertexBuffer(renderer.getfAmbient().getVertexArray(), 50000, Primitive.QUAD, BufferUsage.STATIC), 0, 0, 0, 50, 2, 50);
		texture = new Texture("textures/bricks.jpg").load(Filter.LINEAR, Filter.LINEAR, 8.0f);
		renderer.meshes.add(floor);

		root.getTransform().setTranslation(new Vector3f(0, 0, -55));
		floorBox = new BoundingBox3D(new Vector3f(), 50, 2, 50);
		player = new BoundingBox3D(new Vector3f(), 0.2f, 0.2f, 0.2f);

		root.getComponents().add(camera);
		root.getComponents().add(renderer);
		root.getComponents().add(freeMove = new FreeMove(root));
		root.getComponents().add(new FreeLook(root));

		Gel.graphics.setDepthTestEnabled(true);
	}

	@Override
	public void update()
	{
		Gel.window.setTitle("FPS: " + Gel.core.getFps());

		Vector3f vec = root.getTransform().getTranslation();

		Vector3f alt = new Vector3f(vec);
		alt.invert();
		// alt.setX(alt.getX() - Gel.window.getWidth() / 1.0f);
		// alt.setY(alt.getY() - Gel.window.getHeight() / 1.0f);
		player.setPosition(alt);

		if (floorBox.intersects(player, new Vector3f(), freeMove.movement))
		{
			freeMove.move = false;
		} else
		{
			freeMove.move = true;
		}
		super.update();

	}
}

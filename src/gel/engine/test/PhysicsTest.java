/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.test;

import gel.engine.core.Application;
import gel.engine.math.Vector2f;
import gel.engine.physics.*;

public class PhysicsTest extends Application
{

	private PhysicsWorld world;
	private PhysicsRenderer renderer;

	@Override
	public void init()
	{
		world = new PhysicsWorld();
		renderer = new PhysicsRenderer();

		world.getBodies().add(new Body(BodyType.DYNAMIC, new BoundingBox2D(new Vector2f(100, 100), 20, 20), 1.0f, 0.01f));
	}

	@Override
	public void resized()
	{

	}

	Vector2f vec = new Vector2f(0.01f, 0);

	@Override
	public void update()
	{
		if (world.getBodies().get(0).getPosition().getX() < 400)
		{
			world.getBodies().get(0).addForce(vec);
		}
		world.simulate();

	}

	@Override
	public void render()
	{
		renderer.render(world);
	}

	@Override
	public void dispose()
	{

	}

}

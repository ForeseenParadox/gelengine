/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.test;

import gel.engine.core.Application;
import gel.engine.core.Gel;
import gel.engine.graphics.Color;
import gel.engine.graphics.batch.SpriteBatch;
import gel.engine.graphics.lighting.Light2DRenderer;
import gel.engine.graphics.lighting.PointLight;
import gel.engine.graphics.texture.Texture;
import gel.engine.math.Vector2f;
import gel.engine.math.Vector3f;
import gel.engine.physics.BoundingBox2D;
import gel.engine.utils.Rand;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import java.util.ArrayList;
import java.util.List;

public class StencilTest extends Application
{

	private SpriteBatch batch;
	private Texture texture;
	private List<PointLight> lights = new ArrayList<PointLight>();
	private Light2DRenderer renderer;
	private BoundingBox2D occluder;

	@Override
	public void init()
	{
		batch = new SpriteBatch();
		batch.getAmbient().setColor(new Color(0.4f, 0.4f, 0.4f));
		texture = new Texture("textures/grass.png").load();

		renderer = new Light2DRenderer();

		GL11.glEnable(GL13.GL_MULTISAMPLE);

	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		if (Gel.input.getMouse().wasButtonPressed(0))
		{
			renderer.occluders.add(new BoundingBox2D(new Vector2f(Gel.input.getMouse().getX(), Gel.input.getMouse().getY()), 32, 32));
		}
		if (Gel.input.getMouse().wasButtonPressed(1))
		{
			lights.add(new PointLight(new Vector3f(Gel.input.getMouse().getX(), Gel.input.getMouse().getY(), 0), new Color(Rand.getRandomFloat(), Rand.getRandomFloat(), Rand.getRandomFloat()), 1, 1,
					1, 0.05f, 0));
		}

		if (lights.size() > 0)
		{
			lights.get(0).getPosition().setX(Gel.input.getMouse().getX());
			lights.get(0).getPosition().setY(Gel.input.getMouse().getY());
		}
		Gel.window.setTitle("FPS: " + Gel.core.getFps() + " Lights: " + lights.size() + " Occluders: " + renderer.occluders.size());
	}

	@Override
	public void render()
	{

		// enable stenciling

		batch.begin();
		for (int x = 0; x < 32 * 32; x += 32)
		{
			for (int y = 0; y < 32 * 32; y += 32)
			{
				batch.render(texture, x, y);
			}
		}
		batch.end();

		for (PointLight light : lights)
		{
			renderer.renderPointLight(light, true);
		}

	}

	@Override
	public void dispose()
	{

	}

}

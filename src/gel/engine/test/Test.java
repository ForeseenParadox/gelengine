/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.test;

import gel.engine.core.Application;
import gel.engine.core.Gel;
import gel.engine.graphics.batch.SpriteBatch;
import gel.engine.graphics.font.FontRenderer;
import gel.engine.graphics.font.FontType;
import gel.engine.graphics.font.TrueTypeFont;
import gel.engine.graphics.texture.Texture;

public class Test extends Application
{

	SpriteBatch batch;
	Texture t;

	TrueTypeFont font;
	FontRenderer render;

	@Override
	public void init()
	{
		batch = new SpriteBatch();
		t = new Texture("textures/grass.png");
		t.load();

		font = new TrueTypeFont(new FontType("Arial"), 30, true);

		render = new FontRenderer();
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		Gel.window.setTitle("FPS: " + Gel.core.getFps());
	}

	@Override
	public void render()
	{

		batch.begin();
	//	batch.render(font.getFontTexture(), 0, 0, 1, 1, font.getFontTexture().getWidth() / 2, font.getFontTexture().getHeight() / 2, 100, 0, 0, 1, 1);
		batch.end();

		render.render(font, "qwertyuiopasdfghjklzxcvbnm !!! QWERTYUIOPASDFGHJKLZXCVBNM", 10, 10);
	}

	@Override
	public void dispose()
	{

	}

}

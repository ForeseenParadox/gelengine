/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.test;

import gel.engine.core.Application;
import gel.engine.core.Gel;
import gel.engine.graphics.Color;
import gel.engine.graphics.batch.SpriteBatch;
import gel.engine.graphics.font.FontRenderer;
import gel.engine.graphics.font.FontType;
import gel.engine.graphics.font.TrueTypeFont;
import gel.engine.graphics.lighting.Light2DRenderer;
import gel.engine.graphics.lighting.PointLight;
import gel.engine.graphics.texture.Texture;
import gel.engine.math.Vector3f;

public class Test2 extends Application
{

	SpriteBatch batch;
	Texture texture;
	Light2DRenderer renderer;
	PointLight light = new PointLight(new Vector3f(100, 100, 0), new Color(0, 0, 1), 1, 1f, 1, 0.05f, 0f);
	TrueTypeFont font;
	FontRenderer frenderer;

	@Override
	public void init()
	{
		batch = new SpriteBatch();
		batch.getAmbient().setColor(new Color(1f, 1f, 1f));
		texture = new Texture("textures/grass.png");
		texture.load();

		renderer = new Light2DRenderer();

		font = new TrueTypeFont(FontType.ARIAL, 20, true);
		frenderer = new FontRenderer();
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		light.getPosition().setX(Gel.input.getMouse().getX());
		light.getPosition().setY(Gel.input.getMouse().getY());
	}

	@Override
	public void render()
	{
		renderer.renderPointLight(light, true);
	}

	@Override
	public void dispose()
	{

	}

}

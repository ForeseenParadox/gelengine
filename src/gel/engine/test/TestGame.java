/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.test;

import org.lwjgl.opengl.GL11;

import gel.engine.core.Basic3DGame;
import gel.engine.core.Gel;
import gel.engine.graphics.*;
import gel.engine.graphics.RenderCore.PolygonMode;
import gel.engine.graphics.lighting.DirectionalLight;
import gel.engine.graphics.mesh.Mesh;
import gel.engine.graphics.model.OBJModel;
import gel.engine.graphics.texture.Texture;
import gel.engine.input.Keyboard;
import gel.engine.math.Vector3f;
import gel.engine.scene.component.FreeLook;
import gel.engine.scene.component.FreeMove;
import gel.engine.scene.component.MeshRenderer;
import gel.engine.scene.component.PerspectiveCamera;

public class TestGame extends Basic3DGame
{

	private PerspectiveCamera camera;
	private MeshRenderer renderer;
	private Mesh mesh;
	private Texture texture;
	private DirectionalLight light = new DirectionalLight(Color.WHITE, new Vector3f(0, 0, -1), 0.1f);

	@Override
	public void init()
	{
		// GL11.glFrontFace(GL11.GL_CW);
		// GL11.glCullFace(GL11.GL_FRONT);
		// GL11.glEnable(GL11.GL_CULL_FACE);
		super.init();
		camera = new PerspectiveCamera(root, 70, Gel.window.getAspectRatio(), 0.01f, 5000f);
		renderer = new MeshRenderer(root, camera);

		VertexBuffer vbo = new VertexBuffer(renderer.getfAmbient().getVertexArray(), 300000 * 4, Primitive.TRIANGLE, BufferUsage.STATIC);

		mesh = new OBJModel("models/monkey.obj").loadModel(vbo);
		// mesh = new Mesh(vbo);
		// mesh.getVertices().add(new Vertex(new Vector3f(0, 0, -10),
		// Color.WHITE, new Vector2f(0, 0)));
		// mesh.getVertices().add(new Vertex(new Vector3f(0, 0, -10 - 2048),
		// Color.WHITE, new Vector2f(0, 1)));
		// mesh.getVertices().add(new Vertex(new Vector3f(2048, 0, -10 - 2048),
		// Color.WHITE, new Vector2f(1, 1)));
		// mesh.getVertices().add(new Vertex(new Vector3f(2048, 0, -10),
		// Color.WHITE, new Vector2f(1, 0)));

		mesh.updateVBOWithData();

		// texture = new Texture("textures/bricks.jpg").load(Filter.LINEAR,
		// Filter.LINEAR, 8.0f);

		// DirectionalLight l1 = new DirectionalLight(new Color(1, 1, 0), new
		// Vector3f(0, 0, -1), 0.2f);
		// renderer.getDirectionalLights().add(light);

		// l2 = new PointLight(new Vector3f(0, 0, 5), new Color(0, 0, 1), 500,
		// 1, 0, 0f, 0.1f);
		// renderer.getPointLights().add(l2);

		renderer.setMaterial(new Material(null, null, 0.2f, 64f));
		renderer.getAmbientLight().setColor(new Color(0.2f, 0.2f, 0.2f));
		renderer.getDirectionalLights().add(light);
		renderer.meshes.add(mesh);

		root.getComponents().add(camera);
		root.getComponents().add(renderer);
		root.getComponents().add(new FreeMove(root));
		root.getComponents().add(new FreeLook(root));

		Gel.graphics.setDepthTestEnabled(true);

	}

	boolean mode = false;

	@Override
	public void update()
	{
		super.update();
		Gel.window.setTitle("FPS: " + Gel.core.getFps());

		if (Gel.input.getKeyboard().wasKeyPressed(Keyboard.KEY_1))
		{
			mode = !mode;
			if (mode)
			{
				Gel.graphics.setPolygonMode(PolygonMode.LINE);
			} else
			{
				Gel.graphics.setPolygonMode(PolygonMode.FILL);
			}
		}
	}

}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.test;

import gel.engine.graphics.Color;
import gel.engine.graphics.Vertex;
import gel.engine.graphics.VertexBuffer;
import gel.engine.graphics.mesh.Mesh;
import gel.engine.math.Vector2f;
import gel.engine.math.Vector3f;

public class Utils
{

	public static Mesh generateAABBCube(VertexBuffer vbo, float x, float y, float z, float width, float height, float depth)
	{
		Mesh res = new Mesh(vbo);

		res.getVertices().add(new Vertex(new Vector3f(x, y, z), Color.WHITE, new Vector2f(0, 0)));
		res.getVertices().add(new Vertex(new Vector3f(x, y + height, z), Color.WHITE, new Vector2f(0, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y + height, z), Color.WHITE, new Vector2f(1, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y, z), Color.WHITE, new Vector2f(1, 0)));

		res.getVertices().add(new Vertex(new Vector3f(x, y, z + depth), Color.RED, new Vector2f(0, 0)));
		res.getVertices().add(new Vertex(new Vector3f(x, y + height, z + depth), Color.RED, new Vector2f(0, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y + height, z + depth), Color.RED, new Vector2f(1, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y, z + depth), Color.RED, new Vector2f(1, 0)));

		res.getVertices().add(new Vertex(new Vector3f(x, y, z), Color.GREEN, new Vector2f(1, 0)));
		res.getVertices().add(new Vertex(new Vector3f(x, y, z + depth), Color.GREEN, new Vector2f(0, 0)));
		res.getVertices().add(new Vertex(new Vector3f(x, y + height, z + depth), Color.GREEN, new Vector2f(0, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x, y + height, z), Color.GREEN, new Vector2f(1, 1)));

		res.getVertices().add(new Vertex(new Vector3f(x + width, y, z), Color.BLUE, new Vector2f(1, 0)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y, z + depth), Color.BLUE, new Vector2f(0, 0)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y + height, z + depth), Color.BLUE, new Vector2f(0, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y + height, z), Color.BLUE, new Vector2f(1, 1)));

		res.getVertices().add(new Vertex(new Vector3f(x, y, z), Color.FUCHSIA, new Vector2f(0, 0)));
		res.getVertices().add(new Vertex(new Vector3f(x, y, z + depth), Color.FUCHSIA, new Vector2f(0, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y, z + depth), Color.FUCHSIA, new Vector2f(1, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y, z), Color.FUCHSIA, new Vector2f(1, 0)));

		res.getVertices().add(new Vertex(new Vector3f(x, y + height, z), Color.FUCHSIA, new Vector2f(0, 0)));
		res.getVertices().add(new Vertex(new Vector3f(x, y + height, z + depth), Color.FUCHSIA, new Vector2f(0, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y + height, z + depth), Color.FUCHSIA, new Vector2f(1, 1)));
		res.getVertices().add(new Vertex(new Vector3f(x + width, y + height, z), Color.FUCHSIA, new Vector2f(1, 0)));

        res.updateVBOWithData();

		return res;
	}

}

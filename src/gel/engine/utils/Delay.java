/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils;

public class Delay
{

	private long delay;
	private long lastCheck;

	public Delay(long delay)
	{
		this.delay = delay;
		this.lastCheck = System.currentTimeMillis();
	}

	public long getDelay()
	{
		return delay;
	}

	public void setDelay(long delay)
	{
		this.delay = delay;
	}

	public void reset()
	{
		this.lastCheck = System.currentTimeMillis();
	}

	public boolean isReady()
	{
		if (System.currentTimeMillis() - lastCheck >= delay)
		{
			lastCheck = System.currentTimeMillis();
			return true;
		} else
		{
			return false;
		}
	}

}

package gel.engine.utils;

import gel.engine.core.Gel;

public class FPSLogger
{

    private Delay delay;

    public FPSLogger()
    {
        delay = new Delay(1000);
    }

    public void log()
    {
        if(delay.isReady())
        {
            Gel.log.info("FPS: " + Gel.core.getFps() + " UPS: " + Gel.core.getUps());
        }
    }

}

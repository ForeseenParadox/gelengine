/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils;

import gel.engine.core.Core;
import gel.engine.math.Matrix2f;
import gel.engine.math.Matrix3f;
import gel.engine.math.Matrix4f;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

public class IOUtils
{

	public static File getInternalResourceAsFile(String path)
	{
		return new File(Core.class.getResource(correctPath(path)).getFile());
	}

	public static InputStream getInternalResourceAsStream(String path)
	{
		return Core.class.getResourceAsStream(correctPath(path));
	}

	public static String getFileName(String path)
	{
		return new File(path).getName();
	}

	public static String getRootPath()
	{
		return Core.class.getProtectionDomain().getCodeSource().getLocation().getFile();
	}

	public static String getRootParentPath()
	{
		return (new File(getRootPath())).getParent();
	}

	public static String loadInternalFileAsString(String path)
	{
		try
		{

			BufferedReader in = new BufferedReader(new InputStreamReader(Core.class.getResourceAsStream(correctPath(path))));

			StringBuilder fileSource = new StringBuilder();

			String currentLine = "";

			while ((currentLine = in.readLine()) != null)
			{
				fileSource.append(currentLine + "\n");
			}

			in.close();
			return fileSource.toString();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static BufferedImage loadInteralImage(String path)
	{

		String correctPath = correctPath(path);

		try
		{
			return ImageIO.read(Core.class.getResourceAsStream(correctPath));
		} catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

	}

	public static String correctPath(String path)
	{
		StringBuilder builder = new StringBuilder();

		path.replace('\\', '/');

		builder.append(path);

		if (!path.startsWith("/"))
		{
			builder.insert(0, '/');
		}

		return builder.toString();
	}

	public static String loadFileAsString(String path)
	{
		return loadFileAsString(new File(path));
	}

	public static String loadFileAsString(File file)
	{
		if (file != null && file.exists())
		{
			try
			{
				BufferedReader in = new BufferedReader(new FileReader(file));
				StringBuilder fileSource = new StringBuilder();

				String currentLine = "";

				while ((currentLine = in.readLine()) != null)
				{
					fileSource.append(currentLine + "\n");
				}

				in.close();

				return fileSource.toString();
			} catch (IOException e)
			{
				System.err.println("Error loading file as string at: " + file.getAbsolutePath());
				e.printStackTrace();
				return null;
			}
		} else
		{
			return null;
		}
	}

	public static ByteBuffer createByteBuffer(int size)
	{
		return BufferUtils.createByteBuffer(size);
	}

	public static IntBuffer createIntBuffer(int size)
	{
		return BufferUtils.createIntBuffer(size);
	}

	public static ShortBuffer createShortBuffer(int size)
	{
		return BufferUtils.createShortBuffer(size);
	}

	public static FloatBuffer createFloatBuffer(int size)
	{
		return BufferUtils.createFloatBuffer(size);
	}

	public static ByteBuffer byteArrayToBuffer(byte[] array)
	{
		ByteBuffer result = createByteBuffer(array.length);

		for (byte b : array)
		{
			result.put(b);
		}

		result.flip();

		return result;
	}

	public static FloatBuffer mat2ToFloatBuffer(Matrix2f mat)
	{
		FloatBuffer res = createFloatBuffer(4);
		for (int x = 0; x < 2; x++)
		{
			for (int y = 0; y < 2; y++)
			{
				res.put(mat.getElement(x, y));
			}
		}
		res.flip();
		return res;
	}

	public static FloatBuffer mat3ToFloatBuffer(Matrix3f mat)
	{
		FloatBuffer res = createFloatBuffer(9);
		for (int x = 0; x < 3; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				res.put(mat.getElement(x, y));
			}
		}
		res.flip();
		return res;
	}

	public static FloatBuffer mat4ToFloatBuffer(Matrix4f mat)
	{
		FloatBuffer res = createFloatBuffer(16);
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				res.put(mat.getElement(x, y));
			}
		}
		res.flip();
		return res;
	}

	public static int byteArrayToInt(byte[] bytes)
	{
		int res = 0;
		if (bytes.length == 4)
		{
			for (int i = 0; i < 4; i++)
			{
				res |= bytes[i];
			}
			return res;
		} else
		{
			throw new IllegalArgumentException("Byte array length must be equal to 4");
		}
	}

	public static byte[] intToByteArray(int i)
	{
		byte[] bytes = new byte[4];
		bytes[0] = (byte) ((i >> 24) & 0xFF);
		bytes[1] = (byte) ((i >> 16) & 0xFF);
		bytes[2] = (byte) ((i >> 8) & 0xFF);
		bytes[3] = (byte) ((i >> 0) & 0xFF);
		return bytes;
	}
}

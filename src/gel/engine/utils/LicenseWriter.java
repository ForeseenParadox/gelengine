/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LicenseWriter
{

	public static final String PATH = "C:\\Users\\fores_000\\Desktop\\Development\\Java\\projects\\Gel\\GelEngine";

	public static void main(String[] args)
	{
		// writeLicense(IOUtils.loadFileAsString(new File("license.txt")), PATH, new String[] { ".vs", ".fs" });
		System.out.println("Uncomment to run license writer");
	}

	public static List<File> getChildren(File root, String[] extensions)
	{
		List<File> files = new ArrayList<File>();

		for (File file : root.listFiles())
		{
			if (!file.isDirectory())
			{
				boolean valid = false;
				for (String extension : extensions)
				{
					if (file.getName().contains(extension))
					{
						valid = true;
					}
				}
				if (valid)
				{
					files.add(file);
				}
			} else
			{
				files.addAll(getChildren(file, extensions));
			}
		}

		return files;
	}

	public static void writeLicense(String license, String rootPath, String[] fileExtensions)
	{
		List<File> files = getChildren(new File(rootPath), fileExtensions);

		for (File file : files)
		{
			StringBuilder builder = new StringBuilder();
			builder.append(license);
			builder.append("\n");
			builder.append(IOUtils.loadFileAsString(file));

			FileOutputStream output = null;
			try
			{
				output = new FileOutputStream(file);
				output.write(builder.toString().getBytes());
			} catch (IOException e)
			{
				e.printStackTrace();
			} finally
			{
				try
				{
					output.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

	}
}

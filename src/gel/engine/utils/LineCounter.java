/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LineCounter
{

	public static final String PATH = "C:\\Users\\fores_000\\Desktop\\Development\\GitGelEngine";

	public static void main(String[] args)
	{

		System.out.println(PATH);

		List<File> allFiles = listAllFiles(new File(PATH));
		int lines = 0;
		int characters = 0;

		// bubble sort the files in terms of lexicographic order
		for (int i = allFiles.size() - 1; i >= 0; i--)
		{
			for (int j = 0; j < i; j++)
			{
				if (allFiles.get(j).getName().compareTo(allFiles.get(j + 1).getName()) > 0)
				{
					File temp = allFiles.get(j);
					allFiles.set(j, allFiles.get(j + 1));
					allFiles.set(j + 1, temp);
				}
			}
		}

		for (File f : allFiles)
		{
			int i = lineCount(f);
			lines += i;
		}

		for (File f : allFiles)
		{
			characters += charCount(f);
		}

		System.out.println(lines + " lines of code in " + allFiles.size() + " classes, avg = " + ((double) lines / allFiles.size()));
		System.out.println("Char count: " + characters + ", avg = " + (characters / allFiles.size()));

	}

	public static int charCount(File file)
	{
		try
		{
			FileInputStream input = null;
			input = new FileInputStream(file);

			int i = 0;

			while (input.read() != -1)
			{
				i++;
			}

			input.close();

			return i;
		} catch (IOException e)
		{
			System.out.println("Caught");
			e.printStackTrace();
			return -1;
		}

	}

	public static int lineCount(File file)
	{
		BufferedReader stream = null;
		try
		{
			stream = new BufferedReader(new FileReader(file));
			int count = 0;
			while (stream.readLine() != null)
			{
				count++;
			}
			stream.close();
			return count;
		} catch (Exception e)
		{
			return 0;
		}
	}

	public static List<File> listAllFiles(File parent)
	{
		if (parent != null)
		{
			List<File> res = new ArrayList<File>();
			for (File f : parent.listFiles())
			{
				String n = f.getName();
				if (f.isDirectory())
				{
					res.addAll(listAllFiles(f));
				} else if (n.contains(".java") || n.contains(".vs") || n.contains(".fs"))
				{
					System.out.println(n);
					res.add(f);
				}

			}
			return res;
		}
		return null;
	}
}

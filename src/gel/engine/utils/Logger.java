/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils;

import java.io.IOException;
import java.io.OutputStream;

public class Logger
{

	private OutputStream output;

	public Logger(OutputStream output)
	{
		this.output = output;
	}

	public OutputStream getOutput()
	{
		return output;
	}

	public void setOutput(OutputStream output)
	{
		this.output = output;
	}

	public void log(LogLevel logLevel, String message, boolean timeStamp)
	{
		StringBuilder string = new StringBuilder();

		if (timeStamp)
		{
			string.append("[" + DateUtils.getHours12() + ":" + DateUtils.getMinutes() + ":" + DateUtils.getSeconds() + "]");
		}

		switch (logLevel)
		{
			case INFO:
				string.append("[INFO]: ");
				break;
			case WARNING:
				string.append("[WARNING]: ");
				break;
			case FATAL:
				string.append("[FATAL]: ");
				break;
		}

		string.append(message + "\n");

		try
		{
			output.write(string.toString().getBytes());
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void info(String message, boolean timeStamp)
	{
		log(LogLevel.INFO, message, timeStamp);
	}

	public void info(String message)
	{
		info(message, true);
	}

	public void warning(String message, boolean timeStamp)
	{
		log(LogLevel.WARNING, message, timeStamp);
	}

	public void warning(String message)
	{
		warning(message, true);
	}

	public void fatal(String message, boolean timeStamp)
	{
		log(LogLevel.FATAL, message, timeStamp);
	}

	public void fatal(String message)
	{
		fatal(message, true);
	}
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils;

import gel.engine.math.Matrix4f;
import gel.engine.math.Vector2f;
import gel.engine.math.Vector3f;

public class MathUtils
{

    public static float clamp(float lower, float higher, float value)
    {
        if(value < lower)
        {
            return lower;
        }else if(value > higher)
        {
            return higher;
        }else
        {
            return value;
        }
    }

	public static Matrix4f orthoMatrix(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		Matrix4f mat = new Matrix4f();
		orthoMatrix(mat, left, right, bottom, top, zNear, zFar);
		return mat;
	}

	public static void orthoMatrix(Matrix4f mat, float left, float right, float bottom, float top, float zNear, float zFar)
	{
		mat.setElement(0, 0, 2 / (right - left));
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, 2 / (top - bottom));
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, -2 / (zFar - zNear));
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, -((right + left) / (right - left)));
		mat.setElement(1, 3, -((top + bottom) / (top - bottom)));
		mat.setElement(2, 3, (zFar + zNear) / (zFar - zNear));
		mat.setElement(3, 3, 1);

	}

	public static Matrix4f perspectiveMatrix(float fov, float aspect, float zNear, float zFar)
	{
		Matrix4f mat = new Matrix4f();
		perspectiveMatrix(mat, fov, aspect, zNear, zFar);
		return mat;
	}

	public static void perspectiveMatrix(Matrix4f mat, float fov, float aspect, float zNear, float zFar)
	{
		// float f = (float) (1 / (Math.tan(fov / 2)));
		float tan = (float) Math.tan(Math.toRadians(fov / 2));
		float zRange = zNear - zFar;

		mat.setElement(0, 0, 1f / (tan * aspect));
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, 1f / tan);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, (zFar + zNear) / zRange);
		mat.setElement(3, 2, -1);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, (2 * zNear * zFar) / zRange);
		mat.setElement(3, 3, 0);

	}

	public static Matrix4f translationMatrix(Vector2f translate)
	{
		return translationMatrix(translate.getX(), translate.getY());
	}

	public static Matrix4f translationMatrix(float x, float y)
	{
		Matrix4f mat = new Matrix4f();
		translationMatrix(mat, x, y);
		return mat;
	}

	public static void translationMatrix(Matrix4f mat, Vector2f translate)
	{
		translationMatrix(mat, translate.getX(), translate.getY());
	}

	public static void translationMatrix(Matrix4f mat, float x, float y)
	{
		translationMatrix(mat, x, y, 0);
	}

	public static Matrix4f translationMatrix(Vector3f translate)
	{
		return translationMatrix(translate.getX(), translate.getY(), translate.getZ());
	}

	public static Matrix4f translationMatrix(float x, float y, float z)
	{
		Matrix4f mat = new Matrix4f();
		translationMatrix(mat, x, y, z);
		return mat;
	}

	public static void translationMatrix(Matrix4f mat, Vector3f translate)
	{
		translationMatrix(mat, translate.getX(), translate.getY(), translate.getZ());
	}

	public static void translationMatrix(Matrix4f mat, float x, float y, float z)
	{
		mat.setElement(0, 0, 1);
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, 1);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, 1);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, x);
		mat.setElement(1, 3, y);
		mat.setElement(2, 3, z);
		mat.setElement(3, 3, 1);
	}

	public static Matrix4f rotationMatrix(Vector3f rot)
	{
		Matrix4f mat = new Matrix4f();
		rotationMatrix(mat, rot.getX(), rot.getY(), rot.getZ());
		return mat;
	}

	public static Matrix4f rotationMatrix(float rotX, float rotY, float rotZ)
	{
		Matrix4f mat = new Matrix4f();
		rotationMatrix(mat, rotX, rotY, rotZ);
		return mat;
	}

	public static void rotationMatrix(Matrix4f mat, Vector3f rot)
	{
		rotationMatrix(mat, rot.getX(), rot.getY(), rot.getZ());
	}

	public static void rotationMatrix(Matrix4f mat, float rotX, float rotY, float rotZ)
	{
		Matrix4f rotXMat = rotationX(rotX);
		Matrix4f rotYMat = rotationY(rotY);
		Matrix4f rotZMat = rotationZ(rotZ);

		mat.set(rotZMat.multiply(rotYMat.multiply(rotXMat)));
	}

	public static Matrix4f rotationX(float theta)
	{
		Matrix4f mat = new Matrix4f();
		rotationX(mat, theta);
		return mat;
	}

	public static void rotationX(Matrix4f mat, float theta)
	{
		float sin = (float) Math.sin(theta);
		float cos = (float) Math.cos(theta);

		mat.setElement(0, 0, 1);
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, cos);
		mat.setElement(2, 1, -sin);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, sin);
		mat.setElement(2, 2, cos);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, 0);
		mat.setElement(3, 3, 1);
	}

	public static Matrix4f rotationY(float theta)
	{
		Matrix4f mat = new Matrix4f();
		rotationY(mat, theta);
		return mat;
	}

	public static void rotationY(Matrix4f mat, float theta)
	{
		float sin = (float) Math.sin(theta);
		float cos = (float) Math.cos(theta);

		mat.setElement(0, 0, cos);
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, sin);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, 1);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, -sin);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, cos);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, 0);
		mat.setElement(3, 3, 1);
	}

	public static Matrix4f rotationZ(float theta)
	{
		Matrix4f mat = new Matrix4f();
		rotationZ(mat, theta);
		return mat;
	}

	public static void rotationZ(Matrix4f mat, float theta)
	{
		float sin = (float) Math.sin(theta);
		float cos = (float) Math.cos(theta);

		mat.setElement(0, 0, cos);
		mat.setElement(1, 0, -sin);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, sin);
		mat.setElement(1, 1, cos);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, 1);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, 0);
		mat.setElement(3, 3, 1);
	}

	public static void scaleMatrix(Vector2f scale)
	{
		scaleMatrix(scale.getX(), scale.getY());
	}

	public static void scaleMatrix(float scaleX, float scaleY)
	{
		scaleMatrix(scaleX, scaleY, 1);
	}

	public static void scaleMatrix(Matrix4f mat, Vector2f scale)
	{
		scaleMatrix(mat, scale.getX(), scale.getY());
	}

	public static void scaleMatrix(Matrix4f mat, float scaleX, float scaleY)
	{
		scaleMatrix(mat, scaleX, scaleY, 1);
	}

	public static Matrix4f scaleMatrix(Vector3f scale)
	{
		return scaleMatrix(scale.getX(), scale.getY(), scale.getZ());
	}

	public static Matrix4f scaleMatrix(float scaleX, float scaleY, float scaleZ)
	{
		Matrix4f mat = new Matrix4f();
		scaleMatrix(mat, scaleX, scaleY, scaleZ);
		return mat;
	}

	public static void scaleMatrix(Matrix4f mat, Vector3f scale)
	{
		scaleMatrix(mat, scale.getX(), scale.getY(), scale.getZ());
	}

	public static void scaleMatrix(Matrix4f mat, float scaleX, float scaleY, float scaleZ)
	{
		mat.setElement(0, 0, scaleX);
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, scaleY);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, scaleZ);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, 0);
		mat.setElement(3, 3, 1);
	}

	public static boolean isPowerOfTwo(int n)
	{
		if ((n != 0) && (n & (n - 1)) == 0)
		{
			return true;
		} else
		{
			return false;
		}
	}

}

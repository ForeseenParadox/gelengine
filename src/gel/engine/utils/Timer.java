/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils;

public class Timer
{

	private long elapsed;
	private boolean started;
	private boolean paused;
	private long lastUpdate;

	public Timer()
	{
		elapsed = 0;
	}

	public long getElapsedTime()
	{
		return elapsed;
	}

	public void start()
	{
		started = true;
		lastUpdate = System.currentTimeMillis();
	}

	public void stop()
	{
		started = false;
		elapsed = 0;
	}

	public void pause()
	{
		paused = true;
	}

	public void unpause()
	{
		paused = false;
	}

	public void reset()
	{
		elapsed = 0;
	}

	public void update()
	{
		if (started && !paused)
		{
			elapsed += System.currentTimeMillis() - lastUpdate;
			lastUpdate = System.currentTimeMillis();
		}
	}

}

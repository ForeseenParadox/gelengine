/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils;

import java.util.ArrayList;
import java.util.List;

public class Utils
{

	public static String[] removeEmptyStrings(String[] array)
	{
		List<String> res = new ArrayList<String>();

		for (String x : array)
		{
			if (!x.equals(""))
			{
				res.add(x);
			}
		}

		String[] resToArray = new String[res.size()];
		res.toArray(resToArray);

		return resToArray;
	}

	public static boolean correctExtension(String path, String ext)
	{
		String[] fileTokens = path.split("\\.");
		if (fileTokens[fileTokens.length - 1].equals(ext))
		{
			return true;
		} else
		{
			return false;
		}
	}
}

/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils.xml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLDoc
{

	private XMLHeader header;
	private File file;
	private XMLElement rootElement;

	public XMLDoc(File file)
	{
		this.file = file;

		Document doc = createDOMDocument();

		// initialize root
		Element root = doc.getDocumentElement();
		rootElement = new XMLElement();
		rootElement.setName(root.getNodeName());

		// add atribs
		NamedNodeMap attributes = root.getAttributes();
		List<XMLAttrib> attribs = new ArrayList<XMLAttrib>();
		for (int i = 0; i < attributes.getLength(); i++)
		{
			String key = attributes.item(i).getNodeName();
			String value = attributes.item(i).getNodeValue();
			attribs.add(new XMLAttrib(key, value));
		}
		rootElement.addAllAttributes(attribs);

		// add children
		NodeList children = root.getChildNodes();
		rootElement.addAllNodesAsChildren(children);
	}

	public XMLHeader getXmlHeader()
	{
		return header;
	}

	public File getFile()
	{
		return file;
	}

	public XMLElement getRootElement()
	{
		return rootElement;
	}

	public void seyXmlHeader(XMLHeader header)
	{
		this.header = header;
	}

	public void setFile(File file)
	{
		this.file = file;
	}

	public void setRootElement(XMLElement rootElement)
	{
		this.rootElement = rootElement;
	}

	public Document createDOMDocument()
	{

		if (file != null)
		{
			try
			{
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				// factory.setValidating(true);
				factory.setIgnoringComments(true);
				factory.setIgnoringElementContentWhitespace(true);
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(file);
				// doc.getDocumentElement().normalize();
				header = new XMLHeader(doc.getXmlVersion(), doc.getXmlEncoding());
				return doc;
			} catch (IOException e)
			{
				e.printStackTrace();
			} catch (SAXException e)
			{
				e.printStackTrace();
			} catch (ParserConfigurationException e)
			{
				e.printStackTrace();
			}

		}

		return null;

	}

	public void save()
	{
		if (file != null)
		{
			try
			{
				if (!file.exists())
				{
					file.createNewFile();
				}
				BufferedWriter writer = new BufferedWriter(new FileWriter(file));

				// version and encoding
				if (header != null)
				{
					writer.append("<?xml version=\"" + header.getXmlVersion() + "\" encoding=\"" + header.getXmlEncoding() + "\"?>\n");
				}

				if (rootElement != null)
				{
					writer.append(rootElement.toStringWithChildren(0));
				}

				writer.close();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

}

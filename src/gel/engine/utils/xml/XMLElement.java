/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.utils.xml;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLElement
{

	private String name;
	private String value;
	private List<XMLElement> children;
	private List<XMLAttrib> attribs;

	public XMLElement()
	{
		this(null, null);
	}

	public XMLElement(String name, String value)
	{
		this.name = name;
		this.value = value;
		children = new ArrayList<XMLElement>();
		attribs = new ArrayList<XMLAttrib>();
	}

	public String getName()
	{
		return name;
	}

	public String getValue()
	{
		return value;
	}

	public List<XMLElement> getChildren()
	{
		return children;
	}

	public List<XMLElement> getDescendents()
	{
		List<XMLElement> res = new ArrayList<XMLElement>();
		for (XMLElement child : children)
		{
			res.add(child);
			if (child.getChildren().size() > 0)
			{
				res.addAll(child.getDescendents());
			}
		}
		return res;
	}

	public List<XMLAttrib> getAttribs()
	{
		return attribs;
	}

	public void addChild(XMLElement child)
	{
		this.children.add(child);
	}

	public void addAllNodesAsChildren(NodeList nodeList)
	{
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{

				XMLElement element = new XMLElement();
				element.setName(node.getNodeName());

				if (node.hasChildNodes())
				{
					NodeList children = node.getChildNodes();
					boolean child = true;
					for (int j = 0; j < children.getLength(); j++)
					{
						if (children.item(j).getNodeType() == Node.ELEMENT_NODE)
						{
							child = false;
						}
					}

					if (child)
					{
						element.setValue(node.getTextContent());
					}
				}

				if (node.hasAttributes())
				{
					NamedNodeMap attributes = node.getAttributes();
					List<XMLAttrib> attribs = new ArrayList<XMLAttrib>();
					for (int j = 0; j < attributes.getLength(); j++)
					{
						String key = attributes.item(j).getNodeName();
						String value = attributes.item(j).getNodeValue();
						attribs.add(new XMLAttrib(key, value));
					}
					element.addAllAttributes(attribs);
				}
				addChild(element);

				if (node.hasChildNodes())
				{
					element.addAllNodesAsChildren(node.getChildNodes());
				} else
				{
				}
			}

		}
	}

	public void removeChild(XMLElement child)
	{
		this.children.remove(child);
	}

	public void addAttrib(XMLAttrib attrib)
	{
		this.attribs.add(attrib);
	}

	public void addAllAttributes(List<XMLAttrib> attribs)
	{
		this.attribs.addAll(attribs);
	}

	public void removeAttrib(XMLAttrib attrib)
	{
		this.attribs.remove(attrib);
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public void tabulate(StringBuilder builder, int count)
	{
		for (int i = 0; i < count; i++)
		{
			builder.append('\t');
		}
	}

	public String toStringWithChildren(int tabs)
	{
		StringBuilder builder = new StringBuilder();

		// add tabs as needed
		tabulate(builder, tabs);

		// add start tag and attributes
		builder.append("<" + name + "");
		if (attribs.size() > 0)
		{
			for (int i = 0; i < attribs.size(); i++)
			{
				XMLAttrib attrib = attribs.get(i);
				builder.append(attrib.getKey() + "=" + "\"" + attrib.getValue() + "\"");
				if (i < (attribs.size() - 1))
				{
					builder.append(" ");
				}
			}
		} else
		{
			builder.append(">");
		}

		// test if there are children

		if (getChildren().size() > 0)
		{
			builder.append("\n");
			for (XMLElement child : children)
			{
				builder.append(child.toStringWithChildren(tabs + 1));
			}
			tabulate(builder, tabs);
			builder.append("</" + name + ">\n");
		} else
		{
			if (value != null)
			{
				builder.append(value);
			}
			builder.append("</" + name + ">\n");
		}

		return builder.toString();
	}

	@Override
	public String toString()
	{
		return "";
	}
}

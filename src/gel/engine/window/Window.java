/*
 * Gel Engine
 * Copyright (C) 2014  Joey Leavell
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gel.engine.window;

import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;
import gel.engine.core.Gel;

import java.awt.Canvas;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;

public class Window
{

	public static final int BYTES_PER_PIXEL = 4;
	public static final int SAMPLES = 8;

	public Window()
	{
		if (Gel.window == null)
		{
			Gel.window = this;
		} else
		{
			throw new IllegalStateException("Can not make more than one instance of Window");
		}
	}

	public void update()
	{
		Display.update();
	}

	public void create()
	{
		try
		{
			Display.create(new PixelFormat(8, 8, 1, SAMPLES));
		} catch (LWJGLException e)
		{
			System.out.println("Error creating window");
			e.printStackTrace();
		}
	}

	public void dispose()
	{
		Display.destroy();
	}

	public String getTitle()
	{
		return Display.getTitle();
	}

	public int getWidth()
	{
		return Display.getWidth();
	}

	public int getHeight()
	{
		return Display.getHeight();
	}

	public float getAspectRatio()
	{
		return (float) getWidth() / getHeight();
	}

	public boolean isResizable()
	{
		return Display.isResizable();
	}

	public boolean isCloseRequested()
	{
		return Display.isCloseRequested();
	}

	public boolean isVisible()
	{
		return Display.isVisible();
	}

	public boolean wasResized()
	{
		return Display.wasResized();
	}

	public void setTitle(String title)
	{
		Display.setTitle(title);
	}

	public void setSize(int width, int height)
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(width, height));
		} catch (LWJGLException e)
		{
			System.err.println("Error changing the size of the window");
			e.printStackTrace();
		}
	}

	public void setLocation(int x, int y)
	{
		Display.setLocation(x, y);
	}

	public void setViewport(int x, int y, int width, int height)
	{
		bindAsRenderTarget();
		glViewport(x, y, width, height);
	}

	public void setResizable(boolean resizable)
	{
		Display.setResizable(resizable);
	}

	public void setParent(Canvas parent)
	{
		try
		{
			Display.setParent(parent);
		} catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	public void bindAsRenderTarget()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

}
